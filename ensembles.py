from typing import List, Union, Tuple

# backend choosing must be top of the list
from plotutilities import import_matplotlib
import_matplotlib()

from joblib import Parallel, delayed
import pandas as pd
import numpy as np
from collections import Counter

import biopredict
import postrun
from lasso import BiopredictLasso
from logit import BiopredictLogit
from regressiontools import ModelFeatureReducer
from ensembletools import EnsembleSampler, EnsembleResultHandler
from survivaltools import SurvivalTransformer
from dataloading import DataProvider
from utilities import StopWatch, zip_to_list
from biopredict_r import RInterface


def reseed():
    np.random.seed(None)
    RInterface.reset_seed()
    

def _ensemble_run_reducer_parallel(reducer: ModelFeatureReducer,
                                   sampler: EnsembleSampler,
                                   iterations: np.ndarray,
                                   is_multiprocess: bool,
                                   column_scores_labels: List[str],
                                   subsamplesize: int,
                                   target_features: int):
    print("Starting computation for iteration {} to {}".format(min(iterations), max(iterations)))
    # this is a row slice of the final result
    column_scores = pd.DataFrame(np.nan, index=iterations, columns=column_scores_labels)
    counter = Counter()

    if is_multiprocess:
        # if forked by joblib, need new entropy for sub-process
        reseed()

    for i in iterations:
        sampler.use_sample(subsamplesize)
        obj = reducer.run(sampler.sample, sampler.clinical_sample, target_features=target_features)
        if obj is None:
            continue
        features = obj.model_features()
        score_insample = obj.score(sampler.sample, sampler.clinical_sample)
        score_outofsample = obj.score(sampler.sample_control, sampler.clinical_sample_control)
        if i % 500 == 0:
            print(" finished iteration {}, reducer ran {}".format(i, reducer.iterations))

        for feature in features:
            counter[feature] += 1

        column_scores.loc[i] = (score_insample, score_outofsample)

    print("Finished computation for iteration {} to {}".format(min(iterations), max(iterations)))
    return pd.Series(counter), column_scores


class Ensemble:

    class Task:
        def __init__(self,
                     reducer: ModelFeatureReducer,
                     columns: Union[List[str], List[List[str]]],
                     transformer: Union[SurvivalTransformer, None]):
            self.reducer = reducer
            self.columns = columns
            self.transformer = transformer

    def __init__(self,
                 bpdata: DataProvider,
                 subsamplesize: int,
                 iterations: int,
                 preprocessor: object,
                 randomize_outcome,
                 target_features: int,
                 n_jobs,
                 directory):
        self.dataset = bpdata
        self.subsamplesize = subsamplesize
        self.iterations = iterations
        self.target_features = target_features
        self.directory = directory
        self.randomize_outcome = randomize_outcome
        self.preprocessor = preprocessor
        self.n_jobs = n_jobs

    def run(self, tasks: List[Task]) -> Tuple[pd.DataFrame, pd.DataFrame]:
        result_tuples = []
        for task in tasks:
            result_tuples.append(self.run_reducer(task))
        values_tuple, scores_tuple = zip(*result_tuples)
        return pd.concat(values_tuple, axis=1), pd.concat(scores_tuple, axis=1)

    def run_reducer(self,
                    task: Task
                    ) -> Tuple[pd.DataFrame, pd.DataFrame]:
        methodname = task.reducer.class_ref.__name__.replace("Biopredict", "").lower()
        # handle the case where an item of columns is a list of time,status columns
        columnlabels = [self.dataset.column_label(column) for column in task.columns]
        values = pd.DataFrame()
        scores = pd.DataFrame()
        sampler = EnsembleSampler(self.dataset, self.preprocessor, task.transformer)
        for column, columnlabel in zip(task.columns, columnlabels):
            print("Doing ensemble fit for \"{}\" using {}".format(columnlabel, methodname))
            watch = StopWatch()
            sampler.set_outcome(column, self.randomize_outcome)
            # reducer.reset_iterations()
            column_scores_labels = [columnlabel + ssuf for ssuf in [" (in sample)", " (out of sample)"]]
            iterations = np.arange(0, self.iterations)

            if self.n_jobs == 1:
                counter, column_scores = _ensemble_run_reducer_parallel(task.reducer, sampler, iterations, False,
                                                                        column_scores_labels,
                                                                        self.subsamplesize, self.target_features)
            else:
                # # harvest entropy before going multi-process
                # sampler.pregenerate_samples(self.subsamplesize, self.iterations)
                # get near-equal sized chunks
                jobs_iteration_chunks = np.array_split(iterations, self.n_jobs)

                # joblib can only pickle top-level functions, code is moved to top-level above
                list_of_tuples = Parallel(n_jobs=self.n_jobs)(
                    delayed(_ensemble_run_reducer_parallel)
                    (task.reducer, sampler, iterations, True,
                     column_scores_labels,
                     self.subsamplesize, self.target_features)
                    for iterations in jobs_iteration_chunks
                )
                counter_parts, column_scores_parts = zip_to_list(list_of_tuples)

                # data comes in chunks from jobs. Sum / assemble.
                counter = pd.concat(counter_parts, axis=1).sum(axis=1)
                column_scores = pd.concat(column_scores_parts, axis=0)  # type: pd.DataFrame

            counter.name = columnlabel
            values = pd.concat([values, counter], axis=1)  # type: pd.DataFrame
            scores = pd.concat([scores, column_scores], axis=1)  # type: pd.DataFrame

            print("\"{}\" using {}: {} iterations, {}"
                  .format(columnlabel, methodname, self.iterations, watch.elapsed_string()))
        return values, scores


def run_ensemble(ensemble: Ensemble,
                 survival_transformer,
                 save: bool,
                 is_multiprocess: bool
                 ) -> Tuple[pd.DataFrame, pd.DataFrame]:
    columns_logit = ensemble.dataset.classification_columns()
    columns_survival = ensemble.dataset.survival_columns()

    if is_multiprocess:
        # if forked by joblib, need new entropy for sub-process
        reseed()

    values, scores = ensemble.run(
        [
            Ensemble.Task(ModelFeatureReducer(BiopredictLasso, starting_parameter=25000),
                          columns_survival,
                          survival_transformer),
            Ensemble.Task(ModelFeatureReducer(BiopredictLogit, starting_parameter=0.001),
                          columns_logit,
                          None)
            # Ensemble.Task(ModelFeatureReducer(BiopredictCoxLasso), columns_survival, survival_transformer)
        ])

    if save:
        handler = EnsembleResultHandler(values, scores, directory=ensemble.directory)
        handler.save_values()
        handler.save_aggregate_score_statistics()
        # ensemble.boxplot(scores, title="Scores distribution", filenamepart="ensemble")
    return values, scores


def main(mode,
         with_permutations,
         short,
         jobs):
    # import warnings
    # warnings.simplefilter("error")

    dataset = biopredict.dataset(mode)
    directory = biopredict.ensemble_directory(mode)
    preprocessor = biopredict.preprocessor(mode)

    survival_transformer = biopredict.survival_transformer(dataset)

    iterations_factor = 1 if short else 1000

    ensemble = Ensemble(dataset,
                        subsamplesize=20,
                        iterations=50*iterations_factor,
                        target_features=10,
                        preprocessor=preprocessor,
                        directory=directory,
                        n_jobs=jobs,
                        randomize_outcome=False)
    watch = StopWatch()
    run_ensemble(ensemble, save=True, is_multiprocess=False, survival_transformer=survival_transformer)
    watch.print_elapsed(" Main ensembles took ")

    if not with_permutations:
        return

    permutation_rounds = 50
    ensemble.iterations = iterations_factor
    watch = StopWatch(process_time=False)
    ensemble.randomize_outcome = True
    ensemble.n_jobs = 1  # now we do parallelization at the level of this method
    values_list, scores_list = zip_to_list(
        Parallel(n_jobs=jobs)(delayed
                              (run_ensemble)(ensemble, save=False, is_multiprocess=True,
                                             survival_transformer=survival_transformer)
                              for _ in range(0, permutation_rounds)
                              ))
    watch.print_elapsed(" Permutated ensembles took ")

    results = EnsembleResultHandler(values_list, scores_list, directory=ensemble.directory)
    suffix = "-randomized-outcome"
    results.save_counts_sum(suffix)
    results.save_aggregate_score(suffix)
    results.save_aggregate_score_statistics(suffix)


if __name__ == "__main__":
    import argparse as ap
    parser = ap.ArgumentParser(description="Ensemble feature selection")
    parser.add_argument("data", choices=biopredict.possible_modes())
    parser.add_argument("action", choices=["ensemble", "postrun"], help="Either 'ensemble' or 'postrun'")

    parser.add_argument("--nocontrol", action="store_true")
    parser.add_argument("--short", action='store_true', default=False)
    parser.add_argument("--jobs", default=1, type=int)
    args, leftovers = parser.parse_known_args()

    if args.action == "ensemble":
        if args.nocontrol:
            print("Omitting controls")
        main(mode=args.data,
             with_permutations=not args.nocontrol,
             short=args.short,
             jobs=min(max(1, args.jobs), 72)
             )
    elif args.action == "postrun":
        postrun.analyse(args.data)
    else:
        print("No action selected!")
