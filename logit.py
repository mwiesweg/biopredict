from fileutilities import prepare_directory
from paperformat import DinA
from plotutilities import format_for_float
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from typing import List, Dict, Iterable
import time

from regressiontools import ModelFromFeatures


class BiopredictLogit(LogisticRegression):

    regularize_with_parameter = True
    param_bigger_for_less_features = False

    def __init__(self, parameter: float, penalty: str = "l1", **kwargs):
        super().__init__(C=parameter, penalty=penalty, **kwargs)
        self.data = None
        self.current_outcome = None
        self.current_selected_features = None
        self.current_prediction = None
        self.current_score = None

    def fit(self, X, y, **fit_params):
        self.data = X
        self.current_outcome = y
        super().fit(self.data, self.current_outcome)
        self.current_selected_features = np.sort(np.unique(np.nonzero(self.coef_)[-1]))
        return self

    def model_dimensions(self) -> int:
        return len(self.current_selected_features)

    def model_features(self):
        return self.data.columns[self.current_selected_features]

    def measure_fit(self) -> Dict:
        self.current_prediction = pd.Series(self.predict(self.data))
        self.current_score = self.score(self.data, self.current_outcome)
        return {'prediction': self.current_prediction, 'score': self.current_score}

    def plot(self, **plotargs) -> None:
        values = pd.DataFrame({'truth': self.current_outcome, 'prediction': self.current_prediction})
        crosstab = pd.crosstab(index=values["truth"], columns=values["prediction"])
        crosstab.plot(kind="bar",
                      figsize=DinA(4).portrait(),
                      stacked=True)


class BiopredictLogitClassifier(ModelFromFeatures):
    def __init__(self, data: pd.DataFrame, **kwargs):
        super().__init__(model=LogisticRegression(C=1.0, penalty="l2", **kwargs), data=data)


def explore_fit(data: pd.DataFrame, clinical: pd.DataFrame, outcome_columns: Iterable[str], cs) -> None:
    prepare_directory("../fit/logit")
    for outcome_column in outcome_columns:
        directory = prepare_directory("../fit/logit/" + outcome_column)
        cs_range_string = "-{:.4f}-{:.4f}".format(min(cs), max(cs))
        file = open(directory + '/' + "coefficients" + cs_range_string, "wt")

        for c in cs:
            logit = BiopredictLogit(data=data, parameter=c)
            print("Fitting "+outcome_column+" with c="+format_for_float(c, 3).format(c)+", features {}...".format(data.shape[1]))
            time1 = time.process_time()
            logit.fit(clinical[outcome_column])
            time2 = time.process_time()
            print("...took {:.3f}. Now plotting.".format(time2-time1))
            measures = logit.measure_fit()
            nonzero = np.sort(np.unique(np.nonzero(logit.coef_)[-1]))

            print("Logit fit for c="+format_for_float(c, 3).format(c)+":", file=file)
            print("   Number of coefficients: {:5d}".format(logit.model_dimensions()), file=file)
            print("   R²: {:.5f}".format(measures["score"]), file=file)
            print("   Coefficients:", file=file)

            for index in nonzero:
                print(16*" " + data.columns[index], file=file)

            logit.plot()
            plt.legend(loc='best')
            plt.xlabel("Truth")
            plt.ylabel("Prediction")
            plt.savefig(directory + "/plot-"+outcome_column+"-"+format_for_float(c, 3).format(c)+".pdf")
            plt.close()

        file.close()

def explore_cs(data, clinical, columns):

    # alphas = np.round(np.geomspace(1, 100000, 25))
    cs = [1, 0.75, 0.5, 0.25, 0.1, 0.05, 0.025, 0.01, 0.005, 0.001, 5e-4, 1e-4, 5e-5, 2.5e-5, 1e-5, 5e-6, 1e-6]
    explore_fit(data, clinical, columns, cs)


if __name__ == "__main__":
    print("Loading data...")
    from dataloading import BiopredictSourceData
    bpdata = BiopredictSourceData().dataset()

    columns = bpdata.classification_columns()
    explore_cs(bpdata.data, bpdata.clinical, columns)


