from typing import List, Dict, Union, Tuple, Callable, Set, Iterable

# backend choosing must be top of the list
from plotutilities import import_matplotlib
import_matplotlib()

import os
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

import biopredict
from fileutilities import prepare_directory
from paperformat import DinA
from utilities import StopWatch
from modelevaluation import ModelEvaluation
from transformers import make_dataframe_pipeline, LabAvoidZeroTransformer, AllRatiosTransformer


def _load_file(mode: str, key: str) -> pd.DataFrame:
    filenames = {"counts": "counts.csv",
                 "counts_rand": "counts-sum-randomized-outcome.csv",
                 "scores": "scores.csv",
                 "scores_rand": "scores-aggregate-randomized-outcome.csv"}
    filename = filenames[key]
    if not filename:
        raise ValueError("Undefined key: " + key)
    print("Reading " + filename)
    return pd.read_csv(biopredict.ensemble_directory(mode) + "/" + filename,
                       sep=";",
                       index_col=0)


def top_entries(mode: str,
                columns: List[str],
                n: int) -> List[List[str]]:
    counts = _load_file(mode, "counts")
    return [
        list(counts[column].sort_values(ascending=False).head(n).index)
        for column in columns
    ]


def analyse(mode: str):
    directory = biopredict.ensemble_directory(mode)
    out_directory = prepare_directory("../fit/evaluation")
    counts, counts_rand, scores, scores_rand = (_load_file(mode, key)
                                                for key in ["counts", "counts_rand", "scores", "scores_rand"])

    # for df in [counts, counts_rand]:
    #     df.index = df.index.str.replace("normalized_", "")

    dataset = biopredict.dataset(mode)
    data_all_features = dataset.data
    if mode == "lab":
        data_all_features = pd.concat([
            dataset.data,
            make_dataframe_pipeline(LabAvoidZeroTransformer(), AllRatiosTransformer()).fit_transform(dataset.data)
        ], axis=1)
    survival_transformer = biopredict.survival_transformer()

    def barplot(counter: pd.Series, title: str, filenamepart: str):
        plot_data = counter[counter > 0].sort_values(ascending=False)
        plot_data.plot.bar(title=title, figsize=DinA(4).landscape())
        plt.tight_layout()
        plt.savefig(directory + "/barplot-" + filenamepart + ".pdf")
        plt.close()

    def histogram(scores_series: pd.Series, title: str, filenamepart: str):
        binwidth = 0.1
        bins = np.arange(scores_series.min(), scores_series.max(), binwidth).tolist() + [scores_series.max()]
        scores_series.plot.hist(title=title, figsize=DinA(4).landscape(), bins=bins)
        plt.tight_layout()
        plt.savefig(directory + "/histogram-" + filenamepart + ".pdf")
        plt.close()

    def boxplot(scores_frame: pd.DataFrame, title: str, filenamepart: str):
        scores_frame.plot.box(title=title, figsize=DinA(3).portrait(), ylim=(-2, 1))
        locs, labels = plt.xticks()
        plt.xticks(locs, labels, rotation="90")
        plt.tight_layout()
        plt.savefig(directory + "/boxplot-" + filenamepart + ".pdf")
        plt.close()

    def plot_count_vs_stdev(log: bool = False):
        print("Plot count vs stddev")
        df = pd.DataFrame({"sum": counts["sum"],
                           "stddev": data_all_features.std(axis=0)})
        if log:
            df["stddev"] = np.log(df["stddev"])
        ax = df.plot(x="sum", y="stddev", kind="scatter", figsize=DinA(4).landscape())
        ax.set_xlabel("number of selections")
        ax.set_ylabel("log(stddev)" if log else "stddev")
        plt.tight_layout()
        plt.savefig(directory + "/count vs stdev.pdf")
        plt.close()

    def plot_true_vs_random_rank():
        print("Plot true vs random rank")
        count_vs_count = pd.DataFrame(0., index=counts.index, columns=["True position", "Randomized position"])
        for i in range(0, counts.shape[0]):
            count_vs_count.iloc[i, 0] = i
            label = count_vs_count.index[i]
            if counts_rand.index.contains(label):
                rand_rank = counts_rand.index.get_loc(label)
            else:
                rand_rank = np.NaN
            count_vs_count.iloc[i, 1] = rand_rank
        ax = count_vs_count.plot(x="True position", y="Randomized position", kind="scatter", figsize=DinA(2).landscape())
        ax.set_xlabel("rank with true outcome")
        ax.set_ylabel("rank with randomized outcome")
        plt.tight_layout()
        plt.savefig(directory + "/true vs random ranks.pdf")
        plt.close()

    def plot_barplots():
        for column in counts.columns:
            print("Barplot for " + column)
            barplot(counts[column], title="Features selection by " + column,
                    filenamepart="features-"+column)
        print("Barplot for random")
        barplot(counts_rand.iloc[:,0],
                title="Features selection by randomized outcome (all endpoints)",
                filenamepart="features-randomized")

    def plot_scores():
        # uneven columns -> out of sample
        for i in range(1, scores.shape[1], 2):
            print("Scores boxplot for " + scores.columns[i])
            histogram(scores.iloc[:, i],
                      title="Scores distribution for {}".format(scores.columns[i]),
                      filenamepart="scores-"+scores.columns[i])
        print("Boxplot for randomized scores")
        boxplot(scores_rand,
                title="Scores distribution for randomized outcome",
                filenamepart="scores-randomized")

    plot_count_vs_stdev(log=True)
    plot_true_vs_random_rank()
    plot_barplots()

    # does not finish for large data sets, not yet solved
    # plot_scores()

    def top_values(series: pd.Series, top_n: int):
        return series.sort_values(ascending=False).head(top_n)

    def create_selections(column_labels: List[str] = None) -> Tuple[pd.DataFrame, Dict[str, pd.DataFrame]]:
        top_n = 50
        # create list of the first 50 for each method, sorted by count
        if column_labels is None:
            filtered_columns = counts.columns[counts.columns != "sum"]
        else:
            filtered_columns = counts.columns[counts.columns.isin(column_labels)]

        tops = pd.concat([top_values(counts[col], top_n) for col in filtered_columns],
                         axis=1) # type: pd.DataFrame
        tops.fillna(0, inplace=True)
        tops.index = tops.index.str.replace("normalized_", "")

        def true_to_random_ratio(label):
            if label not in counts.index or label not in counts_rand.index:
                return np.NaN
            return counts.loc[label, "sum"] / counts_rand.loc[label, "sum"]

        # add mean, median, n selected. Dont add sequentially, that will be bad...
        tops = pd.concat([tops,
                          tops.sum(axis=1).rename("sum"),
                          tops.mean(axis=1).rename("mean"),
                          tops.median(axis=1).rename("median"),
                          (tops != 0).sum(axis=1).rename("selected"),
                          tops.index.to_series().map(true_to_random_ratio).rename("ratio")
                          ], axis=1)

        tops_once_selected = tops[tops["selected"] > 1]

        selections = {
            "selected, mean": tops_once_selected.sort_values(by=["selected", "mean"], ascending=False),
            "selected, median": tops_once_selected.sort_values(by=["selected", "median"], ascending=False),
            "median": tops_once_selected.sort_values(by=["median", "mean"], ascending=False),
            "mean": tops_once_selected.sort_values(by=["mean", "median"], ascending=False),
            "ratio": tops_once_selected.sort_values(by=["ratio", "mean"], ascending=False)
        }
        for key, df in selections.items():
            df.drop(["mean", "selected", "median", "ratio"], axis=1, inplace=True)
        return tops_once_selected, selections

    tops, selections = create_selections()
    #selections = create_selections(["TTF IO", "OS IO","best response IO", "best response binary", "best response ternary"])
    # "TTF IO", "OS IO","best response IO", "best response binary", "best response ternary", "TTF IO-survival", "OS IO-survival"

    def plot_selections():
        params_list = [
            ["selected, mean",  "Features by 1) selected at all by methods 2) mean of selections",
             "/feature selection by n(selected at all), mean.pdf"],
            ["selected, median","Features by 1) selected at all by methods 2) median of selections",
             "/feature selection by n(selected at all), median.pdf"],
            ["median","Features by median of selections", "/feature selection by selection median.pdf"],
            ["mean","Features by mean of selections","/feature selection by selection mean.pdf"],
            ["ratio", "Features by ratio true count / random count", "/features selection by ratio.pdf"]
        ]
        figsize = DinA(4).landscape((2, 1))
        for params in params_list:
            selections[params[0]].plot(kind="bar", figsize=figsize, title=params[1])
            plt.tight_layout()
            plt.savefig(directory + params[2])
            plt.close()
        # jam with
        # pdfjam --nup 2x2 --papersize '{46.81in,16.54in}' -o feature-selection-four-approaches.pdf -- feature\ selection\ by\ selection\ *

    plot_selections()

    def indexes_to_string_list(index_dict: Dict[str, pd.Index]) -> Dict[str, List[str]]:
        return {name: sorted(index.values.tolist()) for name, index in index_dict.items()}

    def uniqe_lists_sorted(iterable_of_lists: Iterable) -> List[List]:
        unique_tuples = {tuple(l) for l in iterable_of_lists}
        unique_lists = [list(t) for t in unique_tuples]
        print(unique_lists)
        return sorted(unique_lists, key=len)

    def sets_by_rank(n: int,
                     selections_to_honour: List[str] = selections.keys(),
                     direct_tops_from_selection: List[str] = None,
                     direct_tops: List[str] = None) -> Dict[str, List[str]]:
        if direct_tops_from_selection is None:
            direct_tops_from_selection = []
        if direct_tops is None:
            direct_tops = []
        # construct frame of features -> ranks in selections for each method
        ranks = pd.DataFrame(
            [
                {df.index[i]: i for i in range(0, df.shape[0])}
                for df in [selections[selection_key] for selection_key in selections_to_honour]
            ],
            index=["by " + s for s in selections_to_honour]).T
        ranks.fillna(0, inplace=True)
        ranks = pd.concat([ranks,
                           ranks.mean(axis=1).rename("mean"),
                           ranks.min(axis=1).rename("min")], axis=1)

        sorted_ranks = {
            "min":  ranks.sort_values(by="min").index,
            "mean": ranks.sort_values(by="mean", ascending=False).index
        }

        def top_by_combination_method(method: Callable, indexes: List[pd.Index], top: int) -> pd.Index:
            for i in range(1, indexes[0].size):
                index = method(indexes[0][:i], indexes[1][:i])
                if index.size >= top:
                    return index[:top]

        by_intersection = top_by_combination_method(pd.Index.intersection, [sorted_ranks[x] for x in ["min", "mean"]], n)
        by_union = top_by_combination_method(pd.Index.union, [sorted_ranks[x] for x in ["min", "mean"]], n)
        by_min_rank = sorted_ranks["min"][:n]
        by_mean_rank = sorted_ranks["mean"][:n]

        # we return 1) lists derived by chosen methods from a combination of selections
        # 2) lists that constitute direct top n of a single selection
        # 3) lists that constitute direct top n of a "tops" list of a single method
        return {**indexes_to_string_list({"overall by intersection": by_intersection,
                                          "overall by union": by_union,
                                          "overall by min rank": by_min_rank,
                                          "overall by mean rank": by_mean_rank}),
                **indexes_to_string_list({"tops from "+column: selections[column].index[:n]
                                          for column in direct_tops_from_selection}),
                **indexes_to_string_list({"tops from "+column: top_values(tops[column], n).index
                                          for column in direct_tops})
                }

    suggested_model_sizes = [3, 5, 8]

    def create_sets(print_to = None) -> List[List[str]]:
        def print_dict(dictionary, file=None):
            for name, list_ in dictionary.items():
                print("   {}:".format(name), list_, file=file)

        dicts_list = []
        for n in suggested_model_sizes:
            direct_selection = ["ratio", "mean"]
            direct_tops = ["TTF IO-survival", "OS IO-survival", "best response binary", "sum"]
            limited = sets_by_rank(n, selections_to_honour=["selected, mean", "mean", "ratio"],
                                   direct_tops_from_selection=direct_selection,
                                   direct_tops=direct_tops)
            dicts_list.append(limited)
            #full = sets_by_rank(n, direct_tops_from_selection=direct_selection)
            #dicts_list.append(full)
            print("n={}:".format(n), file=print_to)
            print("  Based on three selections:", file=print_to)
            print_dict(limited, file=print_to)
            #print("  Based on all selections:", file=print_to)
            #print_dict(full, file=print_to)

        # we have a list of dicts method -> list if sets. Unpack list, then unpack dictionary value which is a list
        return uniqe_lists_sorted(l for dictionary in dicts_list for l in dictionary.values())

    def sets_from_randomized():
        leaders = [counts_rand.index[:top] for top in suggested_model_sizes]
        top20 = counts_rand.head(20)
        # for each suggest model size, draw 4 random samples from the top 20. Reproducible by giving random_state.
        randomsamples = [top20.sample(n, random_state=i*n).index for n in suggested_model_sizes for i in range(0,4)]
        return uniqe_lists_sorted(leaders + randomsamples)

    with open(out_directory+"/model-selection", "w") as f:
        sets = create_sets(f)
    random_sets = sets_from_randomized()

    watch = StopWatch(process_time=False)

    def evaluate_models(mode: str,
                        sets: List[List[str]], suffix: str = ""):
        iterations = 1000
        n_jobs = 10

        pan_sets_measurements_list = []
        for i, set_ in enumerate(sets):
            print("Evaluating set {} ({})".format(i, ", ".join(set_)))

            evaluator = ModelEvaluation(dataset,
                                        features=set_,
                                        feature_scaler=biopredict.preprocessor(mode),
                                        survival_transformer=survival_transformer)

            watch.restart()
            evaluator.evaluate_in_sample(subdir=str(i) + suffix)
            watch.print_elapsed("   Set {}: In sample took ".format(i), " s")

            watch.restart()
            all_measurements, all_coefficients = evaluator.evaluate_out_of_sample(subdir=str(i) + suffix,
                                                                                  subsamplesize=20,
                                                                                  iterations=iterations,
                                                                                  n_jobs=n_jobs
                                                                                  )
            pan_sets_measurements_list.append(all_measurements)
            watch.print_elapsed("   Set {}: Out-of-sample iterations took ".format(i), " min")

        watch.restart()
        # create three-tiered multi-index frame, level 0: 0...sets, level 1: 1..iterations, level 2: outcomes
        pan_sets_measurements = pd.concat(pan_sets_measurements_list,
                                          keys=range(0, len(sets)),
                                          names=["set", "iteration", "outcome"],
                                          axis=1)  # type: pd.DataFrame
        n_plots = pan_sets_measurements.index.size
        fig, ax = plt.subplots(nrows=n_plots, ncols=1, figsize=DinA(4).landscape((4, 2*n_plots)))

        stacked = pan_sets_measurements.stack(["set", "outcome"]) # type: pd.DataFrame
        for i, measurement in enumerate(pan_sets_measurements.index):
            # create a frame with columns: set, outcome; index: iterations
            measurement_slice = stacked.loc[measurement].T
            # get mean over outcomes per set (frame with index: iterations, columns:set
            means = measurement_slice.groupby(level=0, axis=1).mean()
            for set in means.columns:
                measurement_slice[set, "all outcomes"] = means[set]
            measurement_slice.groupby(level=0, axis=1).boxplot(subplots=False, ax=ax[i])
            ax[i].set_xticklabels(ax[i].get_xticklabels(), rotation=315)
            ax[i].set_ylabel(measurement)
        plt.tight_layout()
        plt.savefig(out_directory+"/all_measurements" + suffix + ".pdf")
        watch.print_elapsed("Plotting took ", " s")

    evaluate_models(mode, sets)
    evaluate_models(mode, random_sets, "-randomized")