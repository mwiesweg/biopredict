from dataloading import BiopredictSourceData
from fileutilities import prepare_directory
from paperformat import DinA
from plotutilities import PlotCycler
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import Lasso
from typing import List, Dict, Iterable, ClassVar
import time


class BiopredictLasso(Lasso):

    regularize_with_parameter = True
    param_bigger_for_less_features = True

    def __init__(self, parameter: float, **kwargs):
        super().__init__(alpha=parameter, **kwargs)
        self.data = None
        self.current_outcome = None
        self.current_selected_features = None
        self.current_prediction = None
        self.current_score = None

    def fit(self, X, y, **fit_params):
        self.data = X
        self.current_outcome = y
        super().fit(self.data, self.current_outcome)
        self.current_selected_features = np.nonzero(self.coef_)[0] # feature coordinates
        return self

    def measure_fit(self) -> Dict:
        self.current_prediction = self.predict(self.data)
        self.current_score = self.score(self.data, self.current_outcome)
        return {'prediction': self.current_prediction, 'score': self.current_score}

    def model_dimensions(self) -> int:
        return len(self.current_selected_features)

    def model_features(self):
        return self.data.columns[self.current_selected_features]

    def plot(self, **plotargs) -> None:
        values = pd.DataFrame({'x': self.current_outcome, 'y': self.current_prediction})
        values.sort_values('x', inplace=True)
        fit_fn = np.poly1d(np.polyfit(values['x'], values['y'], 1))
        xfit = np.geomspace(np.min(values['x']), np.max(values['y']), 100)
        yfit = fit_fn(xfit)
        plt.plot(values['x'], values['y'], label="\u03B1={:.0f} (n={:d})".format(self.alpha, len(np.nonzero(self.coef_)[0])), **plotargs)
        plotargs.update(dict(linewidth=0.2, linestyle='-', marker=None))
        plt.plot(xfit[yfit > 1], yfit[yfit > 1], **plotargs)


class BiopredictLassoExplorer:

    def __init__(self, lasso_class: ClassVar = BiopredictLasso):
        self.lasso_class = lasso_class

    def explore_fit(self, data: pd.DataFrame, clinical: pd.DataFrame, outcome_columns: Iterable[str], alphas) -> None:
        prepare_directory("../fit/lasso")
        for outcome_column in outcome_columns:
            directory = prepare_directory("../fit/lasso/" + outcome_column)
            alpha_range_string = "-{: d}-{: d}".format(min(alphas), max(alphas))
            file = open(directory + '/' + "coefficients" + alpha_range_string, "wt")
            plt.figure(figsize=DinA(3).portrait())

            cycler = PlotCycler(len(alphas), colormap_name="YlGnBu")

            for alpha in alphas:
                lasso = self.lasso_class(data, alpha)
                print("Fitting " + outcome_column + " with alpha={:.3f}, features {}...".format(alpha, data.shape[1]))
                time1 = time.process_time()
                lasso.fit(clinical[outcome_column])
                time2 = time.process_time()
                print("...took {:.3f}. Now plotting.".format(time2 - time1))
                measures = lasso.measure_fit()
                nonzero = np.nonzero(lasso.coef_)[0]  # feature coordinates

                print("Lasso fit for alpha={:.2f}:".format(alpha), file=file)
                print("   Number of coefficients: {:5d}".format(lasso.model_dimensions()), file=file)
                print("   R²: {:.5f}".format(measures["score"]), file=file)
                print("   Coefficients:", file=file)
                for index in nonzero:
                    print(16 * " " + data.columns[index] + ":{:16.5f}".format(lasso.coef_[index]), file=file)

                lasso.plot(**cycler.next_args())

            plt.legend(loc='best')
            plt.xlabel("Actual survival time (d)")
            plt.ylabel("Predicted survival time (d)")
            plt.savefig(directory + "/plot-" + outcome_column + alpha_range_string + ".pdf")
            plt.close()
            file.close()

    def explore_alphas(self, data, clinical, columns):

        # alphas = np.round(np.geomspace(1, 100000, 25))
        alphas = [1, 100, 250, 500, 750, 1000, 1500, 2000, 5000, 7500, 10000,
                  20000, 50000, 75000, 100000, 500000, 1000000]
        self.explore_fit(data, clinical, columns, alphas)

        alphas = [5000, 7500, 10000, 15000, 20000, 25000, 30000, 40000, 50000]
        self.explore_fit(data, clinical, columns, alphas)

    def print_for_survival_analysis(self, data: pd.DataFrame, clinical: pd.DataFrame, outcome_columns, alpha):
        directory = prepare_directory("../fit/lasso")
        values = clinical
        for outcome_column in outcome_columns:
            lasso = self.lasso_class(data, alpha)
            lasso.fit(clinical[outcome_column])
            lasso.measure_fit()
            series = pd.Series(lasso.current_prediction, index=values.index)
            values.loc[:, outcome_column + "-prediction"] = series
        values.to_csv(directory + "/clinical-alpha-{:d}.csv".format(alpha), sep=';')

    def explore_in_sample(self, bpdata, columns):
        self.explore_alphas(bpdata.data, bpdata.clinical, columns)
        self.print_for_survival_analysis(bpdata.data, bpdata.clinical, columns, 10000)
        self.print_for_survival_analysis(bpdata.data, bpdata.clinical, columns, 75000)


if __name__ == "__main__":
    print("Loading data...")
    bpdata = BiopredictSourceData().dataset()

    columns = bpdata.regression_columns()
    BiopredictLassoExplorer().explore_in_sample(bpdata, columns)






