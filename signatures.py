import os
import pandas as pd
import numpy as np

from pandas.util import hash_pandas_object
from joblib import Parallel, delayed
from sklearn.svm import SVR, NuSVR
from sklearn.metrics import mean_squared_error

import biopredict
from transformers import StoringSafeTransformer, gmean, make_dataframe_pipeline


class SignaturesExtractor(StoringSafeTransformer):

    storage_mode = "dataframe"

    def __init__(self,
                 aggregation="mean"):

        def signature_definitions():
            with os.scandir("../signatures/genes") as it:
                for entry in it:
                    if entry.name.endswith(".csv"):
                        yield pd.read_csv(entry.path)

        self.signature_definitions = pd.concat(list(signature_definitions()), axis=0)
        self.signatures = pd.DataFrame()
        self.aggregation = aggregation

    def fit_data(self):
        return self.signatures

    def set_fit_data(self,
                     params):
        self.signatures = params

    def fit(self, X, y=None):
        self.signatures = self.signature_definitions[self.signature_definitions["gene"].isin(X.columns)]
        return self

    def features(self):
        return self.signatures["gene"].unique()

    def transform(self, X):
        features = self.signatures["type"].unique()
        df = pd.DataFrame(index=X.index, columns=features)
        for feature in features:
            genes = self.signatures.loc[self.signatures["type"] == feature, "gene"]
            subspace = X[genes]
            if self.aggregation == "gmean":
                value = subspace.apply(gmean, axis=1)
            elif self.aggregation == "euclidian":
                # not really suitable because vector dimensions differ
                value = np.linalg.norm(subspace, ord=2, axis=1)
            else:
                value = subspace.mean(axis=1)
            df[feature] = value
        return df


def _cell_composition_deconvolution(X, matrix, sample_idx):
    nus = [0.25, 0.5, 0.75]
    svr = NuSVR(kernel="linear")

    def coefficients_and_rmse(target):
        for nu in nus:
            print("nu", nu)
            svr.nu = nu
            svr.fit(matrix, target)
            # (1, n_SV) x (n_SV, n_cell_types) = (1, n_cell_types)
            coefficients = np.dot(svr.dual_coef_, svr.support_vectors_).squeeze()
            # (n_genes, n_cell_types,) x (n_cell_types,) = (n_genes,)
            target_pred = np.dot(matrix, coefficients)
            rmse = mean_squared_error(target, target_pred)
            # set negative values to zero and normalize remaining ones
            coefficients = np.maximum(coefficients, 0)
            coefficients = coefficients / coefficients.sum()
            yield coefficients, rmse

    # slice one sample and the subset of genes found in matrix, (n_genes, )
    target = X.loc[sample_idx, matrix.index]
    # get coefficients for different nus and their rmse and select the one with the lowest
    coefficients_candidates, rmses = (zip(*coefficients_and_rmse(target)))
    return pd.Series(coefficients_candidates[np.argmin(rmses)],
                     index=matrix.columns,
                     name=sample_idx)


class CellCompositionTransformer(StoringSafeTransformer):

    storage_mode = "dataframe"

    def __init__(self,
                 source="newman",
                 n_jobs=1,
                 from_cache=False,
                 to_cache=False,
                 verbose=False,
                 unfiltered_data=None):
        self.source = source
        self.source_matrix = pd.read_csv("../signatures/expression/{}.csv".format(source),
                                         index_col=0)
        self.matrix = pd.DataFrame()
        self.unfiltered_data = unfiltered_data

        self.n_jobs = n_jobs
        self.from_cache = from_cache
        self.to_cache = to_cache

        self.verbose=verbose

    def fit_data(self):
        return self.matrix

    def set_fit_data(self,
                     params):
        self.matrix = params

    def cache_path(self, X):
        hash_series = hash_pandas_object(X)
        hash_ = 0
        for row_hash in hash_series:
            hash_ ^= row_hash
        return "../signatures/expression/{}-{}-cached.feather".format(self.source, hash_)

    def load_cached_transformation(self, X):
        file = self.cache_path(X)
        if os.path.exists(file):
            if self.verbose:
                print("For data with dim {}: cache file {}".format(X.shape, file))
            cached = pd.read_feather(file).set_index("index")
            if X.index.isin(cached.index).all():
                if self.verbose:
                    print("Cell decomposition: using precomputed results")
                return cached.loc[X.index]
            else:
                print("Cannot use cached version because indexes do not match")
                exit()
        return None

    def fit(self, X, y=None):
        # signature matrix subset (n_genes, n_cell_types)
        self.matrix = self.source_matrix.loc[self.source_matrix.index.isin(X.columns)]
        if self.verbose:
            print("Genes available in data: {} of {}".format(self.matrix.shape[0], self.source_matrix.shape[0]))
        return self

    def transform(self, X):
        if self.from_cache:
            assert self.unfiltered_data is not None
            cached = self.load_cached_transformation(X)
            if cached is None and self.unfiltered_data is not None:
                cached = self.load_cached_transformation(self.unfiltered_data)
                cached = cached.loc[X.index]
            if cached is not None:
                return cached

        series_list = Parallel(n_jobs=self.n_jobs)(delayed(_cell_composition_deconvolution)
                                                   (X, self.matrix, sample)
                                                   for sample in X.index)
        result = pd.concat(series_list, axis=1).T.reindex(index=X.index)
        if self.to_cache:
            result.reset_index().to_feather(self.cache_path(X))
            print("Saved result to cache")
        return result


def cache_cell_composition(recompute,
                           n_jobs):

    dset = biopredict.dataset("rna", "learn", remove_correlating=False)
    # if recompute is True, will not load from cache but recompute in any case
    transformer = CellCompositionTransformer(to_cache=True,
                                             from_cache=not recompute,
                                             n_jobs=n_jobs)
    print("Caching 'learn' dataset, dim {}".format(dset.data.shape))
    transformer.fit_transform(dset.data)

    dset = biopredict.dataset("rna", "validate", remove_correlating=False)
    transformer = CellCompositionTransformer(to_cache=True,
                                             from_cache=not recompute,
                                             n_jobs=n_jobs)
    print("Caching 'validate' dataset, dim {}".format(dset.data.shape))
    transformer.fit_transform(dset.data)


if __name__ == "__main__":
    import argparse as ap
    parser = ap.ArgumentParser(description="Cell type signatures")
    parser.add_argument("--cache-composition", action="store_true")
    parser.add_argument("--recompute", action="store_true")
    parser.add_argument("--jobs", default=1, type=int)
    args, leftovers = parser.parse_known_args()
    if args.cache_composition:
        cache_cell_composition(args.recompute, args.jobs)
        exit()

    from sklearn.preprocessing import StandardScaler
    dset = biopredict.dataset("rna", remove_correlating=False)
    if False:
        pl1 = make_dataframe_pipeline(StandardScaler(), SignaturesExtractor())
        pl2 = make_dataframe_pipeline(SignaturesExtractor(aggregation="gmean"), StandardScaler())
        print(pd.concat([pl.fit_transform(dset.data)["B lineage"] for pl in [pl1, pl2]], axis=1))
    if True:
        pl = make_dataframe_pipeline(CellCompositionTransformer(from_cache=True),
                                     StandardScaler())
        print(pl.fit_transform(dset.data).head())