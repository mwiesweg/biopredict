

# If running in a headless environment, force a safe matplotlib backend
def import_matplotlib():
    import os
    if "DISPLAY" not in os.environ:
        import matplotlib
        matplotlib.use('Agg')
# must be called after import in each module that is run as a script!
import_matplotlib()

from itertools import cycle
from matplotlib import lines, markers
from seaborn import color_palette
from typing import Dict
import math


class PlotCycler:

    def __init__(self, num_features: int, colormap_name: str = "husl"):
        self.palette = color_palette(colormap_name, num_features)
        self.color_cycler = cycle(self.palette)
        self.line_cycler = cycle(list(lines.lineStyles.keys())[:-3])
        self.marker_cycler = cycle(list(markers.MarkerStyle.markers.keys())[:-4])  # last four are "None"-equivalent

    def next_args(self) -> Dict:
        return dict(
            color=next(self.color_cycler),
            marker=next(self.marker_cycler),
            linestyle=next(self.line_cycler)
        )


def format_for_float(f: float, minimum_precision: int):
    min_precision = math.ceil(abs(math.log(f, 10)))
    precision = max(min_precision, minimum_precision)
    precision_as_string = "{:d}".format(precision)
    return "{:." + precision_as_string + "f}"
