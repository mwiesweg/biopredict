# From a fork of sklearn
# https://github.com/maniteja123/scikit-learn
# with my own fixes to make it work, and removal of unneeded features

# Authors: Alexandre Gramfort <alexandre.gramfort@inria.fr>
#          Mathieu Blondel <mathieu@mblondel.org>
#          Olivier Grisel <olivier.grisel@ensta.org>
#          Andreas Mueller <amueller@ais.uni-bonn.de>
#          Eric Martin <eric@ericmart.in>
#          Giorgio Patrini <giorgio.patrini@anu.edu.au>
# License: BSD 3 clause

import os
import itertools
import json
import numpy as np
import pandas as pd
from typing import Sequence, Tuple, Union
from scipy import stats
from sklearn.preprocessing import StandardScaler
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import Pipeline, FeatureUnion, _name_estimators
from sklearn.utils import check_array
from sklearn.utils.validation import FLOAT_DTYPES
# from sklearn.externals import joblib
import joblib

from fileutilities import prepare_directory


class DataFrameSafeTransformerMixin:
    pass


def is_dataframe_or_series(x):
    return isinstance(x, pd.DataFrame) or isinstance(x, pd.Series)


def require_dataframe(x, errmsg=None):
    if not isinstance(x, pd.DataFrame):
        if not errmsg:
            errmsg = "Requiring a dataframe as argument to the transformer"
        raise ValueError(errmsg)


def require_dataframe_or_series(x, errmsg=None):
    if not is_dataframe_or_series(x):
        if not errmsg:
            errmsg = "Requiring a dataframe or series as argument to the transformer"
        raise ValueError(errmsg)


def is_estimator_and_no_transformer(obj):
    return isinstance(obj, BaseEstimator) and not isinstance(obj, TransformerMixin)


class DataFrameTransformer(BaseEstimator, TransformerMixin, DataFrameSafeTransformerMixin):
    """
    Wraps another transformer, preserving the series/data frame type of X
    """
    def __init__(self,
                 transformer):
        self.transformer = transformer

    def _generate_transformer_class_generator(self):

        def generator(**constr_params):
            constr_params.pop("_transformer_class_generator", None)
            return self.transformer.__class__(**constr_params)

        return generator

    def fit(self, X, y=None, **fit_params):
        if y is None:
            self.transformer.fit(X, **fit_params)
        else:
            self.transformer.fit(X, y, **fit_params)
        return self

    def transform(self, X):
        require_dataframe_or_series(X, "DataFrameTransformer expects a series or a data frame to transform")

        transformed = self.transformer.transform(X)

        # did the transformer preserve dataframeness?
        if is_dataframe_or_series(transformed):
            return X

        # no, it converted to an ndarray
        if isinstance(X, pd.DataFrame):
            return pd.DataFrame(transformed, index=X.index, columns=X.columns)
        else:
            return pd.Series(transformed, index=X.index, name=X.name)

    def get_params(self, deep=True):
        return {"transformer": self.transformer}

    def set_params(self, **params):
        self.transformer = params["transformer"]

    def __repr__(self):
        return "DataFrame-safe wrapped " + self.transformer.__repr__()


def make_dataframe_transformer(transformer):
    if transformer is None:
        return None
    if isinstance(transformer, DataFrameSafeTransformerMixin):
        return transformer
    return DataFrameTransformer(transformer)


def make_dataframe_pipeline(*steps, **kwargs):

    def dataframe_safe():
        # wrap all transformers, but not the final estimator if it is no transformer
        for name, step in _name_estimators(steps):
            if is_estimator_and_no_transformer(step):
                yield name, step
            else:
                yield name, make_dataframe_transformer(step)

    return Pipeline(list(dataframe_safe()), **kwargs)


def is_dataframe_safe_transformer(transformer):
    # single transformer: must be safe (includes DataFrameFeatureUnion)
    if isinstance(transformer, DataFrameSafeTransformerMixin):
        return True

    # pipeline: all members must be safe, except for the last element, if it is only an estimator
    if isinstance(transformer, Pipeline):
        for index, (name, step) in enumerate(transformer.steps):
            if index == len(transformer.steps)-1 and is_estimator_and_no_transformer(step):
                return True
            if not is_dataframe_safe_transformer(step):
                print("DataFrame safety check failed at {} {}".format(name, step))
                return False
        return True

    # all other cases: not safe
    return False


class DataFrameFeatureUnion(FeatureUnion, DataFrameSafeTransformerMixin):

    def __init__(self, transformer_list):

        for name, part in transformer_list:
            if not is_dataframe_safe_transformer(part):
                raise ValueError("DataframeFeatureUnion expects only data-frame safe transformers. "
                                 "Not satisfied by {} {}.".format(name, part))

        FeatureUnion.__init__(self,
                              transformer_list=transformer_list,
                              n_jobs=1,  # unsupported by us
                              transformer_weights=None)  # unsupported by us

    def fit_transform(self, X, y=None, **fit_params):

        def fitted_transformed():
            for name, trans in self.transformer_list:
                tr = trans.fit_transform(X, y, **fit_params)
                yield tr

        return pd.concat(list(fitted_transformed()), axis=1)

    def transform(self, X):

        def transformed():
            for name, trans in self.transformer_list:
                yield trans.transform(X)

        return pd.concat(list(transformed()), axis=1)


def make_dataframe_union(*transformers):
    return DataFrameFeatureUnion(_name_estimators(transformers))


class StoringTransformer:

    storage_mode = "json"
    storage_id = ""

    def fit_data(self):
        raise NotImplementedError()

    def set_fit_data(self,
                     params):
        raise NotImplementedError()


class StoringSafeTransformer(BaseEstimator, TransformerMixin, StoringTransformer, DataFrameSafeTransformerMixin):
    pass


class WrappingEstimator(BaseEstimator):

    def __init__(self,
                 estimator):
        self.estimator = estimator

    def fit(self, X, y, **fit_params):
        return self.estimator.fit(X, y, **fit_params)

    def score(self, X, y):
        return self.estimator.score(X, y)

    def predict(self, X):
        return self.estimator.predict(X)

    # there is no useful redirection of set/get_params because parameters
    # must have direct correspondance with explicit constructor arguments


class ModelPersistence:

    def __init__(self, directory):
        self.directory = directory

    def _filename(self, folder, transformer, suffix):
        id_ = transformer.storage_id
        if not id_:
            id_ = type(transformer).__name__
        return folder + "/" + id_ + "." + suffix

    def _save_json(self, params, folder, transformer):
        with open(self._filename(folder, transformer, "json"), "w") as f:
            json.dump(params, f)

    def _load_json(self, folder, transformer):
        with open(self._filename(folder, transformer, "json"), "r") as f:
            return json.load(f)

    def _save_dataframe(self, params, folder, transformer):
        params.to_csv(self._filename(folder, transformer, "csv"))

    def _load_dataframe(self, folder, transformer):
        return pd.read_csv(self._filename(folder, transformer, "csv"), index_col=0)

    def _save_transformer(self, transformer, folder):
        params = transformer.fit_data()

        if transformer.storage_mode == "json":
            print("Saving", transformer)
            self._save_json(params, folder, transformer)
        elif transformer.storage_mode == "dataframe":
            self._save_dataframe(params, folder, transformer)
        else:
            raise ValueError("Unimplemented storage mode " + transformer.storage_mode)

    def _load_transformer(self, transformer, folder):
        if transformer.storage_mode == "json":
            params = self._load_json(folder, transformer)
        elif transformer.storage_mode == "dataframe":
            params = self._load_dataframe(folder, transformer)
        else:
            raise ValueError("Unimplemented storage mode " + transformer.storage_mode)
        transformer.set_fit_data(params)

    def store(self,
              pl,
              metadata=None):
        prepare_directory(self.directory)

        def save_params(transformer, save_directory):
            print("Saving {} in {}".format(transformer, save_directory))
            if isinstance(transformer, StoringTransformer):
                self._save_transformer(transformer, save_directory)
                return None
            elif isinstance(transformer, DataFrameTransformer) and isinstance(transformer.transformer,
                                                                              StoringTransformer):
                self._save_transformer(transformer.transformer, save_directory)
                return None
            elif isinstance(transformer, FeatureUnion) or isinstance(transformer, Pipeline):
                if isinstance(transformer, FeatureUnion):
                    tuples = transformer.transformer_list
                else:
                    tuples = transformer.named_steps.items()
                for name, nested_transformer in tuples:
                    nested_estimator = save_params(nested_transformer, save_directory)
                    if nested_estimator is not None:
                        return nested_estimator
            elif is_estimator_and_no_transformer(transformer) or transformer is None:
                return transformer
            else:
                raise ValueError("Estimator ", transformer, " is no storing transformer, pipeline, feature union"
                                                            " nor a final estimator")

        estimator = save_params(pl, self.directory)
        if isinstance(estimator, WrappingEstimator):
            estimator = estimator.estimator

        # pickles None if estimator is None, and that is fine
        joblib.dump(estimator, self.directory + "/estimator.pkl")

        if metadata is not None:
            with open(self.directory + "/metadata.json", "w") as f:
                json.dump(metadata, f)

    def load_pipeline(self, create_pipeline_func):

        def load_params(transformer, save_directory):
            if isinstance(transformer, StoringTransformer):
                self._load_transformer(transformer, save_directory)
            elif isinstance(transformer, DataFrameTransformer) and isinstance(transformer.transformer,
                                                                              StoringTransformer):
                self._load_transformer(transformer.transformer, save_directory)
            elif isinstance(transformer, FeatureUnion) or isinstance(transformer, Pipeline):
                if isinstance(transformer, FeatureUnion):
                    tuples = transformer.transformer_list
                else:
                    tuples = transformer.named_steps.items()
                for name, nested_transformer in tuples:
                    load_params(nested_transformer, save_directory)
            elif is_estimator_and_no_transformer(transformer) or transformer is None:
                return
            else:
                raise ValueError("Estimator ", transformer, " is no storing transformer, pipeline, feature union"
                                                          " nor a final estimator")

        estimator = joblib.load(self.directory + "/estimator.pkl")

        pl = create_pipeline_func(estimator)
        load_params(pl, self.directory)
        return pl

    def load_metadata(self):
        metadata_file = self.directory + "/metadata.json"
        if os.path.exists(metadata_file):
            with open(metadata_file, "r") as f:
                return json.load(f)
        else:
            return None

    @staticmethod
    def load(storage_directory,
             pipeline_creation_func):
        persistence = ModelPersistence(storage_directory)
        metadata = persistence.load_metadata()
        pipeline = persistence.load_pipeline(lambda estimator: pipeline_creation_func(metadata, estimator))
        return metadata, pipeline


class StoringStandardScaler(StandardScaler, StoringTransformer):

    storage_mode = "dataframe"

    def fit_data(self):
        return pd.DataFrame({
            "scale": self.scale_,
            "mean": self.mean_
        })

    def set_fit_data(self,
                     params: pd.DataFrame):
        self.mean_ = params["mean"].values
        self.scale_ = params["scale"].values


class LabTransformer(StoringTransformer, BaseEstimator, TransformerMixin):

    storage_mode = "dataframe"

    def __init__(self):
        super().__init__()
        self.mean_ = None
        self.scale_ = None
        self.log_ = None
        self.min_ = None

    normal_range = {
        # List of 4 elements
        # 0: distribution type
        # 1: LLN
        # 2: ULN
        # 3: minimum value to use if measured value is 0 and 0 is not acceptable for mathematical reasons
        "Bilirubin": ["lognormal", 0.3, 1.2, 0.1],
        "CRP": ["lognormal", 0.1, 0.5, 0.1],
        "GOT": ["lognormal", 1, 50, 1],
        "GPT": ["lognormal", 1, 50, 1],
        "LDH": ["lognormal", 100, 247, 1],

        "Calcium": ["normal", 2.08, 2.65, 0.01],
        "Ges.-Eiweiß": ["normal", 6.4, 8.3, 0.1],
        "Hämoglobin": ["normal", 11.6, 17.2, 0.1],
        "Natrium": ["normal", 136, 145, 1],
        "Kalium": ["normal", 3.5, 5.1, 0.1],
        "Leukozyten": ["normal", 3.6, 9.2, 0.01],
        "Lymphozyten": ["normal", 1.0, 3.4, 0.01],
        "Monozyten": ["normal", 0.2, 0.8, 0.01],
        "Neutrophile": ["normal", 1.7, 6.2, 0.01],
        "Thrombozyten": ["normal", 180, 380, 1]
    }

    def fit(self, X, y=None):
        require_dataframe(X, "Need a dataframe, not an ndframe, to read the column names!")

        self.mean_ = pd.Series(0., index=X.columns)
        self.scale_ = pd.Series(0., index=X.columns)
        self.log_ = pd.Series(False, index=X.columns)
        self.min_ = pd.Series(np.NaN, index=X.columns)

        for col in X.columns:
            if col not in self.normal_range:
                raise NotImplementedError("Missing lab type: " + col)

            params = self.normal_range[col]

            lln = params[1]
            uln = params[2]
            self.log_[col] = params[0] == "lognormal"
            self.min_[col] = params[3]

            if self.log_[col]:
                lln = np.log(lln)
                uln = np.log(uln)

            quant = 1.959964  # qnorm(0.975)
            mu = np.mean([lln, uln])
            sd = np.mean([mu - lln, uln - mu]) / quant
            self.mean_[col] = mu
            self.scale_[col] = sd
        return self

    def fit_data(self):
        return pd.DataFrame({
            "mean": self.mean_,
            "scale": self.scale_,
            "log": self.log_,
            "min": self.min_
        })

    def set_fit_data(self,
                     params: pd.DataFrame):
        self.mean_ = params["mean"]
        self.scale_ = params["scale"]
        self.log_ = params["log"]
        self.min_ = params["min"]


class LabAvoidZeroTransformer(LabTransformer, DataFrameSafeTransformerMixin):

    def transform(self, X):
        return X.copy().apply(lambda series: series.replace(0, self.min_[series.name]))


class LabStandardScaler(LabTransformer, DataFrameSafeTransformerMixin):

    def transform(self, X):

        def scale(series):
            if self.log_[series.name]:
                series = np.log(series.replace(0, self.min_[series.name]))
            series -= self.mean_[series.name]
            series /= self.scale_[series.name]
            return series

        return X.copy().apply(scale)

    def inverse_transform(self, X):
        # Note: inverse transform does not restore 0 of values on log-scale, but keeps the minimum

        def inverse_scale(series):
            series *= self.scale_[series.name]
            series += self.mean_[series.name]
            if self.log_[series.name]:
                series = np.exp(series)
            return series

        return X.copy().apply(inverse_scale)


class AllRatiosTransformer(BaseEstimator, TransformerMixin, DataFrameSafeTransformerMixin, StoringTransformer):

    def __init__(self):
        self.combinations = None

    def fit(self, X, y=None):

        self.combinations = [(a,b)
                             for a,b in itertools.combinations_with_replacement(X.columns, 2)
                             if a != b]
        return self

    def transform(self, X):
        new_col_names = [a + "/" + b for a,b in self.combinations]
        df = pd.DataFrame(0.0, index=X.index, columns=new_col_names)
        for (a, b), col in zip(self.combinations, new_col_names):
            if np.any(X[b] == 0):
                print("AllRatios: {} zeros in denominator {}".format(np.sum(X[b] == 0), b))
            df[col] = X[a] / X[b]
        return df

    def fit_data(self):
        return self.combinations

    def set_fit_data(self,
                     params):
        self.combinations = params


class FeatureSubset(StoringSafeTransformer):

    def __init__(self,
                 subset=None):
        self.subset = subset

    def fit_data(self):
        return self.subset

    def set_fit_data(self,
                     params):
        self.subset = params

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        require_dataframe(X)
        if self.subset:
            X = X[self.subset]
        return X

    def features(self):
        return self.subset


def _boxcox_lambda(x):
    # candidate space for lambda, all use [-2; 2]
    optim_space = np.arange(-2, 2, 0.05)
    # create vectorized function of log-likelihood function
    vec_llf = np.frompyfunc(lambda lmd: stats.boxcox_llf(lmd, x), 1, 1)
    # compute log-likelihood over candidate space
    ll = vec_llf(optim_space)
    # pick lambda with largest log-likelihood
    return optim_space[np.argmax(ll)]


class BoxCoxTransformer(BaseEstimator, TransformerMixin, StoringTransformer):
    """BoxCox transformation on individual features.
    Boxcox transform wil be applied on each feature (each column of
    the data matrix) with lambda evaluated to maximise the log-likelihood
    Parameters
    ----------
    copy : boolean, optional, default True
        Set to False to perform inplace computation.
    Attributes
    ----------
    lambdas_ : array of float, shape (n_transformed_features,)
        The parameters of the BoxCox transform for the selected features.
    n_features_ : int
        Number of features in input during fit
    Notes
    -----
    The Box-Cox transform is given by::
        y = (x ** lmbda - 1.) / lmbda,  for lmbda > 0
            log(x),                     for lmbda = 0
    ``boxcox`` requires the input data to be positive.
    """
    def __init__(self, copy=True):
        self.lambdas_ = None
        self.n_features_ = 0
        self.copy = copy

    storage_mode = "dataframe"

    def fit_data(self):
        return pd.DataFrame({"boxcoxtransformer__lambdas": self.lambdas_})

    def set_fit_data(self,
                     params: pd.DataFrame):
        self.lambdas_ = params["boxcoxtransformer__lambdas"].values
        self.n_features_ = self.lambdas_.shape[0]

    def fit(self, X, y=None):
        """Estimate lambda for each feature to maximise log-likelihood
        Parameters
        ----------
        X : array-like, shape [n_samples, n_features]
            The data to fit by apply boxcox transform,
            to each of the features and learn the lambda.
        Returns
        -------
        self : object
            Returns self.
        """
        X = check_array(X, ensure_2d=True, dtype=FLOAT_DTYPES)
        self.n_features_ = X.shape[1]
        self.lambdas_ = np.apply_along_axis(_boxcox_lambda, 0, X)
        return self

    def transform(self, X):
        """Transform each feature using the lambdas evaluated during fit time
        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The data to apply boxcox transform.
        Returns
        -------
        X_tr : array-like, shape (n_samples, n_features)
            The transformed data.
        """
        X = check_array(X, ensure_2d=True, dtype=FLOAT_DTYPES, copy=self.copy)
        if X.shape[1] != self.n_features_:
            raise ValueError("X has a different shape than during fitting.")
        for i in range(self.n_features_):
            X[:, i] = stats.boxcox(X[:, i], self.lambdas_[i])
        return X

    def inverse_transform(self, X):
        """Invere transform each feature using the lambdas evaluated during fit time
        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The transformed data after boxcox transform.
        Returns
        -------
        X_inv : array-like, shape (n_samples, n_features)
            The original data.
        Notes
        -----
        The inverse Box-Cox transform is given by::
        y = log(x * lmbda + 1.) / lmbda,  for lmbda > 0
            exp(x),                       for lmbda = 0
        """

        inv = check_array(X, ensure_2d=True, dtype=FLOAT_DTYPES, copy=True)

        # for lambda != 0
        mask = self.lambdas_ != 0
        mask_lambdas = self.lambdas_[mask]
        inv_masked = inv[:, mask]
        inv_masked *= mask_lambdas
        np.log1p(inv_masked, out=inv_masked)
        inv_masked /= mask_lambdas
        np.exp(inv_masked, out=inv_masked)
        inv[:, mask] = inv_masked

        # for lambda == 0
        mask = self.lambdas_ == 0
        inv[:, mask] = np.exp(inv[:, mask])
        return inv


def gmean(a, axis=0, dtype=None, zero_sub=0):
    if zero_sub != 0:
        a = a.copy()
        a[a == 0] = zero_sub
    return stats.gmean(a, axis=axis, dtype=dtype)


class NCounterTransformer(StoringSafeTransformer):

    def __init__(self,
                 method: str=None,
                 reference_genes: Sequence[str]=None):
        self.params = {}

        if (method == "ref" or method == "ref-only") and not reference_genes:
            raise ValueError("You must give reference genes for normalization method \"{}\"".format(method))

        self.params["method"] = method if method else ("ref" if reference_genes else "global")
        self.params["reference_genes"] = reference_genes

    def fit_data(self):
        return self.params

    def set_fit_data(self,
                     params):
        self.params = params

    class DataContainer:
        def __init__(self, data: pd.DataFrame):
            self.data = data

        def set_max(self, m):
            self.data = self.data.clip(lower=m)

        def round(self, decimals=0):
            self.data = self.data.round(decimals=decimals)

        def pos_idc(self) -> pd.Index:
            return self.data.columns[self.data.columns.str.contains("POS_")]

        def neg_idc(self) -> pd.Index:
            return self.data.columns[self.data.columns.str.contains("NEG_")]

        def control_idc(self) -> pd.Index:
            return self.neg_idc().append(self.pos_idc())

        def pos_gmeans(self) -> pd.Series:
            return self.gmean(self.pos_idc())

        def neg_means_sds(self) -> Tuple[pd.Series, pd.Series]:
            neg = self.data.loc[:, self.neg_idc()]
            return neg.mean(axis=1), neg.std(axis=1)

        def gmean(self, idc) -> pd.Series:
            return self.data.loc[:, idc].apply(gmean, axis=1)

        def genes_with_highest_abundance(self, top_n: int) -> Sequence[str]:
            data_without_control = self.data_without_control()
            gene_gmeans = data_without_control.apply(gmean, axis=0, zero_sub=1)
            return gene_gmeans.sort_values(ascending=False).index[:top_n].values.tolist()

        def data_without_control(self) -> pd.DataFrame:
            data_without_control = self.data.drop(self.control_idc(), axis=1)  # type: pd.DataFrame
            return data_without_control

        def mul_columns(self, series: pd.Series):
            self.data = self.data.mul(series, axis=0)

        def subtract_background(self, background: pd.Series):
            self.data = np.maximum(1, self.data.sub(background, axis=0))

        def drop_columns(self, index: Union[pd.Index, Sequence[str]]):
            self.data = self.data.drop(index, axis=1)

    def fit(self, X, y=None):
        container = self.DataContainer(X)
        self.params["overall_pos_gmean"] = gmean(container.pos_gmeans())

        if self.params["method"] == "global":
            self.params["overall_highest_abundance_genes"] = container.genes_with_highest_abundance(50)
            sample_gmeans = container.gmean(self.params["overall_highest_abundance_genes"])
            self.params["overall_highest_abundance_gmean"] = gmean(sample_gmeans)
        elif self.params["method"] in ["ref", "ref-only"]:
            sample_gmeans = container.gmean(self.params["reference_genes"])
            self.params["overall_max_gmean"] = sample_gmeans.max()
        return self

    def transform(self, X) -> pd.DataFrame:
        container = self.DataContainer(X)

        # 0 -> 1
        container.set_max(1)

        # Step 1: Positive Control Normalization using the geometric mean
        if self.params["method"] not in ["ref-only"]:
            pos_gmeans_ratio = self.params["overall_pos_gmean"] / container.pos_gmeans()
            container.mul_columns(pos_gmeans_ratio)
            container.drop_columns(container.pos_idc())

        # Step 2: Housekeeping gene or global normalization
        if self.params["method"] == "global":
            sample_gmeans = container.gmean(self.params["overall_highest_abundance_genes"])
            overall_to_sample_highest_abundance_genes_ratio = self.params["overall_highest_abundance_gmean"] / sample_gmeans
            container.mul_columns(overall_to_sample_highest_abundance_genes_ratio)
        elif self.params["method"] in ["ref", "ref-only"]:
            sample_gmeans = container.gmean(self.params["reference_genes"])
            overall_to_sample_refgenes_ratio = self.params["overall_max_gmean"] / sample_gmeans
            container.mul_columns(overall_to_sample_refgenes_ratio)
            container.drop_columns(self.params["reference_genes"])
        elif self.params["method"] == "none":
            pass
        else:
            raise ValueError("Unsupported method ", self.params["method"])

        # Step 3: Assessing background using the negative control probes
        if self.params["method"] not in ["ref-only"]:
            neg_means, neg_sds = container.neg_means_sds()
            background = neg_means + 2*neg_sds  # type: pd.Series
            container.subtract_background(background)

        container.round()
        container.set_max(1)

        return container.data_without_control()


if __name__ == "__main__":
    from dataloading import BiopredictSourceData
    from utilities import StopWatch
    import cProfile
    dataset = BiopredictSourceData().dataset()

    watch = StopWatch()
    transform = BoxCoxTransformer()

    cProfile.run("transform.fit(dataset.data)")
    cProfile.run("transform.transform(dataset.data)")

    iterations = 20
    for i in range(iterations):
        print("Round {}".format(i))
        transform.fit(dataset.data)
        transform.transform(dataset.data)
    watch.print_elapsed(n_iterations=iterations)