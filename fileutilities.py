import os


def prepare_directory(directory, *args):
    directory = directory.format(*args)
    if not os.path.exists(directory):
        os.makedirs(directory)
    return directory

