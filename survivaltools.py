import pandas as pd
import numpy as np
from typing import Dict, List, Tuple, Union

import biopredict_r as R
from numericutilities import SeriesRandomizer, DataFrameRandomizer

from transformers import WrappingEstimator

from sklearn.metrics import mean_squared_error


class SurvivalTransformer:

    def __init__(self,
                 drop_censoring_column: bool = True,
                 log_transform: bool = False,
                 score_method: str = "iAUC",
                 imputation: str = None,
                 imputations_params: Dict = None):
        self.drop_censoring_column = drop_censoring_column
        self.log_transform = log_transform
        self.score_method = score_method
        self.imputation = imputation
        self.imputation_params = imputations_params
        self.needed_columns = []
        self.survival = None

        if self.imputation == "gamma":
            if not {"strata", "dco", "informative_censoring"} <= self.imputation_params.keys():
                raise ValueError("Need the following imputation params: strata, dco, informatice_censoring, optional: m")
            if "m" not in self.imputation_params:
                self.imputation_params["m"] = 1
            # parameters containing lists of columns
            self.needed_columns += [item
                                    for list_field in ["strata"]
                                    for item in self.imputation_params[list_field]]
            # parameters giving columns names
            self.needed_columns += [self.imputation_params[field] for field in ["dco", "informative_censoring"]]
        elif self.imputation:
            raise NotImplementedError("Unsupported imputation type " + self.imputation)

    def __getstate__(self):
        # self.survival is not pickle'able
        dict_ = self.__dict__.copy()
        dict_["survival"] = None
        return dict_

    def columns_needed(self) -> List[str]:
        return self.needed_columns

    def imputation_count(self):
        if self.imputation_params:
            return self.imputation_params["m"]
        return 1

    def randomize(self,
                  survival_data: pd.DataFrame,
                  extra_data: pd.DataFrame = None,
                  ) -> Tuple[pd.DataFrame, pd.DataFrame]:
        survival_data_rand = pd.concat([
            SeriesRandomizer(survival_data.iloc[:, 0]).randomize_expon(round_=True, lower_clip=1),
            SeriesRandomizer(survival_data.iloc[:, 1]).randomize()
        ], axis=1)  # type: pd.DataFrame

        if self.imputation == "gamma":
            extra_data_rand = DataFrameRandomizer(extra_data).randomize(self.imputation_params["strata"] +
                                                                        [self.imputation_params["informative_censoring"]])
            dco_colname = self.imputation_params["dco"]
            # randomize the dco - survival time difference and add that to the new survival time
            extra_data_rand[dco_colname] = (extra_data[dco_colname]
                                            .sub(survival_data.iloc[:, 0])
                                            .pipe(SeriesRandomizer)
                                            .randomize_expon(round_=True)
                                            .add(survival_data_rand.iloc[:, 0])
                                            )
        else:
            extra_data_rand = extra_data

        return survival_data_rand, extra_data_rand

    # Transforms the given survival_data according to transformation parameters.
    # Some transformations (imputation) need extra data, which must then be given as required.
    # Returns a list of data frame: Normally, with length 1; if requested transformations
    # result in multiple results (imputation with multiple alternatives), contains the alternative
    # dataframes of identical shape.
    def transform(self,
                  survival_data: pd.DataFrame,
                  extra_data: pd.DataFrame) -> List[pd.DataFrame]:

        if len(survival_data.shape) != 2 or survival_data.shape[1] != 2:
            raise ValueError("Must have at least two-column frame of time, status")
        if any(not extra_data.columns.contains(c) for c in self.needed_columns):
            raise ValueError("Must have extra_data with columns " + ", ".join(self.needed_columns))

        if self.imputation == "gamma":
            data = pd.concat([survival_data, extra_data], axis=1)  # type: pd.DataFrame
            if not self.survival:
                self.survival = R.Survival()
            transformed = self.survival.gamma_impute(data,
                                                     survival_columns=list(data.columns[0:2]),
                                                     strata_columns=self.imputation_params["strata"],
                                                     dco_column=self.imputation_params["dco"],
                                                     informative_censoring_column=self.imputation_params["informative_censoring"],
                                                     m=self.imputation_params["m"]
                                                     )
        else:
            transformed = [survival_data.copy()]

        if self.log_transform:
            for _, df in enumerate(transformed):
                df.iloc[:, 0] = df.iloc[:, 0].transform(np.log1p).values

        if self.drop_censoring_column:
            transformed = list(map(lambda df: df.iloc[:, 0], transformed))

        return transformed

    def inverse_transform(self,
                          transformed_data: Union[pd.Series, pd.DataFrame]):
        if self.log_transform:
            transformed_data = np.exp(transformed_data)

        return transformed_data


class SurvivalEstimator(WrappingEstimator):

    def __init__(self,
                 estimator,
                 transformer: SurvivalTransformer,
                 survival_columns: List[str]):
        super().__init__(estimator)
        self.transformer = transformer
        self.survival_columns = survival_columns

        self.r = None  # generate on use

    def __getstate__(self):
        # self.r is not pickle'able
        dict_ = self.__dict__.copy()
        dict_["r"] = None
        return dict_

    def fit(self, X, y, **fit_params):
        if not type(y) is pd.DataFrame:
            raise ValueError("Expecting DataFrame y")
        if not self.r:
            self.r = R.Survival()

        survival_data = y[self.survival_columns]
        extra_data = y[self.transformer.needed_columns]
        # use first of M imputations
        y = self.transformer.transform(survival_data, extra_data)[0]
        return self.estimator.fit(X, y, **fit_params)

    def score(self, X, y):
        survival_data = y[self.survival_columns]
        prediction = self.predict(X)

        if self.transformer.score_method == "iAUC":
            if not self.r:
                self.r = R.Survival()
            return self.r.iauc(survival_data, prediction)
        elif self.transformer.score_method == "rmse":
            # highest is best, so loss must be negated
            return -np.sqrt(mean_squared_error(survival_data.iloc[:, 0], prediction))
        else:
            raise NotImplementedError("Metric {} is not implemented".format(self.transformer.score_method))

    def predict(self, X):
        return self.transformer.inverse_transform(self.estimator.predict(X))

    def get_params(self, deep=True):
        return {
            "transformer": self.transformer,
            "estimator": self.estimator,
            "survival_columns": self.survival_columns
        }

    def set_params(self, **params):
        # accept some params for myself, pass the rest to estimator
        estimator_params = {}
        for key, value in params.items():
            if key == "transformer":
                self.transformer = value
            elif key == "estimator":
                self.estimator = value
            elif key == "survival_columns":
                self.survival_columns = value
            else:
                estimator_params[key] = value

        if estimator_params:
            self.estimator.set_params(**estimator_params)

        return self


if __name__ == "__main__":
    import biopredict
    dataset = biopredict.dataset("rna")
    t = SurvivalTransformer(
        drop_censoring_column=True,
        log_transform=True,
        score_method="rmse",
        imputation="gamma",
        imputations_params={"strata": dataset.columns_for_type("covariates"),
                            "dco": dataset.columns_for_type("dco"),
                            "informative_censoring": dataset.columns_for_type("informative_censoring"),
                            "m": 5}
    )
    survival_data = dataset.metadata[dataset.survival_columns()[1]]
    extra_data = dataset.metadata[t.columns_needed()]
    dfs = t.transform(survival_data, extra_data)
    print(pd.concat(#[survival_data, extra_data] +
                    dfs, axis=1).head())
