import pandas as pd
import numpy as np
import biopredict
from biopredict_r import Survival


def count_and_rel(df, column):
    series = df[column]
    counts = series.value_counts(dropna=False, sort=False)
    perc = series.value_counts(normalize=True, dropna=False, sort=False)

    return pd.DataFrame([pd.Series({
        "field": column,
        "category": counts.index[idx],
        "value":  "{:d} ({:.1%})".format(counts.iloc[idx], perc.iloc[idx])
    }) for idx in range(len(counts))]).fillna("Missing")


def median_and_range(df, column):
    return pd.DataFrame({
        "field": column,
        "value": "{:.1f} ({:.1f} - {:.1f})".format(df[column].median(), df[column].min(), df[column].max())
    }, index=[0])


def median_survival(df, columns):
    series = Survival().survival_metadata(df[columns])
    return pd.DataFrame({
        "field": columns[0],
        "value": "{:.1f} ({:.1f} - {:.1f})".format(series["median"], series["Lower.CI"], series["Upper.CI"])
    }, index=[0])


def patient_characteristics(df: pd.DataFrame):
    fl = df['first line with IO']
    df["prior lines"] = np.where(np.isnan(fl), "Undocumented",
                                 np.where(fl == 2, "1",
                                          np.where(fl == 3, "2", "≥3")))
    tps = df["PD-L1 TPS"]
    df["PD-L1"] = np.where(np.isnan(tps), "Missing",
                           np.where(tps == 0, "Negative",
                                    np.where(tps <50, "<50%", "≥50%")))
    df["histology"] = np.where(df["entity"] == "NSCLC/A", "adenocarcinoma", "squamous/NOS")
    df.replace({"smoking status": "presumed"}, "former", inplace=True)
    return pd.concat([
        count_and_rel(df, col)
        for col in ["sex", "histology", "smoking status", "M",
                    "RAS Mutation", "EGFR Exon 19 & 21", "STK11", "PD-L1",
                    "prior lines", 'best response ternary']
    ] + [
        median_and_range(df, col)
        for col in ["age at dx"]
    ] + [
        median_survival(df, ["TTF IO", "TTF IO reached"])
    ], axis=0, sort=False).set_index(["field", "category"])


training = biopredict.dataset("rna", "learn")
test = biopredict.dataset("rna", "validate", remove_correlating=False)

result = pd.concat([
    patient_characteristics(training.metadata),
    patient_characteristics(test.metadata)
], axis=1, keys=["train", "test"], sort=False).sort_index()  # type: pd.DataFrame
result.to_csv("../pat characteristics.csv", sep=";", decimal=",")
