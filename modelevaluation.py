from typing import List, Dict, Iterable, Callable, ClassVar, Tuple
from itertools import product
import functools
from os import getpid

import numpy as np
import pandas as pd
import sklearn.metrics as metrics
import matplotlib.pyplot as plt
from joblib import Parallel, delayed

from linearregression import BiopredictLinearRegression
from logit import BiopredictLogitClassifier
from dataloading import DataProvider
from fileutilities import prepare_directory
from ensembletools import EnsembleSampler
from paperformat import DinA
from survivaltools import SurvivalTransformer


def plot_confusion_matrix(cm,
                          classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues,
                          colorbar=True):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    if colorbar:
        plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    thresh = cm.max() / 2.
    for i, j in product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')


class ModelEvaluation:

    def __init__(self,
                 bpdata: DataProvider,
                 features: List[str],
                 feature_scaler,
                 survival_transformer: SurvivalTransformer):
        self.bpdata = bpdata
        self.sampler = EnsembleSampler(bpdata, feature_scaler, survival_transformer)
        self.sampler.use_all()
        self.measurements = None
        self.coefficients = None
        self.measurements_list = []
        self.coefficients_list = []
        self.features = features
        self.clinical_output = bpdata.metadata.copy()
        self.confusion_matrices = {}
        self.directory = "../fit/evaluation"

    def reset(self):
        self.measurements = None
        self.coefficients = None
        self.measurements_list = []
        self.coefficients_list = []

    def __abstract_evaluate(self, regressor_class: ClassVar, outcome_columns: List[str], measures,
                            serieslist_from_prediction: Callable,
                            extra_function: Callable = None):
        regressor = regressor_class(self.sampler.sample)
        columnlabels = [self.bpdata.column_label(column) for column in outcome_columns]
        measurements = pd.DataFrame(np.NaN, index=measures.keys(), columns=columnlabels)
        coefficients = pd.DataFrame(0., index=self.features, columns=[])
        for column, columnlabel in zip(outcome_columns, columnlabels):
            self.sampler.set_outcome(column)
            truth = self.sampler.clinical_sample
            sample_control, truth_control = (self.sampler.sample, truth) if self.sampler.sample_control is None \
                else (self.sampler.sample_control, self.sampler.clinical_sample_control)
            regressor.fit_with_features(truth, self.features)
            prediction = regressor.prediction(sample_control)

            prediction = self.sampler.outcome_inverse_transform(prediction)
            truth_control = self.sampler.outcome_inverse_transform(truth_control)

            for name, method in measures.items():
                try:
                    measurements.loc[name, columnlabel] = method(truth_control, prediction)
                except ValueError as err:
                    print(err)
                    print(prediction)
            column_coefficients = regressor.coefficients()
            # ignore frames that are returned for multiclass classifiers
            if isinstance(column_coefficients, pd.Series):
                coefficients[columnlabel] = column_coefficients
            else:
                # this is an n_classes x n_features DataFrame.
                coefficients = pd.concat([coefficients, column_coefficients.add_prefix(columnlabel + " class ")], axis=1)
            try:
                self.clinical_output = pd.concat(
                    [self.clinical_output] + serieslist_from_prediction(prediction, columnlabel, truth), axis=1)
            except ValueError as err:
                print(err)
                continue
            if extra_function is not None:
                extra_function(prediction, column, truth_control)
        self.measurements_list.append(measurements)
        self.coefficients_list.append(coefficients)

    def evaluate_survival_regression(self, outcome_columns: List[str] = None):
        if outcome_columns is None:
            outcome_columns = self.bpdata.survival_columns()
        self.__abstract_evaluate(
            regressor_class=BiopredictLinearRegression,
            outcome_columns=outcome_columns,
            measures=
            {
                "neg_mean_absolute_error": metrics.mean_absolute_error,
                "neg_mean_squared_error": metrics.mean_squared_error,
                "neg_median_absolute_error": metrics.median_absolute_error,
                "r2": metrics.r2_score
            },
            serieslist_from_prediction=lambda prediction, column, truth: [
                pd.Series(prediction, index=truth.index)
                    .rename("predicted-" + column),
                pd.Series(pd.qcut(prediction, 4, duplicates="drop", labels=["Quartile 1","Quartile 2", "Quartile 3", "Quartile 4"]),
                          index=truth.index)
                    .rename("predicted-" + column + "-quartiles"),
                pd.Series(pd.qcut(prediction, 2, duplicates="drop", labels=["Below median", "Above median"]),
                          index=truth.index)
                    .rename("predicted-" + column + "-abovemedian")
            ],
        )

    def evaluate_classification(self, outcome_columns: List[str] = None):
        if outcome_columns is None:
            outcome_columns = self.bpdata.classification_columns()

        def add_confusion_matrix(prediction, column, truth):
            self.confusion_matrices[column] = metrics.confusion_matrix(truth, prediction)

        self.__abstract_evaluate(
            regressor_class=BiopredictLogitClassifier,
            outcome_columns=outcome_columns,
            measures=
            {
                "cohen_kappa_score": metrics.cohen_kappa_score,
                "accuracy": metrics.accuracy_score,
                "precision": functools.partial(metrics.precision_score, average="micro"),
                "recall": functools.partial(metrics.recall_score, average="micro"),
                "f1_score": functools.partial(metrics.f1_score, average="micro")
            },
            serieslist_from_prediction=lambda prediction, column, truth:
            [pd.Series(prediction, index=truth.index).rename("predicted-" + column)],
            extra_function=add_confusion_matrix
        )

    def print(self, subdir: str):
        if self.measurements is None:
            self.__results_to_df()
        directory = prepare_directory(self.directory + "/" + subdir)
        with open(directory + "/features", "w") as f:
            for feature in self.features:
                f.write(feature + "\n")
        self.measurements.to_csv(directory + "/measurements.csv", sep=";", decimal=",")
        self.clinical_output.to_csv(directory + "/clinical-predictions.csv", sep=";", decimal=",")
        matrix_dir = prepare_directory(directory + "/confusionmatrices")
        for column, matrix in self.confusion_matrices.items():
            plot_confusion_matrix(matrix, self.bpdata.metadata[column].cat.categories, title=column)
            plt.savefig(matrix_dir + "/" + column + ".pdf")
            plt.close()

    def evaluate_in_sample(self, subdir: str):
        self.reset()
        self.evaluate_survival_regression()
        self.evaluate_classification()
        self.__results_to_df()
        self.print(subdir)

    def __results_to_df(self):
        # Converts the measurements from evaluate_classification and _regression to a common frame
        self.measurements = pd.concat(self.measurements_list, axis=1)
        self.coefficients = pd.concat(self.coefficients_list, axis=1)

    def ensemble_worker(self, subsamplesize: int, iteration: int):
        # need for joblib run
        np.random.seed(None)

        if not iteration % 100:
            print("      Iteration {} on process {}".format(iteration, getpid()))

        self.reset()
        self.sampler.use_sample(subsamplesize)
        self.evaluate_survival_regression()
        self.evaluate_classification()
        self.__results_to_df()
        return self.measurements, self.coefficients

    def evaluate_out_of_sample(self,
                               subdir: str,
                               subsamplesize: int,
                               iterations: int,
                               n_jobs: int
                               ) -> Tuple[pd.DataFrame, pd.DataFrame]:
        directory = prepare_directory(self.directory + "/" + subdir)
        print("starting {} jobs".format(n_jobs))
        all_measurements_list, all_coefficients_list = map(list, zip(*Parallel(n_jobs=n_jobs)(
            delayed(ModelEvaluation.ensemble_worker)(self, subsamplesize, i)
            for i in range(0, iterations)
        )))

        all_measurements_list.append(self.measurements)
        all_coefficients_list.append(self.coefficients)
        # create multi-index frame, level 0: 1..iterations, level 1: outcomes
        all_measurements = pd.concat(all_measurements_list, keys=range(0, iterations), axis=1)  # type: pd.DataFrame
        all_coefficients = pd.concat(all_coefficients_list, keys=range(0, iterations), axis=1)  # type: pd.DataFrame
        # aggregate over second level
        aggregated_measurements = all_measurements.groupby(level=1, axis=1).mean()
        aggregated_measurements.to_csv(directory + "/outofsample-ensemble-measurements-average.csv", sep=";", decimal=",")
        grouped_coefficients = all_coefficients.groupby(level=1, axis=1)
        aggregated_coefficients = grouped_coefficients.mean()
        aggregated_coefficients["overall mean"] = aggregated_coefficients.mean(axis=1)
        aggregated_coefficients.to_csv(directory + "/outofsample-ensemble-coefficients-average.csv", sep=";", decimal=",")

        all_measurements.T.plot(kind="box",
                                title="Out-of-sample measurements from {} iterations of ensembles of {}".format(iterations, subsamplesize),
                                figsize=DinA(4).landscape(adjustment=(2,1)), subplots=True)
        plt.savefig(directory + "/outofsample-ensemble-measurements.pdf")
        plt.close()
        return all_measurements, all_coefficients



