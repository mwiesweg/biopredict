import pandas as pd
from utilities import StopWatch
from typing import List, Union


class ModelFeatureReducer:

    def __init__(self, class_ref: type, starting_parameter: float = 1, target_features: int = None):
        self.class_ref = class_ref
        self.starting_parameter = starting_parameter
        self.target_features = target_features
        self.iterations = 0
        self.resettable_iterations = 0
        self.lifetime_iterations = 0
        self.maximum_iterations = 1000 # before stopping - perhaps sometimes target_features cannot be reached exactly

    def reset_iterations(self):
        self.resettable_iterations = 0

    def __increment_iterations(self):
        self.iterations += 1
        self.resettable_iterations += 1
        self.lifetime_iterations += 1

    def run(self, data: pd.DataFrame, outcome: pd.Series, target_features: int = None):
        if target_features is None and self.target_features is None:
            raise ValueError("Must specify target_features, at least once in constructor or in method call")
        if target_features is None:
            target_features = self.target_features

        if self.class_ref.regularize_with_parameter:
            parameter = self.starting_parameter
            interval = [None, None]
            watch = StopWatch()
            self.iterations = 0
            while True:
                self.__increment_iterations()
                if self.iterations > self.maximum_iterations:
                    print("ModelFeatureReducer: FAILURE due to reaching maximum iteration count")
                    return None
                obj = self.class_ref(parameter=parameter)
                try:
                    obj.fit(data, outcome)
                except RuntimeError as rerr:
                    print(rerr)
                    return None
                except ValueError as verr:
                    print(verr)
                    return None
                model_dimensions = obj.model_dimensions()
                # print("{} dimensions with parameter {}".format(model_dimensions, parameter))
                if model_dimensions == target_features:
                    # print("Finished in {} iterations, took {:.3f}".format(iterations, watch.elapsed()))
                    return obj
                elif (model_dimensions > target_features) if self.class_ref.param_bigger_for_less_features \
                        else (model_dimensions < target_features):
                    interval[0] = parameter
                    if interval[1] is None:
                        parameter *= 2
                    else:
                        parameter = (parameter + interval[1]) / 2
                else:
                    interval[1] = parameter
                    if interval[0] is None:
                        parameter /= 2
                    else:
                        parameter = (parameter + interval[0]) / 2
        else:
            # We have a class that regularizes itself
            self.__increment_iterations()
            obj = self.class_ref(data=data)
            obj.target_features = target_features
            try:
                obj.fit(outcome=outcome)
            except RuntimeError as rerr:
                print(rerr)
                return None
            return obj


class ModelFromFeatures:

    def __init__(self, model, data: pd.DataFrame):
        self.model = model
        self.data = data
        self.current_outcome = None
        self.current_features = None

    def fit_with_features(self, outcome: object, features: List[str]) -> None:
        self.current_features = features
        self.current_outcome = outcome
        data_for_features = self.data[self.current_features]
        self.model.fit(data_for_features, self.current_outcome)

    def prediction(self, data: pd.DataFrame = None) -> pd.Series:
        if data is None:
            data = self.data
        data_for_features = data[self.current_features]
        prediction = self.model.predict(data_for_features)
        return pd.Series(prediction.flatten(), index=data_for_features.index)

    def coefficients(self) -> Union[pd.Series, pd.DataFrame]:
        if len(self.model.coef_.shape) == 1:
            return pd.Series(self.model.coef_, index=self.current_features)
        elif self.model.coef_.shape[0] == 1:
            return pd.Series(self.model.coef_.flatten(), index=self.current_features)
        else:
            # multiclass classifiers have shape (n_classes, n_features)
            df = pd.DataFrame(self.model.coef_.transpose(), index=self.current_features)
            df.columns.name = "classes"
            return df

