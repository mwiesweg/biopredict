import pandas as pd
import numpy as np
from typing import Union
from warnings import warn

from scipy.stats import expon


# In particular, "by" must be given if the object is a DataFrame
def sort_values_and_index(values, index_ascending=True, values_ascending=True, inplace=False, **arguments_to_valuesort):
    # Sort by 1) sum 2) index, in this order with stable sort algorithm
    if inplace:
        values.sort_index(ascending=index_ascending, inplace=True)
    else:
        values = values.sort_index(ascending=index_ascending)
    values.sort_values(axis=0, inplace=True, ascending=values_ascending, kind='mergesort', **arguments_to_valuesort)
    return values


def is_integerish(float_series: pd.Series) -> bool:
    if not float_series.iat[0].is_integer():
        return False
    as_int = float_series.astype(int)
    return np.all((float_series - as_int) == 0)


class SeriesRandomizer:
    def __init__(self, series: pd.Series):
        self.series = series
        
    def randomize(self) -> pd.Series:
        isnumber = False
        isinteger = False
        isbool = False
        iscategory = False
        try:
            isnumber = np.issubdtype(self.series.dtype, np.number)
            isinteger = np.issubdtype(self.series.dtype, np.integer)
        except TypeError:
            pass
        try:
            isbool = self.series.dtype == "bool"
        except TypeError:
            pass
        try:
            iscategory = self.series.dtype == "category"
        except TypeError:
            pass

        if isnumber:
            lower = self.series.min()
            upper = self.series.max()
            if isinteger or is_integerish(self.series):
                return pd.Series(data=np.random.randint(lower, upper+1, self.series.size), index=self.series.index)
            else:
                return pd.Series(data=(upper-lower)*np.random.random_sample(self.series.size)+lower,
                                 index=self.series.index)
        elif iscategory:
            return pd.Series(data=self.series.cat.categories[np.random.randint(0, self.series.cat.categories.size, self.series.size)],
                             index=self.series.index)
        elif isbool:
            prob = self.series.sum() / self.series.size
            return pd.Series(data=np.random.binomial(1, p=prob, size=self.series.size),
                             index=self.series.index)
        else:
            warn("Unsupported dtype {} for series named {}, falling back to permutation".
                 format(self.series.dtype, self.series.name))
            return self.permutate()

    def randomize_expon(self,
                        round_: bool = False,
                        lower_clip: float = None
                        ) -> pd.Series:
        expected = self.series.median()
        rand = pd.Series(expon.rvs(scale=expected, size=self.series.index.size), index=self.series.index)
        if round_:
            rand = np.round(rand)
        if lower_clip:
            rand = rand.clip_lower(lower_clip)
        return rand

    def permutate(self) -> pd.Series:
        permutated = self.series[np.random.permutation(self.series.size)]
        permutated.index = self.series.index
        return permutated


class DataFrameRandomizer:
    def __init__(self,
                 df: pd.DataFrame):
        self.df = df

    def randomize(self,
                  columns=None
                  ) -> pd.DataFrame:
        if not columns:
            return self.df.apply(lambda series: SeriesRandomizer(series).randomize(),
                                 axis=0, reduce=False)
        else:
            columns = self.df.columns
            randomized = self.df.copy()  # type: pd.DataFrame
            for column in columns:
                randomized[column] = SeriesRandomizer(self.df[column]).randomize()
            return randomized


class PandasRandomizer:
    def __init__(self,
                 obj: Union[pd.DataFrame, pd.Series]):
        if isinstance(obj, pd.DataFrame):
            self.randomizer = DataFrameRandomizer(obj)
        elif isinstance(obj, pd.Series):
            self.randomizer = SeriesRandomizer(obj)

    def randomize(self) -> Union[pd.Series, pd.DataFrame]:
        return self.randomizer.randomize()
