import os
import copy
import re

import numpy as np
import pandas as pd

from typing import List, Union, Dict, Sequence

from utilities import left_merge_preserving_index, reindex_on


def best_response(best_response_str: str):
    if best_response_str == "(LossOfContact)":
        return None
    if best_response_str == "(Therapy)":
        return None
    if best_response_str == "(Deceased)":
        return None
    return best_response_str


def best_response_to_binary(best_response: str):
    if best_response == "StableDisease":
        return "good"
    elif best_response == "ProgressiveDisease":
        return "bad"
    elif best_response == "PartialResponse":
        return "good"
    elif best_response == "CompleteResponse":
        return "good"
    elif best_response == "NoEvidenceOfDisease":
        return "good"
    elif best_response == "MinorResponse":
        return "good"
    elif best_response == "(Deceased)":
        return "bad"
    return None


def order_binary_category(series: pd.Series):
    return series.cat.reorder_categories(["bad", "good"], ordered=True)


def best_response_to_ternary(best_response: str):
    if best_response == "StableDisease":
        return "intermediate"
    elif best_response == "ProgressiveDisease":
        return "bad"
    elif best_response == "PartialResponse":
        return "good"
    elif best_response == "CompleteResponse":
        return "good"
    elif best_response == "NoEvidenceOfDisease":
        return "good"
    elif best_response == "MinorResponse":
        return "good"
    elif best_response == "(Deceased)":
        return "bad"
    return None


def ttf_to_binary(ttf: int):
    if ttf < 150:
        return "bad"
    else:
        return "good"


def ttf_to_ternary(ttf: int):
    if ttf < 100:
        return "bad"
    elif ttf < 200:
        return "intermediate"
    else:
        return "good"


def order_ternary_category(series: pd.Series):
    return series.cat.reorder_categories(["bad", "intermediate", "good"], ordered=True)


class DataProvider:

    def __init__(self):
        # data must only contain numerical columns
        # data and metadata must be identically indexed
        self.data = None
        self.metadata = None
        self.metadata_column_types = {}
        self.transformer = None  # for storing the fitted primary transformer

    def columns_for_type(self, type_: str):
        return self.metadata_column_types[type_]

    def regression_columns(self):
        return self.columns_for_type("regression")

    def classification_columns(self):
        return self.columns_for_type("classification")

    def survival_columns(self):
        return self.columns_for_type("survival")

    def _set_data(self,
                  data,
                  metadata,
                  inplace):
        if inplace:
            self.data = data
            self.metadata = metadata
        else:
            obj = copy.copy(self)
            obj.data = data
            obj.metadata = metadata
            return obj

    def filter_by(self,
                  other: "DataProvider",
                  inplace=False):
        data = self.data.loc[other.metadata.index, :]
        metadata = self.metadata.loc[other.metadata.index, :]
        return self._set_data(data, metadata, inplace)

    def drop_na(self,
                from_data: bool=None,
                from_metadata: bool=None,
                data_subset: Sequence=None,
                metadata_subset: Sequence=None,
                inplace=False):

        def filtered_index(df, flag, subset):
            if flag or subset is not None:
                columns = df.columns if subset is None else subset
                mask = df[columns].notna()
                if mask.ndim > 1:
                    mask = mask.all(axis=1)
                return df.index[mask]
            return df.index

        index_data = filtered_index(self.data, from_data, data_subset)
        index_metadata = filtered_index(self.metadata, from_metadata, metadata_subset)
        index = index_data.intersection(index_metadata)
        data = self.data.loc[index, :]
        metadata = self.metadata.loc[index, :]
        return self._set_data(data, metadata, inplace)

    def transform_data(self,
                       transformer,
                       inplace=False):
        self.transformer = transformer
        if transformer is None:
            return self._set_data(self.data, self.metadata, inplace)
        else:
            data = transformer.transform(self.data)
            return self._set_data(data, self.metadata, inplace)


    def _edit_survival_metatadata(self,
                                  metadata,
                                  metadata_column_types,
                                  add_best_response,
                                  add_informative_censoring,
                                  censoring_columns_boolean):
        if add_best_response:
            multi = metadata["best response IO"]
            br_binary = multi.apply(best_response_to_binary).astype("category")
            br_ternary = multi.apply(best_response_to_ternary).astype("category")
            br_binary = order_binary_category(br_binary)
            br_ternary = order_ternary_category(br_ternary)
            metadata["best response IO"] = multi.apply(best_response).astype("category")
            metadata["best response binary"] = br_binary
            metadata["best response ternary"] = br_ternary
            ttf = metadata["TTF IO"]
            ttf_binary = ttf.apply(ttf_to_binary).astype("category")
            ttf_ternary = ttf.apply(ttf_to_ternary).astype("category")
            ttf_binary = order_binary_category(ttf_binary)
            ttf_ternary = order_ternary_category(ttf_ternary)
            metadata["TTF IO binary"] = ttf_binary
            metadata["TTF IO ternary"] = ttf_ternary
            metadata_column_types["classification"] = ["best response IO",
                                                       "best response binary",
                                                       "best response ternary",
                                                       "TTF IO binary",
                                                       "TTF IO ternary"]

        if add_informative_censoring:
            metadata["informative_censoring"] = np.where(
                (metadata["last disease state"].isin(["LossOfContact", "BestSupportiveCare"]))
                & (metadata["OS reached"] == 0),
                True,
                False)
            metadata_column_types["informative_censoring"] = "informative_censoring"

        if censoring_columns_boolean:
            for columns in self.survival_columns():
                metadata[columns[1]] = metadata[columns[1]].astype(bool)

        return metadata, metadata_column_types

    @staticmethod
    def filter_query(data,
                     metadata,
                     query,
                     **kwargs):
        if query:
            local_dict = None
            if kwargs:
                local_dict = kwargs
            metadata = metadata.query(query, local_dict=local_dict)
            data = data.loc[metadata.index, :]
        return data, metadata

    @staticmethod
    def column_label(column: Union[str, List[str]]):
        return column[0] + "-survival" if isinstance(column, list) else column

    @staticmethod
    def ma_plot_data(data: pd.DataFrame, ref, directory: str, sort_data: bool=False, suffix: str = ""):
        import matplotlib.pyplot as plt
        import seaborn as sns
        from paperformat import DinA
        from fileutilities import prepare_directory

        n_plots = data.index.size-1
        nrows = int(np.ceil(np.sqrt(n_plots)))
        ncols = int(np.ceil(n_plots / nrows))
        fig, ax = plt.subplots(nrows=nrows, ncols=ncols,
                               sharex=True, sharey=True,
                               figsize=DinA(0).landscape())
        ax = ax.flatten() # for convenient access with plain index

        x = data.loc[ref]
        if sort_data:
            x = x.sort_values().reset_index(drop=True)
        ln_x = np.log1p(x)

        for i, row_index in enumerate(data.index.drop(ref)):
            y = data.loc[row_index]
            if sort_data:
                y = y.sort_values().reset_index(drop=True)

            ln_y = np.log1p(y)
            plot_df = pd.DataFrame({"M": ln_x - ln_y,
                                    "A": 1/2*(ln_x + ln_y)})
            plot_df.plot(x="A", y="M", kind="scatter",
                         marker=".",
                         title=row_index,
                         ax=ax[i])
        for i in range(n_plots, nrows*ncols):
            plt.delaxes(ax[i])
        plt.tight_layout()
        plt.savefig(prepare_directory(directory) +
                    ("/{}vsall-sorted{}.pdf" if sort_data else "/{}vsall-inorder{}.pdf").format(ref, suffix))
        plt.close()

    def ma_plot(self, ref, directory: str, sort_data: Union[str, bool] = False, suffix: str = ""):
        DataProvider.ma_plot_data(self.data, ref, directory, sort_data, suffix)


class BiopredictDataset(DataProvider):

    def __init__(self,
                 data: pd.DataFrame,
                 metadata: pd.DataFrame,
                 metadata_column_types: Dict):
        super().__init__()
        self.data = data
        self.metadata = metadata
        self.metadata_column_types = metadata_column_types


class BiopredictSourceData(DataProvider):

    def __init__(self,
                 raw_data_ids,
                 raw_data_files,
                 clinical_data,
                 cohort_assignment_dir,
                 local_epidemiology=None
                 ):
        super().__init__()

        id_keys = ["name", "firstname", "dob"]

        # Load from source files
        patientlist = (
            pd.read_csv(raw_data_ids, sep=';', index_col=0, parse_dates=["dateOfBirth"],
                        dayfirst=False, encoding='utf-8').
            rename(columns={"name": id_keys[0], "firstName": id_keys[1], "dateOfBirth": id_keys[2]})
        )
        # survival = pd.read_table("../Klinische Daten/Klinische Daten.csv", sep=';', index_col=False,
        #                         parse_dates=["date of birth", "last documentation", "start date IO"])#, dayfirst=True)
        #                         # a boolean dtype does not support missing data
        #                         # dtype={"OS reached": np.bool, "TTF IO reached": np.bool})
        survival = (
            pd.read_json(clinical_data,
                         convert_dates=["date of birth", "last documentation", "start date IO"],
                         encoding="utf-8").
            rename(columns={"surname":id_keys[0], "first name": id_keys[1], "date of birth": id_keys[2]})
        )  # type: pd.DataFrame
        survival["id"] = survival["id"].astype(int)

        raw_data = pd.concat([
            pd.read_csv(raw_data_file, sep=';', header=0, index_col=False, encoding='utf-8')
            for raw_data_file in raw_data_files
        ], axis=0)
        # prenormalized_data = pd.read_csv("../Pränormalisierte Rohdaten/genedata.tsv", sep='\t', header=0, index_col=0)

        with os.scandir(cohort_assignment_dir) as it:
            for entry in it:
                if entry.name.endswith(".csv"):
                    csv = pd.read_csv(entry.path, sep=";", parse_dates=["dob"])
                    csv = reindex_on(csv, survival, on=id_keys, remove_on=True)
                    for column in csv.columns:
                        if column in survival.columns:
                            survival[column] = survival[column].combine_first(csv[column])
                        else:
                            survival[column] = csv[column]

        categorical_columns = ["last disease state", "best response IO"]
        for column in categorical_columns:
            survival[column] = survival[column].astype("category")

        # Use patientlist to generate a common index for data and metadata by left-joining twice
        metadata = left_merge_preserving_index(patientlist, survival, on=id_keys, sort_index=True)

        patientlist_file_keys = ["directory", "ncounterid", "filename"]
        raw_data_file_keys = ["directory", "id", "filename"]
        data = reindex_on(raw_data,
                          index_source=patientlist,
                          index_source_on=patientlist_file_keys,
                          df_on=raw_data_file_keys,
                          sort_index=True,
                          remove_on=True)  # type: pd.DataFrame

        # kick out those entries which we did not identify
        unidentified = metadata["id"].isnull()
        data = data.loc[~unidentified]
        metadata = metadata.loc[~unidentified]
        # create intra-patient running index for those cases with >1 sample
        metadata["intra-patient"] = metadata["id"].duplicated().astype(int)
        # now we can make it integer again (no missing values)
        metadata["id"] = metadata["id"].astype(int)

        # Perform a merge to associate the metadata index with the prenormalized data
        # self.data_prenormalized = pd.merge(patientlist.reset_index(), prenormalized_data,
        #                                    left_on="id-prenormalized", right_index=True)
        # self.data_prenormalized.set_index('index', inplace=True)
        # # we only want the index, drop the rest of the metadata
        # self.data_prenormalized.drop(patientlist.columns, axis=1, inplace=True)
        # self.data_prenormalized.sort_index(inplace=True)

        if local_epidemiology:
            self.local_epidemiology = pd.read_csv(local_epidemiology, sep=";")
        else:
            self.local_epidemiology = None

        self.source_data = data
        self.source_metadata = metadata

        self.data = data
        self.metadata = metadata

        self.column_types = {}
        self.metadata_column_types = {"classification": [],  # added later
                                      "regression": ["TTF IO", "OS IO"],
                                      "survival": [["TTF IO", "TTF IO reached"],
                                                   ["OS IO", "OS reached"]],
                                      "dco": "DCO IO",
                                      "covariates": ["first line with IO"],
                                      "informative_censoring": None, # added later
                                      "lab": ["lab value IO during line IO"]
                                      }

        # this is the full clinical data not excluding patients without biopredict data
        self.source_clinical = self._convert_lab_data(survival)

    def dataset(self,
                drop_invalid=True,
                filter_cohorts=None,
                query=None,
                add_best_response=True,
                add_informative_censoring=True,
                censoring_columns_boolean=True) -> BiopredictDataset:
        data = self.source_data.copy()
        metadata = self.source_metadata.copy()
        metadata_column_types = self.metadata_column_types.copy()

        # drop samples marked as invalid
        if drop_invalid:
            data, metadata = self.filter_query(data, metadata, "valid == 1")

        if filter_cohorts is not None:
            data, metadata = self.filter_query(data, metadata,
                                                "cohort in @cohort_filter",
                                                cohort_filter=filter_cohorts)

        metadata, metadata_column_types = self._edit_survival_metatadata(metadata,
                                                                         metadata_column_types,
                                                                         add_best_response,
                                                                         add_informative_censoring,
                                                                         censoring_columns_boolean)

        data, metadata = self.filter_query(data, metadata, query)

        # if normalization_method == "prenormalized":
        #     data = self.data_prenormalized.loc[data.index.intersection(self.data_prenormalized.index)]

        return BiopredictDataset(data, metadata, metadata_column_types)

    def add_logarithms(self):
        logs = np.log(self.data)
        logs.columns = "log_" + logs.columns
        self.column_types["log"] = logs.columns
        self.data = pd.concat([self.data, logs], axis=1)

    def _convert_lab_data(self, lab_df):
        lab_columns = self.metadata_column_types["lab"]
        lab_df = lab_df.copy()

        def dict_to_lab_df(_dict, index):
            if _dict is None:
                return None
            df = pd.concat([pd.Series(value, name=key) for key, value in _dict.items()],
                           axis=1,
                           sort=True)  # type: pd.DataFrame
            df = df.T.reindex(index=index)
            df.columns = pd.to_datetime(df.columns)
            return df

        def dict_series_to_df_series(series):
            keys = set()
            for _dict in series:
                if _dict:
                    keys |= _dict.keys()
            keys = sorted(keys)
            return series.transform(dict_to_lab_df, index=keys)

        lab_df[lab_columns] = lab_df[lab_columns].transform(dict_series_to_df_series)
        return lab_df

    def lab_dataset(self,
                    time="initial",
                    query: str=None,
                    lab_required: Sequence=None,
                    lab_subset: Sequence=None,
                    add_best_response=True,
                    add_informative_censoring=True,
                    censoring_columns_boolean=True) -> DataProvider:

        metadata = self.source_clinical.copy()  # type: pd.DataFrame
        metadata_column_types = self.metadata_column_types.copy()

        lab_col = "lab value IO during line IO"
        start_date_col = "start date IO"

        def delta_to_start(dates, start_date):
            if not len(dates) or start_date is None:
                return None
            return pd.Series(dates - start_date, index=dates).apply(pd.Timedelta.total_seconds)

        def first_valid_value(series: pd.Series):
            idx = series.first_valid_index()
            if idx is None:
                return np.NaN
            return series[idx]

        def absolute_from_relative(series: pd.Series):
            wbc = "Leukozyten"
            result = series.copy()
            for species in ["Monozyten", "Lymphozyten"]:
                abs_count = species
                rel_count = species + " relativ"
                if (rel_count in series
                        and wbc in series
                        and (abs_count not in series or np.isnan(series[abs_count]))):
                    result[abs_count] = series[rel_count] / 100.0 * series[wbc]
            return result

        def next_prior(row):
            df = row[lab_col]
            start_date = row[start_date_col]

            if df is None:
                return None

            # after .T: index: dates, columns: lab test
            df = df.T
            # compute some absolute values if only relative ones are present
            df = df.apply(absolute_from_relative, axis=1)

            # time delta to start date
            deltas = delta_to_start(df.index, start_date)
            if deltas is None:
                return None

            prior_points = deltas <= 0
            if not np.any(prior_points):
                print("No prior lab data: ", row["name"], row["firstname"], "(Start date: ", start_date, ")")
                print(df)
                return None

            # keep only prior dates
            df = df[prior_points].sort_index(ascending=False)
            # for each lab test, get the first (=latest date) valid value
            return df.apply(first_valid_value, axis=0)

        if time == "initial":
            data = metadata.apply(next_prior, axis=1, reduce=False)  # type: pd.DataFrame
        else:
            raise ValueError("Unknown time parameter: ", time)

        metadata.drop(lab_col, axis=1, inplace=True)

        if lab_required:
            data.dropna(subset=lab_required, inplace=True)
            metadata = metadata.loc[data.index, :]

        if lab_subset:
            data.drop([col for col in data.columns if col not in lab_subset], axis=1, inplace=True)

        metadata, metadata_column_types = self._edit_survival_metatadata(metadata,
                                                                         metadata_column_types,
                                                                         add_best_response,
                                                                         add_informative_censoring,
                                                                         censoring_columns_boolean)

        data, metadata = self._filter_query(data, metadata, query)

        return BiopredictDataset(data, metadata, metadata_column_types)


class RiazCell2017Data(DataProvider):

    def __init__(self):
        super().__init__()
        raw_data = pd.read_csv("../data/Riaz Cell 2017/CountData.BMS038.txt", sep="\t")
        raw_data.columns = raw_data.columns.str.replace("_Pre", "_pre")
        raw_data = raw_data.set_index("HUGO").T
        self.source_metadata = pd.read_csv("../data/Riaz Cell 2017/bms038_clinical_data.csv", sep=",")
        self.source_data = reindex_on(raw_data,
                                      index_source=self.source_metadata,
                                      index_source_on=["Sample"],
                                      df_index=True,
                                      sort_index=True,
                                      remove_on=False)  # type: pd.DataFrame
        self.source_data = self.source_data.set_index("Sample")
        self.source_metadata = self.source_metadata.set_index("Sample")

        self.source_metadata["PFS reached"] = self.source_metadata["PFS_SOR"] == 0
        self.source_metadata["OS reached"] = self.source_metadata["OS_SOR"] == 0

        self.metadata_column_types = {
                                      "survival": [["PFS", "PFS reached"],
                                                   ["OS", "OS reached"]]
                                      }
        self.data = self.source_data
        self.metadata = self.source_metadata

    def dataset(self,
                valid_only=True,
                itx_naive_only=True) -> BiopredictDataset:
        data = self.source_data.copy()
        metadata = self.source_metadata.copy()
        metadata_column_types = self.metadata_column_types.copy()

        # drop samples without data
        if valid_only:
            filter = data.notna().all(axis=1)
            metadata = metadata.loc[filter]
            data = data.loc[self.metadata.index]
        if itx_naive_only:
            data, metadata = self.filter_query(data, metadata, "Cohort == \"NIV3-NAIVE\"")

        data = data.clip_lower(1)

        return BiopredictDataset(data, metadata, metadata_column_types)


class BerlinData(DataProvider):

    def __init__(self,
                 raw_data_ids,
                 raw_data_file,
                 clinical_data):
        super().__init__()

        patientlist = pd.read_csv(raw_data_ids, sep=';', index_col=0, encoding='utf-8')
        survival = pd.read_csv(clinical_data, sep=';', header=0, index_col=False, encoding='utf-8',
                               parse_dates=["Datum Erstdiagnose",
                                            "Datum Beginn Immuntherapie ",
                                            "Datum Therapiebeginn 1st line",
                                            "Datum Therapiebeginn 2nd line",
                                            "Datum Therapiebeginn 3rd line",
                                            "Datum Therapiebeginn 4th line",
                                            "Datum Best response",
                                            "Wenn ja, Datum letztes staging",
                                            "Wenn nein, Datum Progress",
                                            "Datum Beginn  Therapie 1",
                                            "Datum Beginn  Therapie 2",
                                            "Datum Beginn  Therapie 3",
                                            "Datum letzter Vitalstatus",
                                            "Todesdatum, wenn bekannt"],
                               dayfirst=False)
        raw_data = pd.read_csv(raw_data_file, sep=';', header=0, index_col=False, encoding='utf-8')

        categorical_columns = []
        for column in categorical_columns:
            survival[column] = survival[column].astype("category")

        id_columns = ["Patienten Nummer"]

        metadata = left_merge_preserving_index(patientlist, survival, on=id_columns, sort_index=True)
        data = reindex_on(raw_data,
                          index_source=patientlist,
                          index_source_on=["filename"],
                          df_on=["filename"],
                          sort_index=True,
                          remove_on=True)  # type: pd.DataFrame
        data.drop("directory", axis=1, inplace=True)

        # no intra-patient duplicates
        metadata["intra-patient"] = 0
        # make conformant with train/test data conventions
        metadata.rename(columns={"Patienten Nummer": "id"}, inplace=True)
        metadata["biopredict"] = "validation"

        def compute_pdl1_tps(pdl1):
            if type(pdl1) is float:
                return pdl1
            if pdl1 is None or pdl1 == "Unbekannt" or pdl1 == "unbekannt":
                return np.nan
            pdl1 = re.sub("[%>]", "", pdl1)
            pdl1 = re.split("[/\\-]", pdl1)[0]
            return int(pdl1)

        def compute_ttf(s):
            index = ["TTF IO", "TTF IO reached"]
            if pd.isnull(s["Wenn ja, Datum letztes staging"]):
                return pd.Series([(s["Wenn nein, Datum Progress"] - s["Datum Beginn Immuntherapie "]).days, 1], index)
            else:
                return pd.Series([(s["Wenn ja, Datum letztes staging"] - s["Datum Beginn Immuntherapie "]).days, 0], index)

        def compute_os(s):
            index = ["OS", "OS reached"]
            if s["Vitalstatus 1_lebt  0_gestorben"] == 0:
                return pd.Series([(s["Todesdatum, wenn bekannt"] - s["Datum Erstdiagnose"]).days, 1], index)
            else:
                return pd.Series([(s["Datum letzter Vitalstatus"] - s["Datum Erstdiagnose"]).days, 0], index)

        def compute_os_io(s):
            index = ["OS IO"]  # does not duplicate "OS reached"
            if s["Vitalstatus 1_lebt  0_gestorben"] == 0:
                return pd.Series([(s["Todesdatum, wenn bekannt"] - s["Datum Beginn Immuntherapie "]).days], index)
            else:
                return pd.Series([(s["Datum letzter Vitalstatus"] - s["Datum Beginn Immuntherapie "]).days], index)

        metadata = pd.concat([
            metadata,
            metadata.apply(compute_ttf, axis=1),
            metadata.apply(compute_os, axis=1),
            metadata.apply(compute_os_io, axis=1),
        ], axis=1)
        metadata["best response IO"] = metadata["best reponse (nach RECIST 1.1.)"].map({
            "PR": "PartialResponse",
            "PD": "ProgressiveDisease",
            "SD": "StableDisease"
        })
        metadata["first line with IO"] = metadata["Anzahl Chemotherapielinien vor Immuntherapie"] + 1
        metadata["PD-L1 TPS"] = metadata["% PD-L1-positiver Tumorzellen (nur membranös)"].apply(compute_pdl1_tps)
        metadata["entity"] = metadata["Histologie"].\
            str.replace(re.compile("[pP]latte"), "NSCLC/sq").\
            str.replace(re.compile("[aA]deno"), "NSCLC/A")

        self.source_data = data
        self.source_metadata = metadata

        self.data = data
        self.metadata = metadata

        self.column_types = {}
        self.metadata_column_types = {"classification": [],  # added later
                                      "regression": ["TTF IO", "OS IO"],
                                      "survival": [["TTF IO", "TTF IO reached"],
                                                   ["OS IO", "OS reached"]],
                                      "dco": None,
                                      "covariates": ["first line with IO"],
                                      "informative_censoring": None,  # added later
                                      "lab": []
                                      }

    def dataset(self,
                drop_invalid=True,
                filter_cohorts=None,
                query=None,
                add_best_response=True,
                add_informative_censoring=False,
                censoring_columns_boolean=True) -> BiopredictDataset:

        if not drop_invalid or filter_cohorts or add_informative_censoring:
            raise ValueError("Invalid parameter for Berlin data")

        data = self.source_data.copy()
        metadata = self.source_metadata.copy()
        metadata_column_types = self.metadata_column_types.copy()

        metadata, metadata_column_types = self._edit_survival_metatadata(metadata,
                                                                         metadata_column_types,
                                                                         add_best_response,
                                                                         False,
                                                                         censoring_columns_boolean)

        data, metadata = self.filter_query(data, metadata, query)

        return BiopredictDataset(data, metadata, metadata_column_types)


def _do_ma_plot(ref, method, sort_data):
    print(ref, "sort_data", sort_data)
    bpdata = BiopredictSourceData().dataset(drop_invalid=True)
    # TODO: different normalization methods via transformer
    bpdata.ma_plot(0, "../plots/maplots-raw", sort_data=sort_data, suffix="-" + method)


def ma_plots():
    from joblib import Parallel, delayed
    Parallel(n_jobs=6)(delayed(_do_ma_plot)(0, method, sort_data)
                       for method in ["global", "ref", "ref-only"]
                       for sort_data in [True, False])


def correlation_control():
    import seaborn as sns
    import matplotlib.pyplot as plt
    import matplotlib.font_manager as font_manager
    from fileutilities import prepare_directory
    source = BiopredictSourceData()
    bpdata = source.dataset()

    prenormalized = source.data_prenormalized.copy()
    pn_index = prenormalized.index
    prenormalized.index = "pn-" + prenormalized.index.astype(str)

    data = pd.concat([bpdata.data, prenormalized]) # type: pd.DataFrame
    data.drop(data.columns[data.isnull().any(axis=0)], axis=1, inplace=True)
    matrix = np.corrcoef(data, rowvar=True)
    matrix = pd.DataFrame(matrix, index=data.index, columns=data.index)

    font_prop = font_manager.FontProperties(family="sans-serif", size=2)
    ax = plt.subplot()
    sns.heatmap(matrix, square=True, ax=ax)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=90)
    ax.set_yticklabels(ax.get_yticklabels(), rotation=0)
    for label in (ax.get_xticklabels() + ax.get_yticklabels()):
        label.set_fontproperties(font_prop)
    directory = prepare_directory("../plots/")
    plt.savefig(directory + "/normalized-prenormalized-correlationmatrix.pdf")
    plt.close()


def export():
    dataset = BiopredictSourceData().dataset()
    dataset.metadata.to_csv("../data/metadata.csv", sep=";", decimal=",")
    dataset.data.to_csv("../data/data.csv", sep=";", decimal=",")


if __name__ == "__main__":

    berlin = BerlinData(raw_data_ids="../Rohdaten/ids-berlin.csv",
                        raw_data_file="../Rohdaten/cohort-berlin.csv",
                        clinical_data="../Klinische Daten/Klinische Daten Kohorte Berlin.csv")
    dset = berlin.dataset()
    pd.options.display.max_columns = 8
    print(dset.metadata.loc[:,["id", "PD-L1 TPS"]])
    # print(dset.data["A2M"])
    # print(dset.metadata[[
    #     "id", "OS", "OS reached", "OS IO", "TTF IO", "TTF IO reached",
    #     "best reponse (nach RECIST 1.1.)", "best response ternary",
    #     "Ab hier: Stammdaten und Ersttherapie E-Nr"]])






