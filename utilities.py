import time
from typing import List
import pandas as pd


class StopWatch:
    def __init__(self, process_time: bool = True):
        self.process_time = process_time
        self.startingtime = 0
        self.restart()

    def elapsed(self) -> float:
        return self.time() - self.startingtime

    def restart(self) -> None:
        self.startingtime = self.time()

    def _to_string(self, time, prefix: str, suffix: str) -> str:
        unit = "s"
        seconds = time
        if seconds < 1.:
            format_string = "{:.3f}"
            time *= 1000
            unit = "ms"
        elif seconds < 10:
            format_string = "{:.3f}"
        else:
            format_string = "{:.1f}"
            if 10. <= seconds < 60.:
                pass
            elif 60. <= seconds < 3600.:
                time /= 60
                unit = "min"
            else:
                format_string = "{:.2f}"
                time /= 3600
                unit = "h"
        return prefix + format_string.format(time) + unit + suffix

    def elapsed_string(self, prefix: str = "took ", suffix: str = "", n_iterations = None) -> str:
        time = seconds = self.elapsed()
        s = self._to_string(time, prefix, suffix)
        if n_iterations:
            return s + self._to_string(time / n_iterations, " (", " per iteration)")
        else:
            return s

    def print_elapsed(self, prefix: str="", suffix: str="", n_iterations = None):
        print(self.elapsed_string(prefix, suffix, n_iterations))

    def time(self) -> float:
        if self.process_time:
            return time.process_time()
        else:
            return time.time()


# Convert a list of tuples to tuples of lists
def zip_to_list(list_of_tuples):
    return map(list, zip(*list_of_tuples))


def as_list(arg) -> List:
    return arg if type(arg) is list else [arg]


def left_merge_preserving_index(left: pd.DataFrame,
                                right: pd.DataFrame,
                                sort_index=False,
                                **kwargs) -> pd.DataFrame:
    df = pd.merge(left.reset_index(),
                  right,
                  "left",
                  **kwargs)
    df.set_index("index", inplace=True)
    if sort_index:
        df.sort_index(inplace=True)
    return df


def reindex_on(df: pd.DataFrame,
               index_source: pd.DataFrame,
               on=None, df_on=None, index_source_on=None, df_index=False, index_source_index=False,
               remove_on=False,
               sort_index=False,
               **kwargs) -> pd.DataFrame:
    '''
    Reindexes df with the index of index_source by merge-style assignment.

    :param df: The data frame containing the data
    :param index_source: The data frame carrying the index -> index_on association
    :param on, df_on, index_source_on, df_index, index_source_index: Just like for merge
    :param remove_on: Should the returned frame contain the "on" columns?
    :return: A data frame indexed like index_source (regarding index -> "on") but with the data columns of df
    '''

    # get a bare dataframe with only the index and the relevant "on" columns
    index_source_keys = []
    if on:
        index_source_keys += on
    if index_source_on:
        index_source_keys += index_source_on
    index_source = index_source[index_source_keys]

    df = left_merge_preserving_index(index_source, df,
                                     on=on,
                                     left_on=index_source_on, right_on=df_on,
                                     left_index=index_source_index, right_index=df_index,
                                     sort_index=sort_index)
    if remove_on:
        remove_cols = set(index_source_keys)
        if df_on:
            remove_cols |= set(df_on)
        df.drop([col for col in remove_cols if col in df.columns], axis=1, inplace=True)

    return df
