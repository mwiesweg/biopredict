
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from typing import List, Dict, Iterable
import myr


class BiopredictCoxLasso:

    regularize_with_parameter = False

    def __init__(self, data: pd.DataFrame, *args):
        self.data = data
        self.current_outcome = None
        self.current_selected_features = None
        self.current_prediction = None
        self.current_score = None
        self.coef_ = None
        self.target_features = None
        self.survivalPackage = myr.packages.Survival()

    def fit(self, outcome: pd.DataFrame):
        self.current_outcome = outcome
        # returns coefficients and iAUC
        self.coef_, self.current_score = self.survivalPackage.cox_lasso_by_nonzero(self.data, self.current_outcome, self.target_features)
        self.current_selected_features = np.nonzero(self.coef_)[0]  # feature coordinates

    def model_dimensions(self) -> int:
        return len(self.current_selected_features)

    def model_features(self):
        return self.data.columns[self.current_selected_features]

    def score(self, *args):
        return self.current_score


if __name__ == "__main__":
    from dataloading import BiopredictSourceData
    print("Loading data...")
    bpdata = BiopredictSourceData().dataset()

    columns = bpdata.survival_columns()
    cox = BiopredictCoxLasso(bpdata.data)
    cox.target_features = 10
    cox.fit(bpdata.metadata.loc[:,columns[1]])
    print(cox.coef_, cox.score())
