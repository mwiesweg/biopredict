import numpy as np
import pandas as pd

from transformers import StoringStandardScaler
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import GroupShuffleSplit
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score

from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import Adam
from keras.wrappers.scikit_learn import KerasClassifier

from xgboost import XGBClassifier

import biopredict
from prediction import fit_grid, load_ensemble_prediction


class MetaNNClassifier(KerasClassifier):

    def __init__(self, **sk_params):
        sk_params.pop("build_fn", None)
        super().__init__(build_fn=None, **sk_params)

    def __call__(self,
                 input_dim=16,
                 layer_dims=(32, 32),
                 drop_outs=(0.25, 0.25)):

        model = Sequential()

        for idx in range(len(layer_dims)):
            if idx == 0:
                model.add(Dense(layer_dims[idx], activation="relu", input_dim=input_dim))
            else:
                model.add(Dense(layer_dims[idx], activation="relu"))
            if drop_outs[idx]:
                model.add(Dropout(drop_outs[idx]))

        model.add(Dense(1, activation='sigmoid'))

        model.compile(optimizer=Adam(),
                      loss='binary_crossentropy',
                      metrics=['accuracy'])

        return model


def nn_params():
    params = [
        {
            "metannclassifier__layer_dims": [(32, 32), (64, 64), (128, 128)],
            "metannclassifier__drop_outs": [(0.3, 0.3), (0.4, 0.4), (0.5, 0.5)]
        },
        {
            "metannclassifier__layer_dims": [(32, 32, 32), (64, 64, 64), (256, 256, 256)],
            "metannclassifier__drop_outs": [(0.2, 0.2, 0.2), (0.4, 0.4, 0.4), (0.5, 0.5, 0.5)]
        }
    ]

    global_params = {
        "metannclassifier__batch_size": [12, 24, 32, 48],
        "metannclassifier__epochs": [50, 100, 200]
    }
    for dict_ in params:
        dict_.update(**global_params)
    return params


def xgb_params():
    return {
        'xgbclassifier__learning_rate': [0.4, 0.5, 0.6, 0.7, 0.8],
        'xgbclassifier__min_child_weight': [1, 2, 3],
        'xgbclassifier__gamma': [0, 0.1, 0.2, 0.3, 0.4, 0.5],
        'xgbclassifier__subsample': [0.8, 0.9, 0.95, 1.0],
        'xgbclassifier__colsample_bytree': [0.4, 0.5, 0.6, 0.7, 0.8],
        'xgbclassifier__max_depth': [2, 3, 6, 8, 10]
    }


def svc_params():
    n_steps = 20
    C_range = np.logspace(-2, 5, n_steps)
    gamma_range = np.logspace(-9, 3, n_steps)
    return [
        {'svc__kernel': ['sigmoid'],
         'svc__C': C_range,
         'svc__gamma': gamma_range},
        {'svc__C': C_range,
         'svc__kernel': ['linear']},
        {'svc__C': C_range,
         'svc__gamma': gamma_range,
         'svc__kernel': ['rbf']}
    ]


def rfc_params():
    n_steps = 20
    return {
        'randomforestclassifier__n_estimators': np.round(np.logspace(1, 4, n_steps)).astype(np.int),
        'randomforestclassifier__criterion': ["gini", "entropy"],
        'randomforestclassifier__min_samples_split': [2, 4, 8],
        'randomforestclassifier__bootstrap': [True, False]
    }


def logref_params():
    n_steps = 20
    return {
        "C": np.logspace(-2, 10, n_steps)
    }


def meta_pipeline(method, x):
    if method == "nn":
        return make_pipeline(
            StoringStandardScaler(),
            MetaNNClassifier(input_dim=x.shape[1])
        )
    elif method == "xgb":
        return make_pipeline(
            StoringStandardScaler(),
            XGBClassifier()
        )
    elif method == "rfc":
        return make_pipeline(
            StoringStandardScaler(),
            RandomForestClassifier(max_features=None,
                                   max_depth=None)
        )
    elif method == "svc":
        return make_pipeline(
            StoringStandardScaler(),
            SVC()
        )
    elif method == "logreg":
        return LogisticRegression()
    else:
        raise ValueError("Unknown meta prediction method", method)


def param_grid(method):
    if method == "nn":
        return nn_params()
    elif method == "xgb":
        return xgb_params()
    elif method == "rfc":
        return rfc_params()
    elif method == "svc":
        return svc_params()
    elif method == "logreg":
        return logref_params()
    else:
        raise ValueError("Unknown meta prediction method", method)


def grid_search_cv(ids,
                   x,
                   y,
                   method,
                   n_jobs=1):
    cv = GroupShuffleSplit(n_splits=10,
                           test_size=0.2,
                           random_state=1)

    grid = GridSearchCV(meta_pipeline(method, x),
                        param_grid=param_grid(method),
                        cv=cv,
                        verbose=2,
                        return_train_score=True,
                        n_jobs=n_jobs
                        )

    return fit_grid("meta", grid, x, y, method, method, groups=ids["id"])


if __name__ == "__main__":
    import warnings
    # warnings.filterwarnings("error")
    warnings.filterwarnings(action='ignore', category=DeprecationWarning)
    import argparse as ap
    parser = ap.ArgumentParser(description="Meta Prediction")
    parser.add_argument("--jobs", default=1, type=int)
    args = parser.parse_args()
    n_jobs = min(max(1, args.jobs), 72)

    ids, x, y = load_ensemble_prediction()

    naive = x["rna SVC best response binary bad/good"] > 0
    print("Naive score: {}".format(accuracy_score(y, naive)))

    meta_df = pd.DataFrame()
    for method in ["rfc", "svc", "xgb", "nn"]:
        meta_df = meta_df.append(grid_search_cv(ids, x, y, method, n_jobs))
    #meta_df.to_csv(biopredict.cv_directory("meta") + "/summary.csv", sep=";", decimal=",")
