from plotutilities import import_matplotlib
import_matplotlib()

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import itertools

from sklearn.base import clone
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC


def categorical_to_integer(x):
    if x is not None and x.dtype.name == 'category':
        return x.cat.codes
    return x

# based on http://scikit-learn.org/stable/auto_examples/svm/plot_iris.html
# (c) sklearn


def make_meshgrid(x, y, h=.02):
    """Create a mesh of points to plot in

    Parameters
    ----------
    x: data to base x-axis meshgrid on
    y: data to base y-axis meshgrid on
    h: stepsize for meshgrid, optional

    Returns
    -------
    xx, yy : ndarray
    """
    x_min, x_max = x.min() - 1, x.max() + 1
    y_min, y_max = y.min() - 1, y.max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))
    return xx, yy


def plot_contours(ax,
                  clf,
                  xx,
                  yy,
                  features,
                  color_dict,
                  draw_contour_lines=False,
                  **params):
    """Plot the decision boundaries for a classifier.

    Parameters
    ----------
    ax: matplotlib axes object
    clf: a classifier
    xx: meshgrid ndarray
    yy: meshgrid ndarray
    params: dictionary of params to pass to contourf, optional
    """
    points = pd.DataFrame(np.c_[xx.ravel(), yy.ravel()], columns=features)
    pred = clf.predict(points)

    if isinstance(clf, SVC):
        # returns (n_samples,) for the binary case and (n_samples, n_classes) otherwise.
        measure_of_decision = clf.decision_function(points)
    else:
        # returns (n_samples, n_classes), with sum=1 for all rows
        measure_of_decision = clf.predict_proba(points)
        if measure_of_decision.shape[1] == 2:
            measure_of_decision = measure_of_decision[:,1]

    if measure_of_decision.ndim == 1:
        Z = measure_of_decision.reshape(xx.shape)
        ax.contourf(xx, yy, Z,
                    cmap=plt.get_cmap("RdYlGn"),
                    # colors=list(color_dict.values()),
                    **params)
        if draw_contour_lines:
            ax.contour(xx, yy, measure_of_decision.reshape(xx.shape), 5, colors="k")
    else:
        Z = pred.reshape(xx.shape)
        levels = np.sort(np.unique(pred))
        ax.contourf(xx, yy, Z, colors=[color_dict[level] for level in levels], **params)
        if draw_contour_lines:  # the contours just dont make sense for better visualization
            for idx, color in zip(range(measure_of_decision.shape[1]), color_dict.values()):
                Z = measure_of_decision[:, idx].reshape(xx.shape)
                ax.contour(xx, yy, Z, 10, colors=color)


def auto_layout(n):
    if n <= 4:
        ncol = n
    else:
        if n % 3 == 0:
            ncol = 3
        else:
            if n < 8:
                ncol = 2
            else:
                if n % 2 == 0:
                    ncol = 4
                else:
                    ncol = 3
    nrow = int(np.ceil(n / ncol))
    return nrow, ncol

def plot_decision_boundaries_of_two_features(ax,
                                             clf,
                                             features,
                                             x: pd.DataFrame,
                                             y,
                                             x_test: pd.DataFrame=None,
                                             y_test=None):
    print("Plotting for", features, x.shape)
    title = "{} and {}".format(*features)
    colors = {"bad": "#d7191c",
              "discordant": "#fdae61",
              "All": "#ffffbf",
              "intermediate": "#92c5de",
              "good": "#1a9641",
              "all four predictors good": "#1a9641"}

    x = x[features]
    x_test = x_test[features] if x_test is not None else np.empty((0, 2))
    y_int = categorical_to_integer(y)

    x0, x1 = x.iloc[:, 0], x.iloc[:, 1]
    x0_test, x1_test = x_test.iloc[:, 0], x_test.iloc[:, 1]
    xx, yy = make_meshgrid(np.concatenate((x0, x0_test)),
                           np.concatenate((x1, x1_test)))

    clf.fit(x, y_int)
    codes_to_color = {idx: colors[y.cat.categories[idx]] for idx in range(len(y.cat.categories))}

    plot_contours(ax, clf, xx, yy, features, codes_to_color, alpha=0.8)
    ax.scatter(x0, x1, c=y.map(colors), s=20, edgecolors='none')
    ax.scatter(x0_test, x1_test, c=y_test.map(colors), s=20, edgecolors="k")
    ax.set_xlim(xx.min(), xx.max())
    ax.set_ylim(yy.min(), yy.max())
    ax.set_xlabel(features[0])
    ax.set_ylabel(features[1])
    ax.set_xticks(())
    ax.set_yticks(())
    ax.set_title(title)


def plot_decision_boundaries(pipeline: Pipeline,
                             x: pd.DataFrame,
                             y: pd.DataFrame,
                             x_test: pd.DataFrame = None,
                             y_test: pd.DataFrame = None,
                             size_per_subfigure=(4.135, 2.338),
                             ncol=None,
                             filepath=None,
                             show=False):

    pipeline = clone(pipeline)
    clf = pipeline.steps[-1][1]
    transformer = Pipeline(pipeline.steps[:-1] + [('NoneType', None)])
    x = transformer.fit_transform(x)
    x_test = transformer.fit_transform(x_test) if x_test is not None else None

    combinations = list(itertools.combinations(x.columns, 2))
    if ncol:
        nrow = int(np.ceil(len(combinations) / ncol))
    else:
        nrow, ncol = auto_layout(len(combinations))
    figsize = tuple(np.asarray(size_per_subfigure) * np.asarray((ncol, nrow)))
    print("Creating plot {} with {} subplots and size {}".format(filepath, len(combinations), figsize))
    fig, sub = plt.subplots(nrow, ncol, figsize=figsize)
    sub = sub.flatten()

    for combination, ax in zip(combinations, sub):
        plot_decision_boundaries_of_two_features(ax, clf, list(combination),
                                                 x, y,
                                                 x_test, y_test)
    for idx in range(len(combinations), len(sub)):
        fig.delaxes(sub[idx])

    plt.tight_layout()
    if filepath:
        plt.savefig(filepath)
    if show:
        plt.show()

    plt.close(fig)

