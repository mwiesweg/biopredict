from plotutilities import PlotCycler
from fileutilities import prepare_directory

import numpy as np
import pandas as pd

import seaborn as sns
import matplotlib.pyplot as plt

from scipy.cluster.hierarchy import dendrogram, linkage, fcluster
from scipy.spatial.distance import squareform
from sklearn.base import BaseEstimator, TransformerMixin
from transformers import StoringTransformer, DataFrameSafeTransformerMixin


class CorrelatingDataRemover(BaseEstimator, TransformerMixin, StoringTransformer, DataFrameSafeTransformerMixin):

    def __init__(self,
                 threshold=1.0,
                 cut_highest=False,
                 cut_lowest=False):
        # parameters
        self.threshold = threshold
        self.cut_highest = cut_highest
        self.cut_lowest = cut_lowest

        # fit
        self.features_to_remove = []

        # internal
        self.data = None
        self.matrix = None
        self.Y = None
        self.Z = None

    def fit(self, X, y=None):
        # remove features with close-to-zero stddev (cannot compute correlation anyway)
        zero_stddev = X.columns[np.isclose(X.std(), 0)]
        data = X.drop(zero_stddev, axis=1)  # type: pd.DataFrame
        # cut extremes?
        if self.cut_highest or self.cut_lowest:
            data = self.cut_extremes(data)
        self.data = data
        # compute correlation matrix
        self.matrix = np.corrcoef(self.data, rowvar=False)
        # compute condensed distance matrix
        self.Y = squareform(1 - abs(self.matrix), checks=False)
        # compute linkage
        self.Z = linkage(self.Y, method='complete')

        clusters = self.clusters()
        # Have a dict leader -> members. Get flat list of members
        self.features_to_remove = [member
                                   for members in clusters.values()
                                   for member in members]
        return self

    def transform(self, X):
        if self.features_to_remove:
            return X.drop(self.features_to_remove, axis=1)
        else:
            return X

    def fit_data(self):
        return self.features_to_remove

    def set_fit_data(self,
                     params):
        self.features_to_remove = params

    def cut_extremes(self, data):
        if not self.cut_highest and not self.cut_lowest:
            return data

        new_length = len(data.index)
        if self.cut_highest:
            new_length -= 1
        if self.cut_lowest:
            new_length -= 1
        new_series = []
        for column, series in data.iteritems():
            drop = []
            if self.cut_highest:
                drop.append(series.idxmax())
            if self.cut_lowest:
                drop.append(series.idxmin())
            series = series.drop(drop)
            series.reset_index(drop=True, inplace=True)
            new_series.append(series)
        return pd.concat(new_series, axis=1)

    def clusters(self, return_statistics=False):
        T = fcluster(self.Z, 1 - self.threshold, criterion="distance")
        num = len(np.unique(T))
        leaves = 0
        clusters = 0
        cluster_cum_sum = 0
        print("Number of entries for threshold {:.3f}: {:d}".format(self.threshold, num))
        cluster_dict = {}

        for k in np.unique(T):
            indices = np.where(T == k)[0].tolist()
            if len(indices) == 1:
                leaves += 1
                continue
            stds = self.data.iloc[:, indices].std(axis=0)
            leader = stds.idxmax()
            members = self.data.columns[indices].values.tolist()
            members.remove(leader)
            cluster_dict[leader] = members
            clusters += 1
            cluster_cum_sum += len(indices)

        if return_statistics:
            return cluster_dict, clusters, leaves, cluster_cum_sum
        else:
            return cluster_dict

    def plot_matrix(self, plot_threshold, suffix: str = ""):
        corr_coeffs = pd.DataFrame(self.matrix, index=self.data.columns, columns=self.data.columns)
        high_coeffs = corr_coeffs[abs(corr_coeffs) > plot_threshold]
        np.fill_diagonal(high_coeffs.values, np.NaN)
        high_coeffs.dropna(inplace=True, how='all')
        f, ax = plt.subplots(figsize=(12, 9))
        sns.heatmap(high_coeffs, vmax=.8, square=True, )
        f.tight_layout()
        directory = prepare_directory("../plots/clusters/")
        plt.savefig(directory + "/correlationmatrix" + suffix + ".pdf")
        plt.close()

    def plot_dendrogram(self, **kwargs):
        plt.figure(figsize=(25, 10))
        plt.title('Hierarchical Clustering Dendrogram')
        plt.xlabel('sample index')
        plt.ylabel('distance')
        dendrogram(
            self.Z,
            leaf_rotation=90.,  # rotates the x axis labels
            leaf_font_size=8,  # font size for the x axis labels
            **kwargs
        )
        plt.show()
        plt.close()

    def plot_cluster_values(self, leader, members):
        plt.figure(figsize=(15, 10))
        plt.title("Cluster led by {}".format(leader))
        plt.xscale("log")
        plt.yscale("log")

        # http://colorbrewer2.org/#type=qualitative&scheme=Paired&n=9
        cycler = PlotCycler(len(members), colormap_name="Paired")

        for member in members:
            values = self.data.loc[:, [leader, member]] + 1  # type: pd.DataFrame
            values.sort_values(leader, inplace=True)
            fit = np.polyfit(values[leader], values[member], 1)
            fit_fn = np.poly1d(fit)
            xfit = np.geomspace(np.min(values[leader]), np.max(values[leader]), 100)
            yfit = fit_fn(xfit)
            plotargs = cycler.next_args()
            plt.plot(values[leader], values[member], label=member, **plotargs)
            plotargs.update(dict(linewidth=0.2, linestyle='-', marker=None))
            plt.plot(xfit[yfit > 1], yfit[yfit > 1], **plotargs)
        plt.legend(loc='best')

    def plot_cluster(self, suffix=''):
        cluster_dict, clusters, leaves, cluster_cum_sum = self.clusters(return_statistics=True)

        print("For threshold {:.3f}: {:d} clusters, {:d} single entries ({:d})".format(self.threshold, clusters, leaves,
                                                                                       leaves + cluster_cum_sum))
        suffix = "-" + suffix if suffix else ""
        directory = prepare_directory("../plots/clusters/")
        txt_file = open(directory + '/clusters-{:.3f}'.format(self.threshold) + suffix, 'wt')
        directory = prepare_directory(directory + "/{:.3f}{}", self.threshold, suffix)

        print_files = [txt_file, None]

        for leader, members in cluster_dict.items():
            self.plot_cluster_values(leader, members)
            plt.savefig(directory + '/' + leader + ".pdf")
            plt.close()
            for file in print_files:
                print("Cluster led by {}:".format(leader), members, file=file)
                print("Correlation: ",
                      self.matrix[self.data.columns.get_loc(leader),
                                  [self.data.columns.get_loc(member) for member in members]],
                      file=file)

    def explore_correlations(self, line_plots_thresholds, matrix_plot_threshold, suffix=''):

        self.plot_matrix(matrix_plot_threshold, suffix)

        # self.plotDendrogram(#p=150, truncate_mode='level',
        #                     labels=data.columns, color_threshold=0)

        for threshold in line_plots_thresholds:
            self.threshold = threshold
            self.plot_cluster(suffix)


if __name__ == "__main__":
    np.set_printoptions(precision=5, suppress=True)  # suppress scientific float notation
    from dataloading import BiopredictSourceData
    bpdata = BiopredictSourceData().dataset()

    correlatingData = CorrelatingDataRemover().fit(bpdata.data)
    correlatingData.explore_correlations([0.975, 0.95, 0.925, 0.9], 0.9)
    exit()

    correlatingData = CorrelatingDataRemover(cut_highest=True).fit(bpdata.data)
    correlatingData.explore_correlations([0.95, 0.90, 0.85, 0.8], 0.9, suffix='cuthigh')
    correlatingData = CorrelatingDataRemover(cut_highest=True, cut_lowest=True).fit(bpdata.data)
    correlatingData.explore_correlations([0.95, 0.90, 0.85, 0.8], 0.9, suffix='cutboth')
