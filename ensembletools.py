from typing import List, Union
import statistics

import numpy as np
import pandas as pd

from dataloading import DataProvider
from survivaltools import SurvivalTransformer
from numericutilities import sort_values_and_index, PandasRandomizer
from utilities import as_list


class EnsembleResultHandler:
    def __init__(self,
                 values_list: Union[pd.DataFrame, List[pd.DataFrame]],
                 scores_list: Union[pd.DataFrame, List[pd.DataFrame]],
                 directory: str = "."):
        self.values_list = as_list(values_list)
        self.scores_list = as_list(scores_list)
        self.directory = directory

    def allsums(self) -> List[pd.Series]:
        return [value.sum(axis=1) for value in self.values_list]

    def counts_sum(self):
        values_sum = pd.DataFrame()
        for values in self.values_list:
            values_sum = values_sum.add(values, fill_value=0)
        values_sum["sum"] = values_sum.sum(axis=1)
        return values_sum

    def save_values(self, suffix: str = ""):
        for values, scores in zip(self.values_list, self.scores_list):
            self.save_value(values, scores, suffix)

    def save_value(self, values: pd.DataFrame, scores: pd.DataFrame, suffix: str = ""):
        values = values.copy() # type: pd.DataFrame
        values["sum"] = values.sum(axis=1)
        sort_values_and_index(values, by="sum", values_ascending=False, inplace=True)
        values.to_csv(self.directory + "/counts{}.csv".format(suffix), sep=';')
        scores.to_csv(self.directory + "/scores{}.csv".format(suffix), sep=';', float_format="%.4f", decimal=',')

    def save_counts_sum(self, suffix: str = ""):
        values = sort_values_and_index(self.counts_sum(), by="sum", values_ascending=False)
        values.to_csv(self.directory + "/counts-sum{}.csv".format(suffix), sep=';')

    def save_aggregate_score(self, suffix: str = ""):
        self.aggregate_score().to_csv(self.directory + "/scores-aggregate{}.csv".format(suffix), sep=';',
                                      float_format="%.4f", decimal=',')

    def save_aggregate_score_statistics(self, suffix: str = ""):
        EnsembleResultHandler.statistics(self.aggregate_score()) \
            .to_csv(self.directory + "/scores-stats{}.csv".format(suffix), sep=';', float_format="%.4f", decimal=',')

    def aggregate_score(self) -> pd.DataFrame:
        return pd.concat(self.scores_list, axis=0)

    @staticmethod
    def statistics(data: pd.DataFrame) -> pd.DataFrame:
        statlabels = ["n", "min", "lower quartile", "mean", "median", "upper quartile", "max", "sd"]
        return pd.DataFrame({column: [f(data[column])
                                      for f in [len, min, lambda s: np.percentile(s, 25),
                                                statistics.mean, statistics.median,
                                                lambda s: np.percentile(s, 75), max,
                                                statistics.stdev]
                                      ]
                             for column in data.columns
                             },
                            index=[label for label in statlabels],
                            columns=data.columns)


def _rows(x: Union[pd.Series, pd.DataFrame],
          index) -> Union[pd.Series, pd.DataFrame]:
    if isinstance(x, pd.Series):
        return x[index]
    else:
        return x.loc[index]


class EnsembleSampler:
    def __init__(self,
                 bpdata: DataProvider,
                 transformer,
                 survival_transformer: Union[SurvivalTransformer, None]):
        self.bpdata = bpdata
        self.sample = None
        self.sample_control = None
        self.sample_source = None
        self.sample_control_source = None
        self.clinical_source = None
        self.clinical_sample = None
        self.clinical_sample_control = None
        self.clinical_extra_source = None
        self.transformer = transformer
        self.outcome_is_survival = False
        self.survival_transformer = survival_transformer
        self.extra_columns = survival_transformer.columns_needed() if survival_transformer else []
        self.pregenerated_samples = None

    def set_outcome(self,
                    column: Union[str, List[str]],
                    randomize_outcome: bool = False):
        # for now, simply survival data are two columns, all others are one column
        self.outcome_is_survival = isinstance(column, list)

        self.clinical_source = self.bpdata.metadata.loc[:, column]
        # check that clinical data is complete
        null_filter = self.clinical_source.isnull()
        if np.any(null_filter):
            if isinstance(null_filter, pd.DataFrame):
                null_filter = null_filter.any(axis=1)
            print("Missing clinical data for " + column + " in: ")
            print(self.bpdata.metadata.loc[null_filter, ["name", "firstname", "dob"]])
            self.clinical_source = _rows(self.clinical_source, ~null_filter)

        self.clinical_extra_source = self.bpdata.metadata.loc[self.clinical_source.index, self.extra_columns]

        # randomize if necessary
        if randomize_outcome:
            if self.outcome_is_survival and self.survival_transformer:
                self.clinical_source, self.clinical_extra_source = \
                    self.survival_transformer.randomize(self.clinical_source, self.clinical_extra_source)
            else:
                self.clinical_source = PandasRandomizer(self.clinical_source).randomize()
        # set sample and sample_control
        self._set_clinical()

    def outcome_inverse_transform(self,
                                  outcome_data: Union[pd.Series, pd.DataFrame]):
        if self.outcome_is_survival and self.survival_transformer:
            return self.survival_transformer.inverse_transform(outcome_data)
        else:
            return outcome_data

    def use_sample(self, subsamplesize: int) -> None:
        self._set_sample(self.bpdata.data.sample(n=subsamplesize))

    def pregenerate_samples(self, subsamplesize: int, n_samples: int):
        self.pregenerated_samples = [np.random.choice(self.bpdata.data.index.size, size=subsamplesize, replace=False)
                                     for _ in range(0, n_samples)]

    def use_pregenerated_sample(self, i: int) -> None:
        self._set_sample(self.bpdata.data.iloc[self.pregenerated_samples[i]])

    def use_all(self):
        self.sample_source = self.bpdata.data
        self.sample_control_source = None
        self._set_clinical()

    def _set_sample(self, sample: pd.DataFrame):
        self.sample_source = sample
        diff = self.bpdata.data.index.difference(self.sample_source.index)
        self.sample_control_source = self.bpdata.data.loc[diff, :]
        self._set_clinical()

    def _transformed_survival(self, index):
        sample = _rows(self.clinical_source, index)

        if self.outcome_is_survival and self.survival_transformer:
            extra_data = _rows(self.clinical_extra_source, index)
            sample = self.survival_transformer.transform(sample, extra_data)[0]

        return sample

    def _set_clinical(self):
        # This method works for both sequences: 1) use... 2) set_outcome, or 1) set_outcome and 2) use...
        if self.sample_source is not None and self.clinical_source is not None:
            with_control = self.sample_control_source is not None

            # create common indices
            index = self.sample_source.index.intersection(self.clinical_source.index)
            if with_control:
                control_index = self.sample_control_source.index.intersection(self.clinical_source.index)
            else:
                control_index = None

            # assign samples, can be a subset of sample_source if rows were lost from clinical null filtering
            self.sample = self.sample_source.loc[index]
            if with_control:
                self.sample_control = self.sample_control_source.loc[control_index]
            else:
                self.sample_control = None

            # transform samples if requested
            if self.transformer:
                self.sample = self.transformer.fit_transform(self.sample)
                if with_control:
                    self.sample_control = self.transformer.transform(self.sample_control)
                else:
                    self.sample_control = None

            # assign clinical samples
            self.clinical_sample = self._transformed_survival(index)
            if with_control:
                self.clinical_sample_control = self._transformed_survival(control_index)
            else:
                self.clinical_sample_control = None




