
import pandas as pd
from sklearn.linear_model import LinearRegression

from regressiontools import ModelFromFeatures


class BiopredictLinearRegression(ModelFromFeatures):
    def __init__(self, data: pd.DataFrame, **kwargs):
        super().__init__(model=LinearRegression(**kwargs), data=data)
