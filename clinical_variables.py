import pandas as pd
import numpy as np

import biopredict
from dataloading import BiopredictSourceData


class ClinicalVariables:

    def __init__(self,
                 source: BiopredictSourceData,
                 subjects=None):
        self.metadata = source.metadata
        if subjects is not None:
            self.metadata = self.metadata[self.metadata["id"].isin(subjects)]
        self.metadata = self.metadata.drop_duplicates(subset="id")
        self.local = source.local_epidemiology
        self.dict = biopredict.clinical_variable_imputation_dictionary()

    def dataset(self):
        """
        Returns a dataframe of the used clinical variables, indexed by sample, columns clinical variables.
        Missing data is imputed using local epidemiology.
        """

        def extract():
            for col in biopredict.clinical_variables():
                series = self.metadata[col].copy()
                missing = series.isnull()
                if missing.any():
                    imputed = self.metadata[missing].apply(self.dict[col],
                                                           axis=1,
                                                           reduce=True,
                                                           args=(self.local,))
                    series[missing] = imputed
                yield series

        return pd.concat(list(extract()) + [self.metadata["id"]], axis=1)

    def datasets(self,
                 n_datasets: int = None,
                 dataset_ids=None):
        """
        Returns n_datasets or len(dataset_ids) datasets with imputation from local epidemiology.
        Datasets are identified with a subset_id column and concatenated to long format (along axis 0)
        """
        if dataset_ids is None:
            dataset_ids = np.arange(n_datasets)

        def individual_datasets():
            for dataset_id in dataset_ids:
                df = self.dataset()
                df["subset_id"] = dataset_id
                yield df

        return pd.concat(list(individual_datasets()))


if __name__ == "__main__":
    print(ClinicalVariables(biopredict.source_data()).datasets(5))
