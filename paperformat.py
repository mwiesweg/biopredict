from typing import Tuple, Union

class DinAFormat:

    value_dict = {
        0: (33.11, 46.81),
        1: (23.39, 33.11),
        2: (16.54, 23.39),
        3: (11.69, 16.54),
        4: (8.27, 11.69),
        5: (5.83, 8.27),
        6: (4.13, 5.83),
        7: (2.91, 4.13),
        8: (2.05, 2.91),
        9: (1.46, 2.05),
        10: (1.02, 1.46),
    }

    @staticmethod
    def portrait(number: int, adjustment: Union[int, Tuple[int,int]] = None) -> Tuple[int, int]:
        if number is None:
            raise ValueError("No DIN A number given")
        if not 0 <= number <= 10:
            raise ValueError("Invalid DIN A number")
        value = DinAFormat.value_dict[number]
        if adjustment is not None:
            if isinstance(adjustment, tuple):
                return value[0]*adjustment[0], value[1]*adjustment[1]
            else:
                return value[0]*adjustment, value[1]*adjustment
        return value

    @staticmethod
    def landscape(number: int, adjustment: Union[int, Tuple[int,int]] = None) -> Tuple[int, int]:
        if isinstance(adjustment, tuple):
            adjustment = adjustment[::-1]
        return DinAFormat.portrait(number, adjustment)[::-1]

    @staticmethod
    def longer(number: int) -> int:
        return DinAFormat.portrait(number)[0]

    @staticmethod
    def shorter(number: int) -> int:
        return DinAFormat.portrait(number)[1]


class DinA:

    def __init__(self, number: int, adjustment: Union[int, Tuple[int,int]] = None):
        self.number = number
        self.adjustment = adjustment

    def portrait(self, adjustment: Union[int, Tuple[int,int]] = None) -> Tuple[int, int]:
        return DinAFormat.portrait(self.number, self.adjustment if adjustment is None else adjustment)

    def landscape(self, adjustment: Union[int, Tuple[int,int]] = None) -> Tuple[int, int]:
        return DinAFormat.landscape(self.number, self.adjustment if adjustment is None else adjustment)

    def longer(self) -> int:
        return DinAFormat.longer(self.number)

    def shorter(self) -> int:
        return DinAFormat.shorter(self.number)

    def __mul__(self, other: Union[int, Tuple[int]]):
        return DinA(self.number, adjustment=other)
