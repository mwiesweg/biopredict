import os
from typing import Union, List, Tuple

import pandas as pd
import numpy as np

# import rpy2.robjects as robjects

import rpy2.robjects as ro
from rpy2.robjects.packages import importr
from rpy2.robjects.packages import SignatureTranslatedAnonymousPackage
from rpy2.robjects import pandas2ri
from rpy2.robjects.conversion import localconverter
import rpy2.robjects.numpy2ri
from rpy2.rinterface_lib.embedded import RRuntimeError


class RInterface:

    # Static vars: some basic packages
    base = importr("base")

    def __init__(self):
        self.package = None

    def __getattr__(self, attr):
        return getattr(self.package, attr)

    @staticmethod
    def reset_seed():
        RInterface.base.set_seed(ro.r.NULL)

    @staticmethod
    def _load_r_file(filename: str):
        dirname = os.path.dirname(os.path.abspath(__file__)) + "/../"
        with open(dirname + filename, 'r') as source_file:
            source = source_file.read()
        return SignatureTranslatedAnonymousPackage(source, filename)


class Survival(RInterface):

    def __init__(self):
        super().__init__()
        self.package = importr("biopredictR")

    def gamma_impute(self,
                     data: pd.DataFrame,
                     survival_columns: List[str],
                     strata_columns: List[str],
                     dco_column: str,
                     informative_censoring_column: str,
                     m: int) -> List[pd.DataFrame]:
        with localconverter(ro.default_converter + pandas2ri.converter):
            df = ro.conversion.py2rpy(data)
        # conversion to data frame will mangle names via make.names!
        colnames_dict = dict(zip(data.columns, df.colnames))

        def tr(x):
            if type(x) is list:
                return [colnames_dict[s] for s in x]
            else:
                return colnames_dict[x]

        imputed_dfs = self.package.survivalGammaImpute(df,
                                                       tr(survival_columns),
                                                       tr(strata_columns),
                                                       tr(dco_column),
                                                       tr(informative_censoring_column),
                                                       max(m, 2))[:m]  # gamma impute wants M >= 2

        def to_named_pandas(df):
            with localconverter(ro.default_converter + pandas2ri.converter):
                df = ro.conversion.rpy2py(df)
            df.index = data.index
            df.columns = survival_columns
            return df

        return [to_named_pandas(imputed_df) for imputed_df in imputed_dfs]

    def iauc(self,
             truth: pd.DataFrame,
             prediction,
             time_span = None):
        truth = pandas2ri.py2ri(truth)
        if not time_span:
            time_span = ro.r.NULL
        else:
            time_span = np.atleast_1d(time_span)
        return self.package.iAUC_survival_vs_predicted_time(outcome = truth,
                                                            predicted_time = prediction,
                                                            time_span = time_span)[0]

    def survival_metadata(self,
                          survival: pd.DataFrame):
        with localconverter(ro.default_converter + pandas2ri.converter):
            return ro.conversion.rpy2py(self.package.survival_metadata(survival, format_numbers=False)).iloc[0]

    def cox_lasso_by_nonzero(self,
                             x: pd.DataFrame,
                             y: pd.DataFrame,
                             requested_non_zero: int = None) -> Tuple[np.ndarray, float]:
        time = None
        status = None
        if "time" in y.columns:
            time = y["time"]
        if "status" in y.columns:
            status = y["status"]
        if time is None or status is None:
            if y.columns.size != 2:
                raise ValueError("Need a data frame with at least two columns \"time\" and \"status\", "
                                 "or a data frame with exactly two columns for time (>=0) and status (0,1)")
        if time is None:
            time = y.iloc[:,0]
        if status is None:
            status = y.iloc[:,1]
        status = status.astype(np.int_)
        y = pd.concat([time, status], axis=1, keys = ["time", "status"])

        with localconverter(ro.default_converter + pandas2ri.converter):
            df_x = ro.conversion.rpy2py(x)
            df_y = ro.conversion.rpy2py(y)

        try:
            if requested_non_zero is None:
                value = self.package.cox_lasso_by_nonzero(df_x, df_y)
            else:
                value = self.package.cox_lasso_by_nonzero(df_x, df_y, requested_non_zero)
        except RRuntimeError as err:
            raise RuntimeError from err
        return np.array(value.rx2("coefficients")), value.rx2("iAUC")[0]


if __name__ == "__main__":
    df1 = pd.DataFrame({"time IO": [30,40,5,60,70,5],
                        "status": [1,0,1,0,1,0],
                        "line": [1,2,3,1,2,3],
                        "DCO IO": [40,100,100,70,120, 250],
                        "ic": [False, True, False, True, False, False]
                        })
    survival = Survival()
    imputed = survival.gamma_impute(df1, ["time IO", "status"], ["line"], "DCO IO", "ic", 5)
    survival_data = df1[["time IO", "status"]]
    print(survival_data)
    for df in imputed:
        print(df)
        iauc = survival.iauc(survival_data, df.iloc[:,0])
        print("AUC:", iauc)
    from survivaltools import SurvivalTransformer
    t = SurvivalTransformer(False, False)

    for _ in range(0,5):
        randomized, _ = t.randomize(survival_data)
        iauc = survival.iauc(randomized, survival_data.iloc[:,0])
        print("AUC of randomized:", iauc, " ", type(iauc))