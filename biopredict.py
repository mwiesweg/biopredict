import os

from typing import List, Sequence

from sklearn.pipeline import Pipeline, FeatureUnion

import transformers as tr
import signatures as sig
from dataloading import DataProvider, BiopredictSourceData, BerlinData
from correlations import CorrelatingDataRemover
from survivaltools import SurvivalTransformer
from fileutilities import prepare_directory

# Common choices in a central file


def possible_modes():
    return ["rna", "rna-signatures", "rna-cell-composition", "lab"]


def applied_modes():
    return ["rna",
            "rna-signatures"
            # "rna-cell-composition",
            # "lab"
            ]


def _directory(mode, directory, base):

    if mode == "rna":
        pass
    else:
        base = "{}-{}".format(mode, base)

    symlink = "../{}/current-{}".format(directory, base)
    if os.path.exists(symlink):
        print("Output to {}/current-{}".format(directory, base))
        return symlink
    return prepare_directory("../{}/{}".format(directory, base))


def ensemble_directory(mode: str):
    return _directory(mode, "plots", "ensembles")


def cv_directory(mode: str):
    return _directory(mode, "fit", "cv")


def model_directory(mode: str):
    return _directory(mode, "fit", "model")


def predefined_reference_genes():
    with open("../Rohdaten/reference_genes") as f:
        return f.read().splitlines()


def source_data():
    return BiopredictSourceData(raw_data_ids="../Rohdaten/ids.csv",
                                raw_data_files=["../Rohdaten/cohorts1-3.csv",
                                                "../Rohdaten/cohort4.csv"],
                                clinical_data="../Klinische Daten/Klinische Daten.json",
                                cohort_assignment_dir="../Klinische Daten/cohorts",
                                local_epidemiology="../Klinische Daten/local epi.csv")


def berlin_data():
    return BerlinData(raw_data_ids="../Rohdaten/ids-berlin.csv",
                      raw_data_file="../Rohdaten/cohort-berlin.csv",
                      clinical_data="../Klinische Daten/Klinische Daten Kohorte Berlin.csv")


def dataset(mode: str,
            intention="learn",
            remove_correlating=True,
            transformer="default",
            source_data=source_data()) -> DataProvider:
    query = ""

    if mode.startswith("rna"):
        if intention == "learn":
            query = "biopredict == 'learn'"
        elif intention == "validate":
            query = "biopredict == 'validation'"
        elif intention == "all":
            query = "biopredict == 'learn' or biopredict == 'validation'"
        else:
            raise ValueError("Unknown intention", intention)

        dset = source_data.dataset(drop_invalid=True,
                                   #filter_cohorts=[1, 2],
                                   #special_filter="radiomics discovery",
                                   query=query,
                                   add_best_response=True)
    elif mode == "lab":
        if intention == "learn":
            query = "biopredict != 'validation'"
        elif intention == "validate":
            query = "biopredict == 'validation'"
        else:
            raise ValueError("Unknown intention", intention)

        dset = source_data.lab_dataset(query=query,
                                       lab_required=lab_core(),
                                       lab_subset=lab_core())
    elif mode == "metadata":
        dset = source_data.dataset()
    else:
        raise ValueError("Unknown mode", mode)

    # use passed transformer if provided (typically stored transformer), else use standard primary transformer
    # standard primary can be None!
    # passed transformer is assumed to be already fit
    if transformer == "default":
        transformer = primary_transformer(mode, remove_correlating)
        if transformer is not None:
            transformer.fit(dset.data, dset.metadata)

    if transformer is not None:
        dset.transform_data(transformer, inplace=True)

    return dset


def clinical_variables():
    return ["EGFR Exon 19 & 21", "PD-L1 TPS"]


def clinical_variable_imputation_dictionary():

    def sample(df, col):
        # Get one scalar value from the non-missing data of df[col]
        series = df[col]
        return series[~series.isnull()].sample(n=1).iloc[0]

    def _impute_egfr(metadata, local):
        if metadata["entity"] == "NSCLC/Sq":
            return 0
        elif metadata["RAS Mutation"] == 1:
            return 0
        return sample(local, "EGFR Exon 19 & 21")

    def _impute_pdl1(metadata, local):
        return sample(local, "PD-L1 TPS")

    return {"EGFR Exon 19 & 21": _impute_egfr,
            "PD-L1 TPS": _impute_pdl1}


def primary_transformer(mode: str,
                        remove_correlating=True):
    if mode.startswith("rna"):
        normalizer = tr.NCounterTransformer(method="ref", reference_genes=predefined_reference_genes())
        if remove_correlating:
            return tr.make_dataframe_pipeline(normalizer,
                                              CorrelatingDataRemover(threshold=0.925),
                                              None)
        else:
            return normalizer
    elif mode == "lab":
        if remove_correlating:
            return CorrelatingDataRemover(threshold=0.9)
        else:
            return None


def preprocessor(mode: str) -> Pipeline:
    return pipeline(mode, None)


def pipeline(mode: str,
             estimator: object) -> Pipeline:
    if mode == "rna":
        return tr.make_dataframe_pipeline(tr.FeatureSubset(),
                                          tr.BoxCoxTransformer(),
                                          tr.StoringStandardScaler(),
                                          estimator)
    elif mode == "rna-signatures":
        # the alternative pipeline StoringStandardScaler -> SignaturesExtractor(mean) seemed roughly equivalent
        return tr.make_dataframe_pipeline(sig.SignaturesExtractor(aggregation="gmean"),
                                          tr.StoringStandardScaler(),
                                          estimator)
    elif mode == "rna-cell-composition":
        # note: cache the composition result of the dataset once by calling signatures.py --cache-composition
        return tr.make_dataframe_pipeline(sig.CellCompositionTransformer(from_cache=True),
                                          tr.StoringStandardScaler(),
                                          estimator)
    elif mode == "lab":
        return tr.make_dataframe_pipeline(
            tr.make_dataframe_union(
                tr.make_dataframe_transformer(tr.LabStandardScaler()),
                tr.make_dataframe_pipeline(tr.LabAvoidZeroTransformer(),
                                           tr.AllRatiosTransformer(),
                                           tr.BoxCoxTransformer()#,
                                           #tr.StoringStandardScaler()
                                           )
            ),
            tr.FeatureSubset(),  # subset selection after the all-ratios transformer
            estimator
        )


def cv_feature_range(mode: str) -> Sequence:
    if mode == "rna" or mode == "lab":
        return range(3, 8)
    elif mode == "rna-signatures" or mode == "rna-cell-composition":
        return [None]


def lab_core() -> List:
    return ["Bilirubin", "CRP", "Calcium", "GOT", "GPT", "Ges.-Eiweiß", "Hämoglobin", "Kalium", "LDH", "Leukozyten",
            "Lymphozyten", "Monozyten", "Natrium", "Neutrophile", "Thrombozyten"]


def dataset_ids(df):
    """
    Creates a list of database id for entries in given df (data or metadata).
    The id can be a tuple (id, intra-patient) if there are intra-patient duplicate samples,
    or is only the id (integer) if not.
    """
    if "intra-patient" in df:
        return [(df.at[idx, "id"], df.at[idx, "intra-patient"]) for idx in df.index]
    else:
        return df["id"]


def isin_dataset_ids(id_, ids):
    for entry in ids:
        if type(id_) == type(entry):
            result = entry == id_
        elif isinstance(id_, tuple):
            result = entry == id_[0]
        else:
            result = entry[0] == id_
        if result:
            return True
    return False


def id_columns():
    return ["id", "intra-patient"]


def drop_id_columns(df):
    return df.drop(id_columns(), axis=1, errors="ignore")


def assign_dataset_id(x, id_):
    if isinstance(id_, tuple):
        x["id"] = id_[0]
        x["intra-patient"] = id_[1]
    else:
        x["id"] = id_
    return x


def dataset_id_strata():
    """
    For learning intention:
    Returns a list of non-overlapping pd.Indexes that differ in availability of data.
    The first item is considered the "core" set, further entries add sets of patients
    for whom not all data is available.
    """
    rna_ids = dataset("rna").metadata
    lab_ids = dataset("lab").metadata
    only_lab_ids = lab_ids[~lab_ids["id"].isin(rna_ids["id"])]
    return [dataset_ids(df) for df in [rna_ids, only_lab_ids]]


def dataset_id_type(mode):
    if mode == "lab":
        return "per-patient"
    else:
        return "per-sample"


def survival_transformer(dset: DataProvider = None) -> SurvivalTransformer:
    if dset is not None:  # context: training / fit
        return SurvivalTransformer(
            drop_censoring_column=True,  # one of our underlying assumptions...
            log_transform=False,
            score_method="rmse",
            imputation="gamma",
            imputations_params={"strata": dset.columns_for_type("covariates"),
                                "dco": dset.columns_for_type("dco"),
                                "informative_censoring": dset.columns_for_type("informative_censoring"),
                                "m": 1}
        )
    else:  # context: prediction
        return SurvivalTransformer(
            drop_censoring_column=True,
            log_transform=False,
            score_method="rmse",
            imputation=None)


def store_current():
    ds = dataset("rna")
    prep = preprocessor("rna")
    ds.data.reset_index().to_feather("../data/data.feather")
    data_transformed = prep.fit_transform(ds.data)
    data_transformed.reset_index().to_feather("../data/transformed-data.feather")
    ds.metadata.drop(["lab value IO during line IO",
                      "participating in Biopredict-Projekt",
                      "participating in Radiomics PDL1 Kohorte"
                      ], axis=1).reset_index().to_feather("../data/metadata.feather")


def export_used_data(format):
    if format == "de":
        format_args = {"sep": ";", "decimal": ","}
    elif format == "en":
        format_args = {"sep": ",", "decimal": "."}
    else:
        raise ValueError("Unsupported format:", format)

    for intent in ["learn", "validate"]:
        dset = dataset("rna",
                       intention=intent,
                       transformer=None)
        dset.data.\
            reset_index(drop=True)\
            .to_csv("../data/{}-data.csv".format(intent), **format_args)
        dset.metadata.\
            drop(["name", "firstname", "dob", "medico id", "id",
                  "directory", "ncounterid", "filename", "id-prenormalized",
                  "lab value IO during line IO",
                  "last documentation", "start date IO",
                  "participating in Biopredict-Projekt",
                  "participating in Radiomics PDL1 Kohorte",
                  "radiomics"],
                 axis=1).\
            round({"age at dx": 1}).\
            reset_index(drop=True).\
            to_csv("../data/{}-metadata.csv".format(intent), **format_args)



def primary_endpoint(metadata):
    return (
        metadata["best response IO"].isin(["MinorResponse", "PartialResponse",
                                           "CompleteResponse", "NoEvidenceOfDisease"]) |
        ((metadata["best response IO"] == "StableDisease") & (metadata["TTF IO"] > 150))
    )


if __name__ == "__main__":
    # store_current()
    export_used_data(format="en")
    exit()
    ds = dataset("rna", "validate")
    ds.metadata.to_csv("../data/validation-metadata.csv", sep=";", decimal=",")
    ds = source_data().lab_dataset(query="biopredict == 'validation'",
                                   #lab_required=lab_core(),
                                   lab_subset=lab_core())
    with_name = ds.data.copy()
    with_name["name"] = ds.metadata["name"]
    with_name.to_csv("../data/validation-labdata.csv", sep=";", decimal=",")