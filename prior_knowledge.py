import pandas as pd
import numpy as np
import scipy.stats as st
from statsmodels.stats.multitest import fdrcorrection
import feather
import os

import biopredict


def quantile_normalize(df_input):
    # (c) shawnlyu@cs.toronto.edu
    df = df_input.copy()
    #compute rank
    dic = {}
    for col in df:
        dic.update({col : sorted(df[col])})
    sorted_df = pd.DataFrame(dic)
    rank = sorted_df.mean(axis = 1).tolist()
    #sort
    for col in df:
        t = np.searchsorted(np.sort(df[col]), df[col])
        df[col] = [rank[i] for i in t]
    return df


def read_file_to_list(filename):
    with open(filename, "r") as f:
        return f.read().splitlines()

def known_checkpoints_treg():
    return read_file_to_list("../signatures/prior_knowledge/checkpoints_treg")


def known_signatures():
    path = "../signatures/prior_knowledge/"
    filenames = ["ayers_ifngamma",
                 "ayers_expanded"]
    return { filename: read_file_to_list(path + filename) for filename in filenames }


def consolidate_genes(genes, dset):
    if not isinstance(genes, set):
        genes = set(genes)
    print(genes)
    missing = genes - set(dset.data.columns)
    if missing:
        print("Missing: "+ str(missing))
    return list(genes & set(dset.data.columns))


def single_genes():
    genes = set(known_checkpoints_treg() + [gene for key, genelist in known_signatures().items() for gene in genelist])
    dset = biopredict.dataset("rna", "learn", remove_correlating=False)
    genes = consolidate_genes(genes, dset)
    #print([gene for gene in dset.data.columns if gene.startswith("HLA")])
    a = dset.data[dset.metadata["best response binary"] == "good"]
    b = dset.data[dset.metadata["best response binary"] == "bad"]
    df = pd.DataFrame(index=genes)

    for gene in genes:
        statistic, pvalue =  st.ttest_ind(a[gene], b[gene], equal_var=False)
        df.loc[gene, "p-value t-test"] = pvalue
        statistic, pvalue =  st.mannwhitneyu(a[gene], b[gene], alternative="two-sided")
        df.loc[gene, "p-value Mann-Whitney"] = pvalue
    rejected, df["p-value t-test corrected"] = fdrcorrection(df["p-value t-test"])
    rejected, df["p-value Mann-Whitney corrected"] = fdrcorrection(df["p-value Mann-Whitney"])

    df.to_csv("../prediction/prior_knowledge_single_genes.csv", sep=";", decimal=",")


def ayers():
    dset = biopredict.dataset("rna", "learn", remove_correlating=False, transformer=None)
    df = np.log10(quantile_normalize(dset.data))
    signatures = known_signatures()
    result = dset.metadata[dset.classification_columns() +
                           [col for cols in dset.survival_columns() for col in cols] +
                           ["PD-L1 TPS", "entity"]].copy()

    for name in signatures:
        genes = consolidate_genes(signatures[name], dset)
        score = df[genes].mean(axis=1)
        result["score " + name] = score
        a = score[dset.metadata["best response binary"] == "good"]
        b = score[dset.metadata["best response binary"] == "bad"]
        print("Good n={} mean={} vs. bad n={} mean={} ".format(a.shape[0], np.mean(a), b.shape[0], np.mean(b)))
        statistic, pvalue =  st.ttest_ind(a, b, equal_var=False)
        print("T-test for {}: p={}".format(name, pvalue))

    result.to_csv("../prediction/prior_knowledge_signatures.csv", sep=";", decimal=",")


if __name__ == "__main__":
    #single_genes()
    ayers()