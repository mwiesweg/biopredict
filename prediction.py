from typing import List, Callable
from ast import literal_eval

from plotutilities import import_matplotlib
import_matplotlib()

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import feather
import os
from collections import OrderedDict

from progress.bar import Bar

from sklearn.svm import SVC, SVR
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.linear_model import LogisticRegression, ElasticNet
from sklearn.model_selection import ShuffleSplit, StratifiedShuffleSplit
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import confusion_matrix, accuracy_score, roc_auc_score
from xgboost import XGBClassifier, XGBRegressor

import biopredict
import transformers as tr
from clinical_variables import ClinicalVariables
from postrun import top_entries
from fileutilities import prepare_directory
from dataloading import DataProvider
from survivaltools import SurvivalEstimator
from modelevaluation import plot_confusion_matrix
from paperformat import DinA
from utilities import StopWatch
from transformers import WrappingEstimator
from visualization import plot_decision_boundaries


rf_random_state = 47


def _subset_index_list(choice,
                       n_subsets,
                       frac,
                       random_state,
                       exclude):
    """
    Returns a list of size n_subsets containing index entries from choice randomly chosen using n, frac, random_state,
    and not including entries in exclude.
    """
    if not isinstance(choice, pd.Index):
        choice = pd.Index(choice)
    if exclude is not None:
        choice = choice.drop(exclude, errors="ignore")

    n = int(round(frac * len(choice)))

    def subsets():
        for _ in range(n_subsets):
            locs = random_state.choice(len(choice), size=n, replace=False)
            yield choice.take(locs)

    return list(subsets())


def _subset_index_list_stratified(choice_sets,
                                  n_subsets,
                                  frac,
                                  random_state,
                                  exclude):
    # we create a list [ [c_1, c_2, ...], [d_1, d_2, ...], ...] for choice sets c, d, ... and subsets 1,2,...,n_subsets
    list_of_lists_of_indices = [_subset_index_list(choice_set, n_subsets, frac, random_state, exclude)
                                for choice_set in choice_sets]
    # change it to list of tuples [ (c_1, d_1), (c_2, d_2) ]
    index_tuples = zip(*list_of_lists_of_indices)

    def concatenated_indices():
        for index_tuple in index_tuples:
            result_index = None
            for sub_index in index_tuple:
                if result_index is None:
                    result_index = sub_index
                else:
                    result_index = result_index.append(sub_index)
            yield result_index

    return list(concatenated_indices())


class SubsetList:
    """
    Creates a list of n_subsets subsets of the dataframe index
    """

    def __init__(self,
                 dataset: DataProvider):
        self.dataset = dataset
        self.subsets = [self.dataset.data.index]

    def generate_subsets(self,
                         n_subsets: int,
                         frac,
                         random_seed: int=None):
        random_state = np.random.RandomState(random_seed)
        self.subsets = _subset_index_list(self.dataset.data,
                                          n_subsets, frac, random_state, None)

    def __iter__(self):
        return iter(self.subsets)

    def __len__(self):
        return len(self.subsets)


class SubsetsButOneDict:
    """
    An ordered dictionary
    subject -> list of n_subset subsets of choice_sets, not containing the subject
    From each entry of choice_sets, subjects are drawn independently
    (allows stratification, also allows double entries if choice_sets are overlapping)
    """

    def __init__(self,
                 choice_sets,
                 subjects):
        self.choice_sets = choice_sets
        self.subjects = subjects
        self.subsets = OrderedDict()
        self.n_subsets_per_subject = 0

    def generate_subsets(self,
                         n_subsets: int,
                         frac,
                         random_seed: int=None):
        self.n_subsets_per_subject = n_subsets
        random_state = np.random.RandomState(random_seed)
        self.subsets = OrderedDict([
            (label, _subset_index_list_stratified(self.choice_sets,
                                                  n_subsets, frac, random_state,
                                                  exclude=label))
            for label in self.subjects
        ])

    def filter(self,
               available_subjects):
        # print(available_subjects)

        def filtered_subset_list(subsets_list):
            for subset in subsets_list:
                yield [x for x in subset if biopredict.isin_dataset_ids(x, available_subjects)]

        filtered = SubsetsButOneDict(self.choice_sets, available_subjects)
        filtered.n_subsets_per_subject = self.n_subsets_per_subject
        for subject, subsets_list in self.items():
            # print(subject, biopredict.isin_dataset_ids(subject, available_subjects))
            if biopredict.isin_dataset_ids(subject, available_subjects):
                subsets_list = list(filtered_subset_list(subsets_list))
                filtered.subsets[subject] = subsets_list
        return filtered

    def __iter__(self):
        return iter(self.subsets)

    def __len__(self):
        return len(self.subsets)

    def items(self):
        return self.subsets.items()


n_jobs = 10


def _make_split(type_):
    if type_ == "classification":
        return StratifiedShuffleSplit(n_splits=10,
                                      test_size=0.2,
                                      random_state=1)
    else:
        return ShuffleSplit(n_splits=20,
                            test_size=0.3,
                            random_state=1)


def _make_grid(pipeline,
               param_grid,
               type_: str,
               subset,
               dataset):
    if subset:
        pipeline.named_steps["featuresubset"].subset = subset

    initial_transformer = pipeline.steps[0][1]
    if hasattr(initial_transformer, "unfiltered_data"):
        initial_transformer.unfiltered_data = dataset.data

    cv = _make_split(type_)

    return GridSearchCV(pipeline,
                        param_grid=param_grid,
                        cv=cv,
                        verbose=2,
                        return_train_score=True,
                        n_jobs=n_jobs
                        )


def fit_grid(mode, grid, X, y, label, suffix, **fit_params):
    watch = StopWatch(process_time=False)
    print("Fitting grid for", label, suffix)
    try:
        grid.fit(X, y, **fit_params)
    except ValueError as e:
        print(e)
        return None
    watch.print_elapsed("Grid for {}, {} took ".format(label, suffix))

    pd.DataFrame(grid.cv_results_).to_csv(biopredict.cv_directory(mode) + "/" + suffix + ".csv", sep=";", decimal=",")

    # sometimes, parameters may be tuples, which makes pandas choke
    def flatten_params(best_params):
        for key, value in best_params.items():
            if isinstance(value, tuple) or isinstance(value, list):
                yield key, str(value)
            else:
                yield key, value

    return pd.DataFrame({
        **{"method": label,
           "best score": grid.best_score_},
        **{key: value for key, value in flatten_params(grid.best_params_)}
    }, index=[0])


def create_pipeline(mode: str,
                    type_: str,
                    estimator,
                    dataset: DataProvider = None,
                    survival_columns: List[str] = None):
    if type_ == "survival":
        transformer = biopredict.survival_transformer(dataset)
        wrapper = SurvivalEstimator(estimator, transformer, survival_columns)
        return biopredict.pipeline(mode, wrapper)
    else:
        return biopredict.pipeline(mode, estimator)


def svm_classification_parameters(mode,
                                  X,
                                  y,
                                  dataset,
                                  suffix: str,
                                  subset):
    n_steps = 20
    C_range = np.logspace(-2, 6, n_steps)
    gamma_range = np.logspace(-9, 3, n_steps)
    grid = [
        {'svc__kernel': ['sigmoid'],
         'svc__C': C_range,
         'svc__gamma': gamma_range},
        {'svc__C': C_range,
         'svc__kernel': ['linear']},
        {'svc__C': C_range,
         'svc__gamma': gamma_range,
         'svc__kernel': ['rbf']}
        #{'svc__C': [1, 100],
        # 'svc__degree': [2],
        # 'svc__kernel': ['poly']},
    ]
    pipeline = biopredict.pipeline(mode, SVC())
    grid = _make_grid(pipeline, grid, "classification", subset, dataset)
    return fit_grid(mode, grid, X, y, "SVC", "svc-" + suffix)


def rf_classification_parameters(mode,
                                 X,
                                 y,
                                 dataset,
                                 suffix: str,
                                 subset):
    rf = RandomForestClassifier(max_features=None,
                                max_depth=None,
                                random_state=rf_random_state)
    n_steps = 20
    grid = {
        'randomforestclassifier__n_estimators': np.round(np.logspace(1, 4, n_steps)).astype(np.int),
        'randomforestclassifier__criterion': ["gini", "entropy"],
        'randomforestclassifier__min_samples_split': [2, 4, 8],
        'randomforestclassifier__bootstrap': [True, False]
    }
    pipeline = biopredict.pipeline(mode, rf)
    grid = _make_grid(pipeline, grid, "classification", subset, dataset)
    return fit_grid(mode, grid, X, y, "RandomForest C", "rfc-" + suffix)


def xgb_classification_parameters(mode,
                                  X,
                                  y,
                                  dataset,
                                  suffix: str,
                                  subset):
    xgb = XGBClassifier()
    grid = {
        'xgbclassifier__learning_rate': [0.2, 0.4, 0.6, 0.8],
        'xgbclassifier__min_child_weight': [1, 2, 3],
        'xgbclassifier__gamma': [0, 0.1, 0.2, 0.3, 0.4, 0.5],
        'xgbclassifier__subsample': [0.6, 0.8, 0.9, 1.0],
        'xgbclassifier__colsample_bytree': [0.4, 0.5, 0.6, 0.7, 0.8],
        'xgbclassifier__max_depth': [2, 3, 6, 8, 10]
    }
    pipeline = biopredict.pipeline(mode, xgb)
    grid = _make_grid(pipeline, grid, "classification", subset, dataset)
    return fit_grid(mode, grid, X, y, "XGB C", "xgbc-" + suffix)


def logreg_classification_parameters(mode,
                                     X,
                                     y,
                                     dataset,
                                     suffix: str,
                                     subset):
    pipeline = biopredict.pipeline(mode, LogisticRegression())

    n_steps = 20
    param_grid = {
        "logisticregression__C": np.logspace(-2, 10, n_steps)
    }

    grid = _make_grid(pipeline, param_grid, "classification", subset, dataset)
    return fit_grid(mode, grid, X, y, "Logistic Regr", "lr-" + suffix)


def en_regression_parameters(mode,
                             X,
                             dataset: DataProvider,
                             survival_columns,
                             suffix: str,
                             subset):
    pipeline = create_pipeline(mode, "survival", ElasticNet(), dataset, survival_columns)

    n_steps = 15
    param_grid = {
        "survivalestimator__alpha": np.logspace(-6, 3, n_steps),
        "survivalestimator__l1_ratio": np.arange(0.1, 1, 0.1)
    }

    grid = _make_grid(pipeline, param_grid, "regression", subset, dataset)
    return fit_grid(mode, grid, X, dataset.metadata, "Elastic Net", "en-" + suffix)


def svm_regression_parameters(mode,
                              X,
                              dataset: DataProvider,
                              survival_columns,
                              suffix: str,
                              subset):
    pipeline = create_pipeline(mode, "survival", SVR(), dataset, survival_columns)

    n_steps = 8
    if mode == "lab":
        C_range = np.logspace(-2, 5, n_steps)
    else:
        C_range = np.logspace(-2, 6, n_steps)
    epsilon_range = [0.01, 0.1, 0.5, 1, 4]
    grid = [
        {'survivalestimator__kernel': ['linear'],
         'survivalestimator__C': C_range,
         'survivalestimator__epsilon': epsilon_range},
        {'survivalestimator__kernel': ['rbf'],
         'survivalestimator__C': C_range,
         'survivalestimator__gamma': np.logspace(-9, 3, n_steps),
         'survivalestimator__epsilon': epsilon_range},
        # {'survivalestimator__kernel': ['poly'],
        # 'survivalestimator__C': [1, 100],
        # 'survivalestimator__degree': [2],
        # 'survivalestimator__epsilon': [0.1, 1]},
        {'survivalestimator__kernel': ['sigmoid'],
         'survivalestimator__C': C_range,
         'survivalestimator__epsilon': epsilon_range}
    ]
    grid = _make_grid(pipeline, grid, "regression", subset, dataset)
    return fit_grid(mode, grid, X, dataset.metadata, "SVR", "svr-" + suffix)


def rf_regression_parameters(mode,
                             X,
                             dataset: DataProvider,
                             survival_columns,
                             suffix: str,
                             subset):
    rf = RandomForestRegressor(max_features=None,
                               max_depth=None,
                               random_state=rf_random_state)
    n_steps = 20
    grid = {
        'survivalestimator__n_estimators': np.round(np.logspace(1, 4, n_steps)).astype(np.int),
        'survivalestimator__criterion': ["mse", "mae"],
        'survivalestimator__min_samples_split': [2, 4, 8],
        'survivalestimator__bootstrap': [True, False]
    }
    pipeline = create_pipeline(mode, "survival", rf, dataset, survival_columns)
    grid = _make_grid(pipeline, grid, "regression", subset, dataset)
    return fit_grid(mode, grid, X, dataset.metadata, "Random Forest Regr", "rfr-" + suffix)


def xgb_regression_parameters(mode,
                              X,
                              dataset: DataProvider,
                              survival_columns,
                              suffix: str,
                              subset):
    xgb = XGBRegressor()
    grid = {
        'survivalestimator__learning_rate': [0.2, 0.3],
        'survivalestimator__min_child_weight': [1, 2],
        'survivalestimator__gamma': [0, 0.2, 0.5],
        'survivalestimator__subsample': [0.6, 0.9, 1.0],
        'survivalestimator__colsample_bytree': [0.5, 0.6, 0.7, 0.8],
        'survivalestimator__max_depth': [3, 6, 8, 10]
    }
    grid_long = {
        'survivalestimator__learning_rate': [0.2, 0.4, 0.6, 0.8],
        'survivalestimator__min_child_weight': [1, 2, 3],
        'survivalestimator__gamma': [0, 0.1, 0.2, 0.3, 0.4, 0.5],
        'survivalestimator__subsample': [0.6, 0.8, 0.9, 1.0],
        'survivalestimator__colsample_bytree': [0.4, 0.5, 0.6, 0.7, 0.8],
        'survivalestimator__max_depth': [2, 3, 6, 8, 10]
    }
    pipeline = create_pipeline(mode, "survival", xgb, dataset, survival_columns)
    grid = _make_grid(pipeline, grid, "regression", subset, dataset)
    return fit_grid(mode, grid, X, dataset.metadata, "XGB R", "xgbr-" + suffix)


def _columns_for_type(dataset, type_):
    if type_ == "survival":
        return dataset.survival_columns()
    else:
        l = dataset.classification_columns()
        l.remove("best response IO")
        return l


def drop_missing_outcome(dataset: DataProvider,
                         outcome_columns,
                         features):
    # check that outcome is available
    # difficult to check for NA as some features may be generated in the pipeline.
    if features:
        preexisting_features = [feature for feature in features if feature in dataset.data.columns]
        if not preexisting_features:
            preexisting_features = None
    else:
        preexisting_features = None
    return dataset.drop_na(metadata_subset=outcome_columns, data_subset=preexisting_features)


def for_columns(type_: str,
                mode: str,
                func: Callable,
                n_top: int):

    dataset = biopredict.dataset(mode)
    columns_list = _columns_for_type(dataset, type_)
    column_labels = [dataset.column_label(columns) for columns in columns_list]

    if n_top:
        print("Using top {}".format(n_top))
        featuresets = top_entries(mode, column_labels, n_top)
    else:
        featuresets = [None] * len(columns_list)

    results = []

    for columns, column_label, features in zip(columns_list, column_labels, featuresets):
        print(func.__name__ + " for " + column_label + ":")
        if features:
            print("Features: " + ", ".join(features))

        filtered_dataset = drop_missing_outcome(dataset, columns, features)
        X = filtered_dataset.data
        if n_top:
            suffix = "{}-{}".format(column_label, + n_top)
        else:
            suffix = column_label

        if type_ == "survival":
            df = func(mode, X, dataset, columns, suffix, features)
        else:
            y = filtered_dataset.metadata[columns]
            df = func(mode, X, y, dataset, suffix, features)

        if df is None:
            continue

        if features:
            df["features"] = ", ".join(features)
        df["n"] = n_top
        df["endpoint"] = column_label
        results.append(df)

    return pd.concat(results, axis=0)


def _summary_filename(type_, mode):
    return biopredict.cv_directory(mode) + "/summary-" + type_ + ".csv"


def _save_summary(dfs, type_, mode):
    results = pd.concat(dfs, axis=0)  # type:pd.DataFrame
    left_columns = ["endpoint", "n", "method", "features", "best score"]

    (results.reset_index(drop=True)
     .reindex(columns=left_columns + [col for col in results.columns if col not in left_columns])
     .sort_values(by=left_columns)
     .to_csv(_summary_filename(type_, mode), sep=";", decimal=",")
     )


def parameters_classification(mode: str):
    type_ = "classification"
    _save_summary(
        [for_columns(type_, mode, classifier_func, n_top)
         for n_top in biopredict.cv_feature_range(mode)
         for classifier_func in [
             xgb_classification_parameters,
             logreg_classification_parameters,
             svm_classification_parameters,
             rf_classification_parameters
         ]], 
        type_,
        mode
    )


def parameters_regression(mode: str):
    type_ = "survival"
    _save_summary(
        [for_columns(type_, mode, classifier_func, n_top)
         for n_top in biopredict.cv_feature_range(mode)
         for classifier_func in [
             xgb_regression_parameters,
             rf_regression_parameters,
             en_regression_parameters,
             svm_regression_parameters
         ]],
        type_,
        mode
    )


def best_pipeline_for(type_: str,
                      mode: str,
                      dataset,
                      survival_columns,
                      n_features: int = None):
    df = pd.read_csv(_summary_filename(type_, mode), sep=";", decimal=",")
    column_label = DataProvider.column_label(survival_columns)
    print("Best machine for {} - {}, {} features:".format(type_, column_label, n_features))

    best_machine = (df.
                    query("endpoint == @column_label" +
                          (" & n == @n_features" if n_features is not None else "")
                          ).
                    sort_values(by="best score", ascending=False).
                    iloc[0, :].
                    dropna())  # type: pd.Series

    print(best_machine)

    if best_machine["method"] == "SVC":
        machine = SVC()
    elif best_machine["method"] == "SVR":
        machine = SVR()
    elif best_machine["method"] == "Elastic Net":
        machine = ElasticNet()
    elif best_machine["method"] == "Logistic Regr":
        machine = LogisticRegression()
    elif best_machine["method"] == "RandomForest C":
        machine = RandomForestClassifier(random_state=rf_random_state)
    elif best_machine["method"] == "Random Forest Regr":
        machine = RandomForestRegressor(random_state=rf_random_state)
    elif best_machine["method"] == "XGB C":
        machine = XGBClassifier()
    elif best_machine["method"] == "XGB R":
        machine = XGBRegressor()
    else:
        raise ValueError("add machine type!")

    pipeline = create_pipeline(mode, type_, machine, dataset, survival_columns)

    first_param = best_machine.index.get_loc("best score") + 1

    def string_to_python(x):
        if isinstance(x, str):
            if "(" in x or "[" in x:
                return literal_eval(x)
        elif isinstance(x, float) and x.is_integer():
            return int(x)
        return x

    params = {k: string_to_python(v) for k, v in best_machine.iloc[first_param:].to_dict().items()}
    pipeline.set_params(**params)

    return pipeline, best_machine


def draw_confusion_matrices(mode: str,
                            prediction_df: pd.DataFrame):
    # draw confusion matrices
    plot_dir = prepare_directory("../plots/prediction-" + mode)
    endpoints = prediction_df["endpoint"].drop_duplicates()

    for endpoint in endpoints:
        endpoint_df = prediction_df.query("endpoint == @endpoint")
        plt.rc("font", size=10, family="Arial")
        fig, ax = plt.subplots(2, 5,
                               figsize=DinA(4).landscape())
        ax_flat = ax.ravel()
        subsets = endpoint_df["subset"].drop_duplicates()
        params = endpoint_df.drop_duplicates(subset=["endpoint"]).iloc[0, :]

        for subset in subsets:
            sub_prediction = endpoint_df.query("subset ==  @subset")
            matrix = confusion_matrix(sub_prediction["truth"], sub_prediction["prediction"])
            classes = params["categories"]
            plt.sca(ax_flat[subset])
            plot_confusion_matrix(matrix,
                                  classes,
                                  title="Subset {}".format(subset),
                                  colorbar=False)

        plt.suptitle("Classification by {} using {} features ({})".format(
            params["technique"], params["n"], params["features"]))
        fig.tight_layout()
        plt.savefig(plot_dir + "/confusion-matrices-" + endpoint + ".pdf")


def target_types():
    return ["classification", "survival"]


def predict_subsets(mode: str,
                    train_frac: float,
                    n_subsets: int,
                    n_features: int = None,
                    predict_full_set=False):
    dataset = biopredict.dataset(mode)
    dfs_for_type = {}
    for type_ in target_types():
        dfs = pd.DataFrame()
        columns_list = _columns_for_type(dataset, type_)
        for columns in columns_list:
            pipeline, best_params = best_pipeline_for(type_, mode, dataset, columns, n_features=n_features)
            features = best_params["features"].split(", ")
            pipeline.named_steps["featuresubset"].subset = features
            filtered_dataset = drop_missing_outcome(dataset, columns, features)
            x = filtered_dataset.data

            for subset in range(n_subsets):
                x_train = x.sample(frac=train_frac)
                x_test = x if predict_full_set else x.drop(x_train.index)  # type: pd.DataFrame
                y_train = filtered_dataset.metadata.loc[x_train.index]
                y_test = filtered_dataset.metadata.loc[x_test.index]

                ground_truth = y_test[columns]
                if type_ == "classification":
                    ground_truth.name = "truth"
                    pipeline.fit(x_train, y_train[columns])
                    y_pred = pipeline.predict(x_test)
                    y_dist = pipeline.decision_function(x_test)
                else:
                    ground_truth.columns = ["truth-time", "truth-status"]

                    # due to the applied imputation, we take the mean of several predictions
                    def y_predictions():
                        for rep in range(10):
                            pipeline.fit(x_train, y_train)
                            yield pipeline.predict(x_test)

                    y_pred = np.mean(np.stack(list(y_predictions()), axis=1), axis=1)
                    y_dist = None

                df = pd.DataFrame({
                    "endpoint": filtered_dataset.column_label(columns),
                    "subset": subset,
                    "n": best_params["n"],
                    "features": best_params["features"],
                    "technique": best_params["method"],
                    "prediction": y_pred
                }, index=x_test.index).join(ground_truth)

                if type_ == "classification":
                    cats = filtered_dataset.metadata[columns].cat.categories
                    score_columns = ["score binary"] if len(cats) == 2 else ["score for " + cat for cat in cats]
                    df = df.join([pd.Series([cats.values], index=df.index, name="categories"),
                                  pd.DataFrame(y_dist, columns=score_columns, index=df.index)])

                new_columns = [c for c in dfs.columns] + [c for c in df.columns if c not in dfs.columns]
                dfs = pd.concat([dfs, df], axis=0)  # type: pd.DataFrame
                dfs = dfs.reindex(columns=new_columns)

        feather_dfs = dfs
        if "categories" in feather_dfs.columns:
            feather_dfs = feather_dfs.drop("categories", axis=1)
        feather.write_dataframe(feather_dfs, biopredict.cv_directory(mode) + "/prediction-tests-" + type_ + ".feather")
        dfs.join(dataset.metadata).to_csv(biopredict.cv_directory(mode) + "/prediction-tests-" + type_ + ".csv",
                                          sep=";", decimal=",")

        dfs_for_type[type_] = dfs

    draw_confusion_matrices(mode, dfs_for_type["classification"])


def ensemble_prediction(mode: str,
                        subsets: SubsetsButOneDict):

    print("Ensemble prediction for", mode)
    dataset = biopredict.dataset(mode)
    df = pd.DataFrame()
    score_df = pd.DataFrame()

    for type_ in target_types():

        columns_list = _columns_for_type(dataset, type_)

        for columns in columns_list:
            pipeline, best_params = best_pipeline_for(type_, mode, dataset, columns, n_features=None)
            all_scores = np.empty(0)

            series = pd.Series({
                "mode": mode,
                "type": type_,
                "technique": best_params["method"]
            })

            if "features" in best_params:
                features = best_params["features"].split(", ")
                pipeline.named_steps["featuresubset"].subset = features
                series["n"] = best_params["n"]
                series["features"] = best_params["features"]
            else:
                features = None

            filtered_dataset = drop_missing_outcome(dataset, columns, features)
            filtered_dataset_ids = biopredict.dataset_ids(filtered_dataset.metadata)
            filtered_subsets = subsets.filter(filtered_dataset_ids)
            print("Dataset size: {}".format(len(filtered_dataset.data)))
            x = filtered_dataset.data
            label = filtered_dataset.column_label(columns)
            series["endpoint"] = label
            bar = Bar(label, max=len(x) * filtered_subsets.n_subsets_per_subject)

            print("{} patients to train prediction".format(
                np.mean([len(subset_ids)
                         for subject_id, subject_subsets in filtered_subsets.items()
                         for subset_ids in subject_subsets])
            ))

            def xy_subset(ids):
                mask = [biopredict.isin_dataset_ids(id_, ids) for id_ in filtered_dataset_ids]
                x = filtered_dataset.data.loc[mask]
                y = filtered_dataset.metadata.loc[mask]
                x = biopredict.drop_id_columns(x)
                return x, y

            for subject_id, subject_subsets in filtered_subsets.items():

                x_test, y_test = xy_subset([subject_id])
                ground_truth = y_test[columns]

                biopredict.assign_dataset_id(series, subject_id)
                series.name = series["id"]
                scores = np.full(len(subject_subsets), np.nan)

                if type_ == "classification":
                    use_probabilities = series["technique"] == "RandomForest C"
                    cats = filtered_dataset.metadata[columns].cat.categories
                    binary_classification = len(cats) == 2

                    if use_probabilities:
                        # we use predict_proba which returns n_samples, n_classes.
                        # Cut away the last probability which is 1-sum(others)
                        class_labels = cats[:-1]
                    else:
                        # we use decision_function, which return (n_samples,) for the binary case
                        # and (n_samples, n_classes) otherwise. All values carry information.
                        if binary_classification:
                            class_labels = ["{}/{}".format(cats[0], cats[1])]
                        else:
                            class_labels = cats
                    results = np.full((len(subject_subsets), len(class_labels)), np.nan)

                    for idx, subset_ids in enumerate(subject_subsets):
                        x_train, y_train = xy_subset(subset_ids)
                        # print("Subject {}, subset idx {}: Training on {}".format(subject_id, idx, len(x_train)))
                        assert not biopredict.isin_dataset_ids(subject_id, x_train)

                        pipeline.fit(x_train, y_train[columns])
                        classifier_classes = pipeline.steps[-1][1].classes_
                        if len(classifier_classes) < len(cats):
                            print("Not all labels present in subset {} for subject {}".format(idx, subject_id))
                        else:
                            if use_probabilities or len(cats) > 2:
                                labels_to_match = class_labels
                            else:
                                # in the binary case, label is "a/b", see above
                                labels_to_match = cats

                            idc_of_class_labels = [np.where(classifier_classes == label)[0][0]
                                                   for label in labels_to_match]
                            assert np.all(classifier_classes[idc_of_class_labels] == labels_to_match)

                            if use_probabilities:
                                results[idx] = pipeline.predict_proba(x_test)[:, idc_of_class_labels]
                            else:
                                if binary_classification:
                                    # is 1 if idc_of_class_labels is [0,1] and -1 if it is [1,0]
                                    inversion = [1, -1][idc_of_class_labels[0]]
                                    results[idx] = pipeline.decision_function(x_test)*inversion
                                else:
                                    results[idx] = pipeline.decision_function(x_test)[:, idc_of_class_labels]
                        scores[idx] = pipeline.score(x_test, ground_truth)
                        bar.next()

                    series["truth"] = ground_truth.iloc[0]

                    for class_idx, class_label in enumerate(class_labels):
                        series["class"] = class_label
                        for subset_idx in range(len(subject_subsets)):
                            series["subset_id"] = subset_idx
                            series["prediction"] = results[subset_idx, class_idx]
                            series["score"] = scores[subset_idx]
                            df = df.append(series)

                else:
                    n_imputations = 10
                    results = np.zeros(len(subject_subsets))

                    for idx, subset_ids in enumerate(subject_subsets):
                        x_train, y_train = xy_subset(subset_ids)

                        # perform imputation multiple times
                        def impute_predict():
                            for _ in range(n_imputations):
                                pipeline.fit(x_train, y_train)
                                result = pipeline.predict(x_test)
                                # score = pipeline.score(x_test, y_test)
                                yield result, 0

                        bar.next()
                        tuples = list(impute_predict())
                        results[idx] = np.asarray([t[0] for t in tuples]).mean()
                        scores[idx] = np.asarray([t[1] for t in tuples]).mean()

                    series["truth"] = ground_truth.iloc[0, 0]
                    series["truth-status"] = ground_truth.iloc[0, 1]

                    for subset_idx in range(len(subject_subsets)):
                        series["subset_id"] = subset_idx
                        series["prediction"] = results[subset_idx]
                        series["score"] = scores[subset_idx]
                        df = df.append(series)

                all_scores = np.append(all_scores, scores)

            score_df = score_df.append(pd.Series({"mode": mode,
                                                  "endpoint": label,
                                                  "score": np.mean(all_scores)}),
                                       ignore_index=True)
            bar.finish()

    df.to_csv(biopredict.cv_directory(mode) + "/ensemble-predictions.csv", sep=";", decimal=",")
    score_df.to_csv(biopredict.cv_directory(mode) + "/ensemble-predictions-scores.csv",
                    sep=";", decimal=",", index=False)
    # df.reset_index().to_feather(biopredict.cv_directory(mode) + "/ensemble-predictions.feather")
    return df


def load_ensemble_prediction(drop_na=True):
    source_data = biopredict.source_data()
    index_cols = ["subset_id", "id", "intra-patient"]
    index_cols_per_patient = index_cols[:-1]

    per_sample_predictions = pd.DataFrame()
    per_patient_predictions = pd.DataFrame()
    for mode in biopredict.applied_modes():
        mode_df = (pd.read_csv(biopredict.cv_directory(mode) + "/ensemble-predictions.csv",
                               sep=";", decimal=",", index_col=0).
                   rename(columns={"prediction": "value"}))
        print("Loading ensemble prediction for {}: {}".format(mode, len(mode_df)))

        # create compound id variable from mode, endpoint and class (for classification) or "time" (for regression)
        mode_df["mode"] = mode
        mode_df["variable"] = mode_df["mode"].str.cat([mode_df["technique"],
                                                       mode_df["endpoint"],
                                                       mode_df["class"].fillna("time")],
                                                      sep=" ")

        if biopredict.dataset_id_type(mode) == "per-sample":
            per_sample_predictions = per_sample_predictions.append(mode_df)
        else:
            per_patient_predictions = per_patient_predictions.append(mode_df)

    per_patient_predictions = (per_patient_predictions
                               [index_cols_per_patient + ["variable", "value"]].
                               # duplicates may have occurred in rare cases
                               drop_duplicates(subset=index_cols_per_patient + ["variable"]).
                               pivot_table(index=index_cols_per_patient, columns="variable", values="value").
                               reset_index()
                               )

    per_sample_predictions = (per_sample_predictions
                              [index_cols + ["variable", "value"]].
                              pivot_table(index=index_cols, columns="variable", values="value").
                              reset_index()
                              )

    prediction_df = per_sample_predictions.merge(per_patient_predictions,
                                                 how="left",
                                                 on=index_cols_per_patient,
                                                 suffixes=["", "_per_patient"])

    patients = prediction_df["id"].unique()
    subsets = prediction_df["subset_id"].unique()

    clin_vars = (ClinicalVariables(source_data, patients).
                 datasets(dataset_ids=subsets).
                 dropna())
    prediction_df = prediction_df.merge(clin_vars,
                                        how="left",
                                        on=index_cols_per_patient,
                                        suffixes=["", "_clin_vars"])  # type: pd.DataFrame

    if drop_na:
        prediction_df.dropna(inplace=True)

    dataset = source_data.dataset()
    assert np.all(pd.Series(patients).isin(dataset.metadata.id))
    metadata = dataset.metadata[dataset.metadata["id"].isin(patients)].drop_duplicates(index_cols_per_patient)
    metadata["primary endpoint"] = biopredict.primary_endpoint(metadata)

    # difference between primary endpoint and best response binary
    # diff = metadata["primary endpoint"] != (metadata["best response binary"] == "good")
    # print(metadata.loc[diff, ["name", "firstname", "best response IO", "TTF IO", "primary endpoint"]])
    metadata_merge = prediction_df.merge(metadata,
                                         how="left",
                                         on="id",
                                         suffixes=["", "_metadata"])
    endpoint = metadata_merge["primary endpoint"]

    # simplest metaprediction: take best response
    # from sklearn.metrics import accuracy_score
    # print(accuracy_score(prediction_df["rna SVC best response binary bad/good"] > 0,
    #                      metadata_merge["best response binary"] == "good"))
    # print(accuracy_score(prediction_df["rna SVC best response binary bad/good"] > 0, endpoint))

    return prediction_df[index_cols], prediction_df.drop(index_cols, axis=1), endpoint


def store_trained_models(model_directory):
    for mode in biopredict.applied_modes():
        mode_directory = model_directory + "/" + mode
        dataset = biopredict.dataset(mode)
        # store "primary transformer" parameters of dataset
        tr.ModelPersistence(mode_directory).store(dataset.transformer, {"mode": mode})

        for type_ in target_types():
            columns_list = _columns_for_type(dataset, type_)
            for columns in columns_list:
                pipeline, best_params = best_pipeline_for(type_, mode, dataset, columns, n_features=None)

                if "features" in best_params:
                    features = best_params["features"].split(", ")
                    pipeline.named_steps["featuresubset"].subset = features
                else:
                    features = None

                filtered_dataset = drop_missing_outcome(dataset, columns, features)
                label = filtered_dataset.column_label(columns)
                x_train = filtered_dataset.data
                y_train = filtered_dataset.metadata

                if type_ == "classification":
                    y_train = y_train[columns]

                pipeline.fit(x_train, y_train)

                metadata = {
                    "mode": mode,
                    "target type": type_,
                    "endpoint": label
                }
                if type_ == "classification":
                    metadata["categories"] = filtered_dataset.metadata[columns].cat.categories.tolist()

                tr.ModelPersistence(mode_directory + "/" + label).store(pipeline, metadata)


class TrainedModel:

    def __init__(self,
                 metadata,
                 pipeline):
        self.metadata = metadata
        self.pipeline = pipeline


class DatasetAndTrainedModels:

    def __init__(self,
                 mode,
                 dataset,
                 trained_models):
        self.mode = mode
        self.dataset = dataset
        self.trained_models = {trained_model.metadata["endpoint"]: trained_model for trained_model in trained_models}


def load_trained_models(storage_subdirectory):

    def create_pipeline_for_metadata(metadata, estimator):
        type_ = metadata["target type"]
        mode = metadata["mode"]
        return create_pipeline(mode, type_, estimator)

    with os.scandir(storage_subdirectory) as it:
        for entry in it:
            if entry.is_dir():
                yield tr.ModelPersistence.load(entry.path, create_pipeline_for_metadata)


def load_datasets_and_trained_models(storage_directory,
                                     dataset_intention):

    def create_primary_transformer_for_metadata(metadata, _):
        mode = metadata["mode"]
        return biopredict.primary_transformer(mode, remove_correlating=True)

    def datasets_and_trained_models():
        with os.scandir(storage_directory) as mode_it:
            for entry in mode_it:
                if entry.is_dir():
                    mode_metadata, primary_transformer = tr.ModelPersistence.load(entry.path,
                                                                                  create_primary_transformer_for_metadata)
                    mode = entry.name
                    assert mode_metadata["mode"] == mode
                    dset = biopredict.dataset(mode, dataset_intention, transformer=primary_transformer)

                    trained_models = [TrainedModel(metadata, pipeline)
                                      for metadata, pipeline in load_trained_models(entry.path)]

                    yield DatasetAndTrainedModels(mode, dset, trained_models)
    return list(datasets_and_trained_models())


def validate_using_stored():
    # TODO: Something is broken here, probably in the serialization
    dms = load_datasets_and_trained_models("../fit/models", "validate")
    df = None

    for dm in dms:
        print("Mode {}: {} full datasets, {} models".format(dm.mode,
                                                            len(dm.dataset.metadata.loc[
                                                                    dm.dataset.data.dropna().index]),
                                                            len(dm.trained_models)))
        metadata = dm.dataset.metadata.copy()
        metadata_columns = metadata.columns
        metadata["endpoint"] = biopredict.primary_endpoint(dm.dataset.metadata)
        for label, model in dm.trained_models.items():
            metadata["{} {}".format(dm.mode, label)] = model.pipeline.predict(dm.dataset.data)
        metadata.set_index("name", inplace=True)
        if df is None:
            df = metadata
        df = pd.concat([df, metadata.drop([col for col in metadata_columns if col != "name"], axis=1)], axis=1)
        # print("Accuracy: {}".format(accuracy_score(metadata["endpoint"], metadata["prediction"])))
        # print("AUC ROC:", roc_auc_score(metadata["endpoint"], metadata["prediction"]))
    df.to_csv("../binpred.csv", sep=";")


def validate(do_plot):
    from dataloading import BiopredictDataset
    plotdir = prepare_directory("../plots/visualization")

    prediction = pd.DataFrame()
    truth = pd.DataFrame()
    scores = pd.DataFrame()
    prediction_train = pd.DataFrame()
    truth_train = pd.DataFrame()
    feature_method_dict = {}
    first = True
    for mode in biopredict.applied_modes():

        if False:  # common normalization, should not be done
            dataset_full = biopredict.dataset(mode, "all")
            learn_mask = dataset_full.metadata["biopredict"] == "learn"
            validate_mask = dataset_full.metadata["biopredict"] == "validation"
            dataset_train = BiopredictDataset(dataset_full.data[learn_mask],
                                              dataset_full.metadata[learn_mask],
                                              dataset_full.metadata_column_types)
            dataset_test = BiopredictDataset(dataset_full.data[validate_mask],
                                              dataset_full.metadata[validate_mask],
                                              dataset_full.metadata_column_types)
        else:

            dataset_train = biopredict.dataset(mode, "learn")

            if mode == "rna-signatures" or mode == "rna-cell-composition":
                transformer = "default"
            else:
                transformer = dataset_train.transformer
            dataset_test = biopredict.dataset(mode, "validate",
                                              remove_correlating=False,
                                              transformer=transformer)

        for type_ in target_types():
            columns_list = _columns_for_type(dataset_train, type_)
            for columns in columns_list:
                pipeline, best_params = best_pipeline_for(type_, mode, dataset_train, columns, n_features=None)

                if "features" in best_params:
                    features = best_params["features"].split(", ")
                    pipeline.named_steps["featuresubset"].subset = features
                else:
                    features = None

                filtered_dataset = drop_missing_outcome(dataset_train, columns, features)
                label = filtered_dataset.column_label(columns)
                full_label = "{} {}".format(mode, label)
                x_train = filtered_dataset.data
                y_train = filtered_dataset.metadata

                x_test = dataset_test.data
                y_test = dataset_test.metadata

                if type_ == "classification":
                    y_train = y_train[columns]
                    y_test = y_test[columns]

                initial_transformer = pipeline.steps[0][1]
                if hasattr(initial_transformer, "unfiltered_data"):
                    initial_transformer.unfiltered_data = dataset_train.data
                pipeline.fit(x_train, y_train)

                metadata = dataset_test.metadata.set_index("id")
                metadata_train = filtered_dataset.metadata.set_index("id")
                if first:
                    for column in ["name", "best response IO", "PD-L1 TPS", "entity"] + \
                                  (columns if isinstance(columns, list) else [columns]):
                        truth[column] = metadata[column]
                        truth_train[column] = metadata_train[column]

                if hasattr(initial_transformer, "unfiltered_data"):
                    initial_transformer.unfiltered_data = dataset_test.data
                if hasattr(initial_transformer, "features"):
                    for feature in initial_transformer.features():
                        if feature in feature_method_dict:
                            feature_method_dict[feature].append(full_label)
                        else:
                            feature_method_dict[feature] = [full_label]

                pred_test = pipeline.predict(x_test)
                pred_train = pipeline.predict(x_train)

                prediction[full_label] = pd.Series(pred_test, index=metadata.index)
                prediction_train[full_label] = pd.Series(pred_train, index=metadata_train.index)

                if type_ == "classification":
                    estimator = pipeline.steps[-1][1]
                    if isinstance(estimator, WrappingEstimator):
                          estimator = estimator.estimator
                    use_probabilities = isinstance(estimator, RandomForestClassifier) or \
                                        isinstance(estimator, XGBClassifier)
                    cats = filtered_dataset.metadata[columns].cat.categories
                    binary_classification = len(cats) == 2
                    classifier_classes = estimator.classes_

                    if use_probabilities:
                        # we use predict_proba which returns n_samples, n_classes.
                        class_labels = cats
                    else:
                        # we use decision_function, which returns (n_samples,) for the binary case
                        # and (n_samples, n_classes) otherwise. All values carry information.
                        if binary_classification:
                            class_labels = ["{}/{}".format(cats[0], cats[1])]
                        else:
                            class_labels = cats

                    if use_probabilities or len(cats) > 2:
                        labels_to_match = class_labels
                    else:
                        # in the binary case, label is "a/b", see above
                        labels_to_match = cats

                    idc_of_class_labels = [np.where(classifier_classes == label)[0][0]
                                           for label in labels_to_match]
                    print(classifier_classes, labels_to_match, idc_of_class_labels)
                    assert np.all(classifier_classes[idc_of_class_labels] == labels_to_match)

                    if use_probabilities:
                        measure_of_decision = pipeline.predict_proba(x_test)[:, idc_of_class_labels]
                        print(measure_of_decision.shape)
                    else:
                        if binary_classification:
                            # is 1 if idc_of_class_labels is [0,1] and -1 if it is [1,0]
                            inversion = [1, -1][idc_of_class_labels[0]]
                            measure_of_decision = pipeline.decision_function(x_test) * inversion
                        else:
                            measure_of_decision = pipeline.decision_function(x_test)[:, idc_of_class_labels]

                    if binary_classification:
                        if measure_of_decision.ndim == 2:
                            measure_of_decision = measure_of_decision[:, labels_to_match == "good"].squeeze()
                        prediction[full_label + " MoD"] = pd.Series(measure_of_decision, index=metadata.index)
                        scores.at[full_label, "roc"] = roc_auc_score(y_test == "good", measure_of_decision)
                    else:
                        for idx, class_label in enumerate(class_labels):
                            prediction[full_label + " MoD " + class_label] = pd.Series(measure_of_decision[:, idx],
                                                                                       index=metadata.index)

                    if do_plot:
                        plot_decision_boundaries(pipeline, x_train, y_train, x_test, y_test,
                                                 filepath=plotdir + "/decision boundaries " + full_label + ".pdf")

                    scores.at[full_label, "acc"] = accuracy_score(y_test, pred_test)
        first = False

    (pd.concat([truth, prediction], axis=1).
     sort_values(by="TTF IO").
     to_csv("../prediction/prediction.csv", sep=";", decimal=","))

    (pd.concat([truth_train, prediction_train], axis=1).
     sort_values(by="TTF IO").
     to_csv("../prediction/prediction-train.csv", sep=";", decimal=","))

    scores.to_csv("../prediction/prediction-scores.csv", sep=";", decimal=",")

    pd.DataFrame.from_dict(feature_method_dict, orient="index").to_csv("../prediction/features.csv", sep=";",
                                                                       header=False)
    print(scores)


def validate_external():
    from dataloading import RiazCell2017Data
    from sklearn.pipeline import Pipeline

    prediction = pd.DataFrame()
    scores = pd.DataFrame()
    dataset_test = RiazCell2017Data().dataset()
    truth = dataset_test.metadata

    for mode in biopredict.applied_modes():

        dataset_train = biopredict.dataset(mode, "learn")
        type_ = "classification"
        columns_list = _columns_for_type(dataset_train, type_)

        for columns in columns_list:
            pipeline, best_params = best_pipeline_for(type_, mode, dataset_train, columns, n_features=None)
            test_pipeline = biopredict.pipeline(mode, None)

            if "features" in best_params:
                features = best_params["features"].split(", ")
                pipeline.named_steps["featuresubset"].subset = features
                test_pipeline.named_steps["featuresubset"].subset = features
            else:
                features = None

            filtered_dataset = drop_missing_outcome(dataset_train, columns, features)
            label = filtered_dataset.column_label(columns)
            full_label = "{} {}".format(mode, label)
            x_train = filtered_dataset.data
            y_train = filtered_dataset.metadata

            if type_ == "classification":
                y_train = y_train[columns]

            initial_transformer = pipeline.steps[0][1]
            if hasattr(initial_transformer, "unfiltered_data"):
                initial_transformer.unfiltered_data = dataset_train.data
            pipeline.fit(x_train, y_train)

            x_test = dataset_test.data
            metadata = dataset_test.metadata

            pipeline_middle_part = Pipeline(test_pipeline.steps[1:-1])
            # initial transformer performs feature selection, must be repeated
            x_test_transformed = pipeline.steps[0][1].transform(x_test)
            x_test_transformed = pipeline_middle_part.fit_transform(x_test_transformed)

            pred_test = pipeline.steps[-1][1].predict(x_test_transformed)

            prediction[full_label] = pd.Series(pred_test, index=metadata.index)

    (pd.concat([truth, prediction], axis=1).
     sort_values(by="PFS").
     to_csv("../prediction/prediction-external.csv", sep=";", decimal=","))

    print(scores)


def validate_berlin():
    prediction = pd.DataFrame()
    truth = pd.DataFrame()
    scores = pd.DataFrame()
    feature_method_dict = {}
    first = True
    for mode in biopredict.applied_modes():

        dataset_train = biopredict.dataset(mode, "learn")

        if mode == "rna-signatures" or mode == "rna-cell-composition":
            transformer = "default"
        else:
            transformer = "default"
            # transformer = dataset_train.transformer

        dataset_test = biopredict.dataset(mode, "validate",
                                          remove_correlating=False,
                                          transformer=transformer,
                                          source_data=biopredict.berlin_data())

        for type_ in target_types():
            columns_list = _columns_for_type(dataset_train, type_)
            for columns in columns_list:
                pipeline, best_params = best_pipeline_for(type_, mode, dataset_train, columns, n_features=None)

                if "features" in best_params:
                    features = best_params["features"].split(", ")
                    pipeline.named_steps["featuresubset"].subset = features
                else:
                    features = None

                filtered_dataset = drop_missing_outcome(dataset_train, columns, features)
                label = filtered_dataset.column_label(columns)
                full_label = "{} {}".format(mode, label)
                x_train = filtered_dataset.data
                y_train = filtered_dataset.metadata

                x_test = dataset_test.data
                y_test = dataset_test.metadata

                if type_ == "classification":
                    y_train = y_train[columns]
                    y_test = y_test[columns]

                initial_transformer = pipeline.steps[0][1]
                if hasattr(initial_transformer, "unfiltered_data"):
                    initial_transformer.unfiltered_data = dataset_train.data
                pipeline.fit(x_train, y_train)

                metadata = dataset_test.metadata.set_index("id")
                if first:
                    for column in ["best response IO", "PD-L1 TPS", "entity"] + \
                                  (columns if isinstance(columns, list) else [columns]):
                        truth[column] = metadata[column]

                if hasattr(initial_transformer, "unfiltered_data"):
                    initial_transformer.unfiltered_data = dataset_test.data
                if hasattr(initial_transformer, "features"):
                    for feature in initial_transformer.features():
                        if feature in feature_method_dict:
                            feature_method_dict[feature].append(full_label)
                        else:
                            feature_method_dict[feature] = [full_label]

                pred_test = pipeline.predict(x_test)

                prediction[full_label] = pd.Series(pred_test, index=metadata.index)

                if type_ == "classification":
                    estimator = pipeline.steps[-1][1]
                    if isinstance(estimator, WrappingEstimator):
                          estimator = estimator.estimator
                    use_probabilities = isinstance(estimator, RandomForestClassifier) or \
                                        isinstance(estimator, XGBClassifier)
                    cats = filtered_dataset.metadata[columns].cat.categories
                    binary_classification = len(cats) == 2
                    classifier_classes = estimator.classes_

                    if use_probabilities:
                        # we use predict_proba which returns n_samples, n_classes.
                        class_labels = cats
                    else:
                        # we use decision_function, which returns (n_samples,) for the binary case
                        # and (n_samples, n_classes) otherwise. All values carry information.
                        if binary_classification:
                            class_labels = ["{}/{}".format(cats[0], cats[1])]
                        else:
                            class_labels = cats

                    if use_probabilities or len(cats) > 2:
                        labels_to_match = class_labels
                    else:
                        # in the binary case, label is "a/b", see above
                        labels_to_match = cats

                    idc_of_class_labels = [np.where(classifier_classes == label)[0][0]
                                           for label in labels_to_match]
                    print(classifier_classes, labels_to_match, idc_of_class_labels)
                    assert np.all(classifier_classes[idc_of_class_labels] == labels_to_match)

                    if use_probabilities:
                        measure_of_decision = pipeline.predict_proba(x_test)[:, idc_of_class_labels]
                        print(measure_of_decision.shape)
                    else:
                        if binary_classification:
                            # is 1 if idc_of_class_labels is [0,1] and -1 if it is [1,0]
                            inversion = [1, -1][idc_of_class_labels[0]]
                            measure_of_decision = pipeline.decision_function(x_test) * inversion
                        else:
                            measure_of_decision = pipeline.decision_function(x_test)[:, idc_of_class_labels]

                    if binary_classification:
                        if measure_of_decision.ndim == 2:
                            measure_of_decision = measure_of_decision[:, labels_to_match == "good"].squeeze()
                        prediction[full_label + " MoD"] = pd.Series(measure_of_decision, index=metadata.index)
                        scores.at[full_label, "roc"] = roc_auc_score(y_test == "good", measure_of_decision)
                    else:
                        for idx, class_label in enumerate(class_labels):
                            prediction[full_label + " MoD " + class_label] = pd.Series(measure_of_decision[:, idx],
                                                                                       index=metadata.index)

                    scores.at[full_label, "acc"] = accuracy_score(y_test, pred_test)

                    # if isinstance(estimator, SVC):
                    #     print(estimator.dual_coef_)
                    #     print(estimator.support_vectors_)
                    #     print(estimator.intercept_)
                    #     print(estimator.n_support_)
                    #     print(estimator.support_)
                    #     exit()
        first = False

    (pd.concat([truth, prediction], axis=1).
     sort_values(by="TTF IO").
     to_csv("../prediction/berlin/prediction.csv", sep=";", decimal=","))

    scores.to_csv("../prediction/berlin/prediction-scores.csv", sep=";", decimal=",")

    pd.DataFrame.from_dict(feature_method_dict, orient="index").to_csv("../prediction/berlin/features.csv", sep=";",
                                                                       header=False)
    print(scores)


def visualisation():
    plot_dir = prepare_directory("../plots/visualisation")
    for mode in biopredict.applied_modes():

        dataset_train = biopredict.dataset(mode, "learn")
        transformer = "default"

        for type_ in ["classification"]:
            columns_list = _columns_for_type(dataset_train, type_)
            for columns in columns_list:
                pipeline, best_params = best_pipeline_for(type_, mode, dataset_train, columns, n_features=None)

                estimator = pipeline.steps[-1][1]
                if isinstance(estimator, WrappingEstimator):
                    estimator = estimator.estimator

                if "features" in best_params:
                    features = best_params["features"].split(", ")
                    pipeline.named_steps["featuresubset"].subset = features
                else:
                    features = None

                filtered_dataset = drop_missing_outcome(dataset_train, columns, features)
                label = filtered_dataset.column_label(columns)
                full_label = "{} {}".format(mode, label)
                x_train = filtered_dataset.data
                y_train = filtered_dataset.metadata[columns]
                initial_transformer = pipeline.steps[0][1]
                if hasattr(initial_transformer, "unfiltered_data"):
                    initial_transformer.unfiltered_data = dataset_train.data
                # restrict to binary/ter problem
                if not np.all(np.isin(y_train.cat.categories, ["good", "bad", "intermediate"])):
                    continue
                if not isinstance(estimator, SVC) and not isinstance(estimator, RandomForestClassifier):
                    continue

                result = pd.DataFrame()
                if isinstance(estimator, SVC):
                    # enable Platt estimation method
                    estimator.probability = True

                    pipeline.fit(x_train, y_train)

                    # plt.rc("font", size=10, family="Arial")
                    # fig, ax = plt.subplots(1, len(features),
                    #                        figsize=DinA(4).landscape())
                    # ax = ax.ravel()

                    for feature in features:
                        # plt.sca(ax[i])
                        testrange = np.linspace(x_train[feature].min(),
                                                x_train[feature].max(),
                                                50)
                        probs = pd.DataFrame(columns=["feature", "category"] + testrange.tolist(),
                                             index=x_train.index)
                        probs["feature"] = feature
                        probs["category"] = y_train
                        category_mask = y_train.cat.categories == "bad"
                        for category in y_train.cat.categories:
                            print("Getting probabilities for {}, {}".format(feature, category))
                            mask = y_train == category
                            sample = x_train[mask].copy()
                            for value in testrange:
                                sample[feature] = value
                                probs.loc[mask, value] = 1-pipeline.predict_proba(sample)[:, category_mask]
                        result = result.append(probs, sort=True)

                elif isinstance(estimator, RandomForestClassifier):
                    pipeline.fit(x_train, y_train)

                    # use first transformer to transform to cell type space
                    x_cell_type = pipeline.steps[0][1].transform(x_train)
                    # disable the already performed transformation step
                    pipeline.steps[0] = (None, "passthrough")
                    # in percentage space
                    x_sample_sum = x_cell_type.sum(axis=1)


                    for cell_type in x_cell_type.columns:
                        # plt.sca(ax[i])
                        testrange = np.linspace(0.01, 0.99, 50)
                        probs = pd.DataFrame(columns=["feature", "category"] + testrange.tolist(),
                                             index=x_train.index)
                        probs["feature"] = cell_type
                        probs["category"] = y_train
                        category_mask = y_train.cat.categories == "bad"
                        for category in y_train.cat.categories:
                            print("Getting probabilities for {}, {}".format(cell_type, category))
                            mask = y_train == category
                            for value in testrange:
                                # set a new percent value for one cell type, and subtract the added amount from the
                                # other cell types according to their relative distribution
                                new_value = value * x_sample_sum[mask]
                                diff_to_true_value = x_cell_type.loc[mask, cell_type] - new_value
                                other_type_relative = x_cell_type.loc[mask, x_cell_type.columns != cell_type].apply(
                                    lambda x: x / x.sum(), axis=1)
                                sample = x_cell_type[mask].copy()
                                sample[cell_type] = new_value
                                differences_over_all_other_types = other_type_relative.mul(diff_to_true_value, axis=0)
                                sample.loc[:, sample.columns != cell_type] += differences_over_all_other_types
                                probs.loc[mask, value] = 1-pipeline.predict_proba(sample)[:, category_mask]
                        result = result.append(probs, sort=True)

                result.to_csv(plot_dir + "/" + full_label + ".csv")
                #         print(probs)
                #         aggregates = probs.aggregate(["mean", "std"])
                #         print("Aggregate for {}, {}".format(feature, category))
                #         print(aggregates)
                #         plt.plot(testrange, aggregates.loc["mean", :])#, linestyle="-", label=category)
                #
                # plt.savefig(plot_dir + "/" + full_label + ".pdf")








def combine_ternary(df, cols, target_value="good"):
    assert len(cols) == 2
    return pd.Series(
        np.choose((df[cols[0]] == target_value) & (df[cols[1]] == target_value), ["discordant", target_value]),
        name="two ternaries " + target_value)


def combine_all_four(df, cols, target_value="good"):
    assert len(cols) == 4
    return pd.Series(
        np.choose((df[cols[0]] == target_value) &
                  (df[cols[0]] == df[cols[1]]) & (df[cols[1]] == df[cols[2]]) & (df[cols[2]] == df[cols[3]]),
                  ["discordant", target_value]),
        name="all four " + target_value)


def test_parameters(modes):
    from sklearn.metrics import confusion_matrix
    prediction_df = pd.read_csv("../prediction/prediction.csv", sep=";", decimal=",")

    pred_combs_good = [
        (combine_ternary(prediction_df,
                         ["rna best response ternary", "rna-signatures best response ternary"], "good"),
         ["good"]),
        (combine_all_four(prediction_df,
                          ["rna best response binary", "rna-signatures best response binary",
                           "rna best response ternary", "rna-signatures best response ternary"], "good"),
         ["good"])
    ]

    pred_combs_bad = [
        (combine_ternary(prediction_df,
                         ["rna best response ternary", "rna-signatures best response ternary"], "bad"),
         ["bad"]),
        (combine_all_four(prediction_df,
                          ["rna best response binary", "rna-signatures best response binary",
                           "rna best response ternary", "rna-signatures best response ternary"], "bad"),
         ["bad"])
    ]

    truth_br_good = ("best response binary", ["good"])
    truth_ttf_good = ("TTF IO binary", ["good"])
    truth_br_bad = ("best response binary", ["bad"])
    truth_ttf_bad = ("TTF IO binary", ["bad"])
    pred_br_good = [("best response binary", ["good"]),
                    ("best response ternary", ["good", "intermediate"])]
    pred_br_bad = [("best response binary", ["bad"]),
                   ("best response ternary", ["bad", "intermediate"])
                   ]

    comparisons = [
        (truth_br_good, pred_br_good),
        (truth_ttf_good, pred_br_good),
        (truth_br_bad, pred_br_bad),
        (truth_ttf_bad, pred_br_bad)
    ]
    combinations = [
        (truth_br_good, pred_combs_good),
        (truth_ttf_good, pred_combs_good),
        (truth_br_bad, pred_combs_bad),
        (truth_ttf_bad, pred_combs_bad)
    ]

    def comparator_label(column, values):
        if len(values) == 1:
            return "{} == {}".format(column, values[0])
        else:
            return "{} is {}".format(column, " or ".join(values))

    def testparams(truth, prediction):
        cm = confusion_matrix(truth, prediction)
        TN = cm[0, 0]
        FN = cm[1, 0]
        TP = cm[1, 1]
        FP = cm[0, 1]

        return pd.DataFrame({
            "sensitivity": TP / (TP + FN),
            "specificity": TN / (TN + FP),
            "ppv": TP / (TP + FP),
            "npv": TN / (TN + FN),
            "TP": TP,
            "FP": FP,
            "TN": TN,
            "FN": FN
        }, index=[0])

    def testparams_for_comparisons():
        for comparison in comparisons:
            comparison_data = comparison[0]
            for prediction_data in comparison[1]:

                truth = prediction_df[comparison_data[0]].isin(comparison_data[1])

                prediction_column = "{} {}".format(mode, prediction_data[0])
                prediction_series = prediction_df[prediction_column]
                prediction_valueset = prediction_data[1]
                prediction = prediction_series.isin(prediction_valueset)
                mask = ~prediction_series.isnull()
                truth = truth[mask]
                prediction = prediction[mask]

                df = pd.DataFrame({
                    "source mode": mode,
                    "truth": comparator_label(comparison_data[0], comparison_data[1]),
                    "predicted by": comparator_label(prediction_column, prediction_valueset)
                }, index=[0])
                df = pd.concat([df, testparams(truth, prediction)], axis=1)

                if len(prediction_valueset) == 1:
                    binary_column_label = prediction_column + " MoD"
                    multiclass_column_label = prediction_column + " MoD " + prediction_valueset[0]
                    if binary_column_label in prediction_df or multiclass_column_label in prediction_df:
                        if binary_column_label in prediction_df:
                            measure_of_decision = prediction_df[binary_column_label]
                        else:
                            measure_of_decision = prediction_df[multiclass_column_label]

                        df["auc"] = roc_auc_score(truth, measure_of_decision[mask])

                yield df
                
    def testparams_for_combinations():
        for comparison in combinations:
            comparison_data = comparison[0]
            for prediction_data in comparison[1]:

                truth = prediction_df[comparison_data[0]].isin(comparison_data[1])

                prediction_series = prediction_data[0]
                prediction_valueset = prediction_data[1]
                prediction = prediction_series.isin(prediction_valueset)
                mask = ~prediction_series.isnull()
                truth = truth[mask]
                prediction = prediction[mask]

                df = pd.DataFrame({
                    "source mode": "combination",
                    "truth": comparator_label(comparison_data[0], comparison_data[1]),
                    "predicted by": comparator_label(prediction_series.name, prediction_valueset)
                }, index=[0])
                df = pd.concat([df, testparams(truth, prediction)], axis=1)

                yield df

    df = pd.DataFrame()
    for mode in modes:
        df = df.append(list(testparams_for_comparisons()), ignore_index=True, sort=False)
    df = df.append(list(testparams_for_combinations()), ignore_index=True, sort=False)
    df.to_csv("../prediction/prediction-testparameters.csv", sep=";", decimal=",")


def best_parameter_results():

    def formatted_parameters(params):
        for key, value in params.items():
            if "__" in key:
                key = "".join(key.split("__")[1:])
                if isinstance(value, float):
                    value = "{:.2f}".format(value)
                yield "{}: {}".format(key, value)
            if key == "features":
                yield "Genes: " + ", ".join(value)

    def format_score(x):
        if 0 < x < 1.0:
            return "{:.1%}".format(x)
        else:
            return "{:.1f}".format(abs(x))

    def parameter_series():
        for mode in biopredict.applied_modes():
            dataset_train = biopredict.dataset(mode, "learn")
            for type_ in target_types():
                columns_list = _columns_for_type(dataset_train, type_)
                for columns in columns_list:
                    _, best_params = best_pipeline_for(type_, mode, dataset_train, columns, n_features=None)
                    label = dataset_train.column_label(columns)
                    yield pd.Series({
                        "mode": mode,
                        "type": type_,
                        "endpoint": label,
                        "algorithm": best_params["method"],
                        "hyperparameters": "; ".join(formatted_parameters(best_params)),
                        "best score": format_score(best_params["best score"])
                    })

    df = pd.concat(list(parameter_series()), axis=1, ignore_index=True).T
    df = df.set_index(["mode", "type", "endpoint"]).sort_index()  # type: pd.DataFrame
    df.to_csv("../best_parameters.csv", sep=";", decimal=",")


if __name__ == "__main__":
    import warnings
    # XGBClassifier uses sklearn code which triggers a numpy warning
    warnings.filterwarnings(action='ignore', category=DeprecationWarning)
    # silence pandas FutureWarnings
    warnings.filterwarnings(action='ignore', category=FutureWarning)

    import argparse as ap
    parser = ap.ArgumentParser(description="Prediction tools")
    parser.add_argument("data", choices=biopredict.possible_modes() + ["all"])
    parser.add_argument("actions",
                        choices=["cv-regression", "cv-classification", "predict-subsets", "ensemble-prediction",
                                 "store-models", "validate", "validate-external", "validate-berlin",
                                 "testparameters", "bestparameters", "visualisation"],
                        nargs="+")
    parser.add_argument("--jobs", default=n_jobs, type=int)
    parser.add_argument("--n_subsets", default=20, type=int)
    parser.add_argument("--train_frac", default=0.5, type=float)
    parser.add_argument("--plot", default=False, type=bool)
    args = parser.parse_args()

    n_jobs = min(max(1, args.jobs), 72)
    if args.data == "all":
        modes = biopredict.applied_modes()
    else:
        modes = [args.data]

    if "cv-regression" in args.actions:
        parameters_regression(args.data)
    if "cv-classification" in args.actions:
        parameters_classification(args.data)
    if "predict-subsets" in args.actions:
        predict_subsets(args.data, train_frac=args.train_frac, n_subsets=args.n_subsets)
    if "ensemble-prediction" in args.actions:
        strata = biopredict.dataset_id_strata()
        subjects = strata[0]  # the first entry is considered the "core" set
        subsets = SubsetsButOneDict(strata, subjects)
        subsets.generate_subsets(n_subsets=args.n_subsets, frac=args.train_frac, random_seed=1)
        for mode in modes:
            df = ensemble_prediction(mode, subsets)
    if "store-models" in args.actions:
        store_trained_models("../fit/models")
    if "validate" in args.actions:
        validate(args.plot)
    if "validate-external" in args.actions:
        validate_external()
    if "validate-berlin" in args.actions:
        validate_berlin()
    if "testparameters" in args.actions:
        test_parameters(modes)
    if "bestparameters" in args.actions:
        best_parameter_results()
    if "visualisation" in args.actions:
        visualisation()



